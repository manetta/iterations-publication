# -*- coding: UTF-8 -*-

import os, json, random

from reportlab.platypus.doctemplate import PageBreak, PageTemplate
from reportlab.platypus.flowables import Preformatted, KeepTogether
from reportlab.platypus import NextPageTemplate, Frame, Image, FrameBreak, Table, TableStyle, Spacer, KeepTogether
from reportlab.lib import colors
from reportlab.lib.units import mm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.graphics.shapes import Drawing, Line, PolyLine
from reportlab.lib.colors import PCMYKColor, Color

from functools import partial
from stylesheet import *
from variables import *
from traces import *

from reportlab.platypus import Paragraph
from reportlab.graphics import shapes
import re

pagesizes = [110*mm, 170*mm]

def addTriggerPattern(startxx, startyy, steps, color, pdf):
	# rectangles
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	w = 2
	h = 3
	xx = startxx
	yy = startyy

	# put the yy on a grid
	yy = round(yy)
	while yy % 3 != 0:
		yy += 1

	for n in range(1, steps, 1):
		yy_down = yy
		yy_up = yy

		# adjust the width of the shapes slowly
		w -= 0.2
		if w < 0:
			w = 0.1

		pdf.setFillColorCMYK(c,m,y,k)
		
		for _ in range(1, n, 1):
			xx += 1
			pdf.rect(xx*mm, yy_down*mm, w*mm, h*mm, fill=1, stroke=0)
			pdf.rect(xx*mm, yy_up*mm, w*mm, h*mm, fill=1, stroke=0)

			# adjust yy position slightly:		
			reach = [reach for reach in range(0, 9, 3)]
			yy_down -= random.choice(reach)
			yy_up += random.choice(reach)

def translateCoordinateCoverPages(coordinate):
	newcoordinate = (60 * sin((pi/30) * (coordinate - 5))) + 90
	return newcoordinate

def backgroundshape(canvas, color=None):
	xx = 12
	yy = 160
	steps = 25
	if color == None:
		color = [0,0,0,0]
	for line in range(0,160,10):
		for i in range(10):
			# yy = translateCoordinateCoverPages(xx)
			addTriggerPattern(xx, yy, steps, color, canvas)
			reach = [r for r in range(-12, 12, 3)]
			rx = random.choice(reach)
			xx += rx
			yy -= 10

# cover
frontcover = False
def drawTracesPages(canvas, doc, filename):
	canvas.saveState()
	pagenumber = canvas.getPageNumber()

	canvas.setFillColorCMYK(0, 0, 0, 0.1) # background grey
	canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)	
	
	# Page 1-4 (Front cover)
	if not 'back' in filename:
		
		# trace 1
		if pagenumber == 1:

			# reset
			canvas.restoreState()
			canvas.saveState()
	
			trace5(canvas)

			canvas.setFillColorCMYK(1,1,1,1) 
			canvas.setFont('brazil', 32)
			canvas.drawCentredString(55*mm, 140*mm, 'Iterations')

		# trace 2
		elif pagenumber == 2:

			# reset
			canvas.restoreState()
			canvas.saveState()
		
			trace4_1(canvas)
		
		# trace 3
		elif pagenumber == 3:

			# reset
			canvas.restoreState()
			canvas.saveState()

			trace15_1(canvas)

		# trace 4
		elif pagenumber == 4:

			# reset
			canvas.restoreState()
			canvas.saveState()
	
			trace11(canvas)
	
	# backcovers
	else:

		# trace 1
		if pagenumber == 1:

			# reset
			canvas.restoreState()
			canvas.saveState()
	
			trace14(canvas)
		
		# trace 2
		elif pagenumber == 2:

			# reset
			canvas.restoreState()
			canvas.saveState()
			
			trace18(canvas)
		
		# trace 3
		elif pagenumber == 3:

			# reset
			canvas.restoreState()
			canvas.saveState()

		# trace 4
		elif pagenumber == 4:

			# reset
			canvas.restoreState()
			canvas.saveState()
	
			trace16(canvas)

	canvas.restoreState()

def makeSectionCover(canvas, doc, contribution, bookletnr):
	canvas.saveState()
	pagenumber = canvas.getPageNumber()

	greycolor = 0.1
	
	if pagenumber == 1:
		if 'x-dex' in contribution.lower():
			color = xtext
			c, m, y, k = color[0], color[1], color[2], color[3]
			canvas.setFillColorCMYK(c,m,y,k)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)
			backgroundshape(canvas, color=xtextback)	
		elif 'introduction' in contribution.lower():
			color = [0.75,0.75,0.75,1]
			c, m, y, k = color[0], color[1], color[2], color[3]
			canvas.setFillColorCMYK(c,m,y,k)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)
			backgroundshape(canvas, color=[0,0,0,0.5])	
		else:
			canvas.setFillColorCMYK(0, 0, 0, 0.2)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)
			backgroundshape(canvas, color=[0,0,0,0])
	elif pagenumber > 2:
		if 'intro' in contribution.lower():
			canvas.setFillColorCMYK(0.75,0.75,0.75,1)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)
		elif 'x-dex' in contribution.lower():
			color = xtext
			c, m, y, k = color[0], color[1], color[2], color[3]
			canvas.setFillColorCMYK(c, m, y, k)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)
		else:
			canvas.setFillColorCMYK(0, 0, 0, greycolor)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)

		if 'spell' in contribution.lower():
			trace2(canvas)
		elif 'rica' in contribution.lower():
			trace19(canvas)
		elif 'spideralex' in contribution.lower():
			trace16(canvas)
		elif 'behuki' in contribution.lower():
			# trace10(canvas)
			trace7(canvas)
		elif 'collective-conditions' in contribution.lower():
			trace13(canvas)
		elif 'scores' in contribution.lower():
			trace8(canvas)
		elif 'introduction' in contribution.lower():
			trace1(canvas, color=[0,0,0,0])
		elif 'x-dex' in contribution.lower():
			trace12(canvas)
	else:
		if 'introduction' in contribution.lower():
			canvas.setFillColorCMYK(0.75, 0.75, 0.75, 1)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)
		elif 'x-dex' in contribution.lower():
			color = xtext
			c, m, y, k = color[0], color[1], color[2], color[3]
			canvas.setFillColorCMYK(c, m, y, k)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)
		else:
			canvas.setFillColorCMYK(0, 0, 0, greycolor)
			canvas.rect(0*mm,0*mm,110*mm,170*mm, stroke=0, fill=1)

	canvas.restoreState()

def makeSpellPages(canvas, doc):
	canvas.saveState()
	# canvas.setFillColorCMYK(0, 0.0, 0, 1)
	# for x in range(10, 170, 20):
	# 	x = 170 - x
	# 	canvas.circle(110*mm, x*mm, 1*mm, stroke=1, fill=0)
		# canvas.rect(109*mm, x*mm, 110*mm, 1*mm, stroke=0, fill=1)
	canvas.restoreState()

def makeRicaPages(canvas, doc):
	canvas.saveState()
	# canvas.setFillColorCMYK(0, 0.0, 0, 1)
	# for x in range(10, 170, 30):
	# 	x = 170 - x
	# 	canvas.rect(109*mm, x*mm, 110*mm, 1*mm, stroke=0, fill=1)
	canvas.restoreState()

def addBehukiBackground(canvas, doc):
	canvas.saveState()
	canvas.setFillColorCMYK(0, 0, 0, 0.05)
	canvas.rect(0*mm, 0*mm, 110*mm, 170*mm, stroke=0, fill=1)
	canvas.restoreState()

def addBlackBackground(canvas, doc):
	canvas.saveState()
	canvas.setFillColorCMYK(0.75, 0.75, 0.75, 1)
	canvas.rect(0*mm, 0*mm, 110*mm, 170*mm, stroke=0, fill=1)
	canvas.restoreState()

def addBehukiSection(canvas, doc, section):
	canvas.saveState()	
	pagenumber = canvas.getPageNumber()
	canvas.setFont('brazil', 12.5)
	canvas.setFillColorCMYK(1, 1, 1, 1)
	home = [3, 4, 22]
	opt = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
	var = [18, 19, 20, 21]
	if pagenumber in home:
		canvas.drawCentredString(53.5*mm, 6*mm, '/home /boot /lost&found')
	elif pagenumber in opt:
		canvas.drawCentredString(53.5*mm, 6*mm, '/opt')
	elif pagenumber in var:
		canvas.drawCentredString(53.5*mm, 6*mm, '/var')
	if pagenumber == 23:
		canvas.setFillColorCMYK(0, 0, 0, 0)
		canvas.rect(0*mm, 0*mm, 110*mm, 170*mm, stroke=0, fill=1)
	canvas.restoreState()

def forcePageBreak(canvas, doc):
	canvas.saveState()	
	canvas.showPage()
	canvas.restoreState()

def makeZigzag(width, height=11, linewidth=0.1):
	d = Drawing(width*mm, height)
	c = []
	for x in range(0, width, 20):
		c.append(x*mm)
		c.append(0)
		c.append((x+10)*mm)
		c.append(height)
	l = PolyLine(c, strokeColor=xdexcolor3, strokeWidth=linewidth*mm)
	d.add(l)
	return d

def addGreyBackground(canvas, doc):
	canvas.saveState()
	canvas.setFillColorCMYK(0, 0, 0, 0.1)
	canvas.rect(0*mm, 0*mm, 110*mm, 170*mm, stroke=0, fill=1)
	canvas.restoreState()

def addXdexScoresfooter(canvas, doc):
	canvas.saveState()
	pagenumber = canvas.getPageNumber()
	canvas.setFont('unifont', 7)

	# add legend
	if pagenumber >= 26 and pagenumber <= 40:
		if pagenumber % 2 == 0:
			# legend
			c, m, y, k = xdexcolor2[0], xdexcolor2[1], xdexcolor2[2], xdexcolor2[3]	
			canvas.setFillColorCMYK(c,m,y,k)
			
			canvas.drawString(7.5*mm, 10.5*mm, 'Handles (H)')

			canvas.drawString(7.5*mm, 7*mm, '◷ = Time')
			canvas.drawString(7.5*mm, 3.5*mm, '▩ = We')
			canvas.drawString(27*mm, 7*mm, '⁎ = How')
			canvas.drawString(27*mm, 3.5*mm, '⧒ = Transition')

			canvas.drawString(55*mm, 10.5*mm, 'Forms (F)')

			c, m, y, k = xcurves[0], xcurves[1], xcurves[2], xcurves[3]
			canvas.setFillColorCMYK(c,m,y,k)
			canvas.drawString(55*mm, 7*mm, 'Curves')
			c, m, y, k = xcolors[0], xcolors[1], xcolors[2], xcolors[3]
			canvas.setFillColorCMYK(c,m,y,k)
			canvas.drawString(55*mm, 3.5*mm, 'Colors')

			c, m, y, k = xtext[0], xtext[1], xtext[2], xtext[3]
			canvas.setFillColorCMYK(c,m,y,k)
			canvas.drawString(75*mm, 7*mm, 'Text')
			c, m, y, k = xabsences[0], xabsences[1], xabsences[2], xabsences[3]
			canvas.setFillColorCMYK(c,m,y,k)
			canvas.drawString(75*mm, 3.5*mm, 'Absences')

		else:
			c, m, y, k = xdexcolor2[0], xdexcolor2[1], xdexcolor2[2], xdexcolor2[3]	
			canvas.setFillColorCMYK(c,m,y,k)

			canvas.drawString(7.5*mm, 10.5*mm, 'Contributions (C)')
			canvas.drawString(7.5*mm, 7*mm, 'Kym Ward')
			canvas.drawString(7.5*mm, 3.5*mm, 'Rica Rickson')
			# canvas.drawString(7.5*mm, 7*mm, 'holding spell')
			# canvas.drawString(7.5*mm, 3.5*mm, 'Becoming Rica Rickson')

			canvas.drawString(28*mm, 7*mm, 'spideralex')
			canvas.drawString(28*mm, 3.5*mm, 'Behuki')
			# canvas.drawString(35*mm, 7*mm, 'Por debajo y por los lados.')
			# canvas.drawString(35*mm, 3.5*mm, 'Dear visitor,')

			canvas.drawString(45*mm, 7*mm, 'Collective Conditions')
			canvas.drawString(45*mm, 3.5*mm, 'common ground')
			# canvas.drawString(57.5*mm, 7*mm, 'Three Documents')
			# canvas.drawString(57.5*mm, 3.5*mm, 'scores')

			canvas.setFont('brazil', 7)
			canvas.drawCentredString(95*mm, 7*mm, 'x-Dexing Scores')
			canvas.setFont('unifont', 7)
			canvas.drawCentredString(95*mm, 3.5*mm, '(Beta Version)')

	canvas.restoreState()

def insertXdexScores(story):
	introtext = '''This beta-version is produced during a booksprint in Barcelona in
		February 2020. The x-dexers present were Lluís Nacenta, Nayarí Castillo,
		Ludovica Michelin, Peter Westerberg, Femke Snelting, Jara Rocha and Manetta
		Berends.'''
	intro = Paragraph(introtext, stylesheet['xdexscores'])
	story.append(intro) 
		
	story.append(NextPageTemplate('xdexScores'))
	story.append(PageBreak())

	f = open('x-dexing-scores.json', 'r')
	data = json.load(f)
	print('xxx nr of scores: {}'.format(len(data.keys())))

	for num in data.keys():
		scores = []
		if len(data[num]['score']) < 2000:
			fixed1 = Paragraph(data[num]['fixed1'], style=stylesheet['xdexscoremetadata'])
			fixed2 = Paragraph(data[num]['fixed2'], style=stylesheet['xdexscoremetadata'])
			loose = Paragraph(data[num]['loose'], style=stylesheet['xdexscoremetadata'])
			scoretext = data[num]['score'][0].upper()+data[num]['score'][1:]
			score = Paragraph('<br />'+scoretext+'<br /><br />', style=stylesheet['xdexscores'])
			comment = Paragraph(data[num]['comment'], style=stylesheet['xdexscoremetadata'])
			
			scores.append([
					Paragraph('Comment', stylesheet['xdexscoremetadataheader']), 
					Paragraph('Fixed', stylesheet['xdexscoremetadataheader']), 
					Paragraph('Fixed', stylesheet['xdexscoremetadataheader']), 
					Paragraph('Loose', stylesheet['xdexscoremetadataheader'])
				])
			scores.append([comment, fixed1, fixed2, loose])

		if len(scores) > 0:
			scores_table = Table(
				scores, 
				colWidths=['50%','16.25%','16.25%','16.25%']
				)

			if any('curve' in string.lower() for string in [data[num]['fixed1'], data[num]['fixed2']]):
				selectedcolor = xcurves
				selectedbackcolor = xcurvesback
			elif any('color' in string.lower() for string in [data[num]['fixed1'], data[num]['fixed2']]):
				selectedcolor = xcolors
				selectedbackcolor = xcolorsback
			elif any('text' in string.lower() for string in [data[num]['fixed1'], data[num]['fixed2']]):
				selectedcolor = xtext
				selectedbackcolor = xtextback
			elif any('absence' in string.lower() for string in [data[num]['fixed1'], data[num]['fixed2']]):
				selectedcolor = xabsences
				selectedbackcolor = xabsencesback
			else:
				selectedcolor = (1, 1, 1, 1)
				selectedbackcolor = (0, 0, 0, 0.25)

			style1 = [
				('GRID', (0, 0), (-1, -1), 0.25, selectedcolor),
				('VALIGN',(0, 0), (-1, -1), 'TOP'),
				('ALIGN)',(1, 0), (-1, -1), 'CENTER'),
				('BACKGROUND', (0, 0), (-1, 0), selectedbackcolor),
			]
			style2 = [
				('GRID', (0, 0), (-1, -1), 0.25, selectedcolor),
			]
			scores_table.setStyle(TableStyle(style1))
			
			# score table
			if len(data[num]['score']) < 1000:
				thescore_content = [
					# [Paragraph('score', stylesheet['xdexscoremetadataheader'])],
				]
				thescore_content.append([score])
				thescore_table = Table(
					thescore_content, 
					colWidths=['100%'],
					)
				thescore_table.setStyle(TableStyle(style2))
			else: 
				thescore_table = score
				print('xxx score len: {}'.format(len(data[num]['score'])))

			space = Spacer(50*mm, 5*mm)
			wholescore = KeepTogether([thescore_table, scores_table, space])
			story.append(wholescore)

	return story

def addSimpleTable(header, text, description, counter, story):
	content = []
	content.append([header])
	content.append([text])
	content.append([description])
	table = Table(
		content, 
		colWidths=['100%'],
		)
	style = [
		('GRID', (0, 0), (-1, -1), 0.25, (1, 1, 1, 1)),
		('VALIGN',(0, 0), (-1, -1), 'TOP'),
		('BACKGROUND', (0, 0), (-1, 0), (0, 0, 0, 0.25))
	]
	table.setStyle(TableStyle(style))
	space = Spacer(50*mm, 11)
	smallspace = Spacer(50*mm, 8)
	if counter == 1:
		wholescore = KeepTogether([space, table, smallspace])
	elif counter in [2, 3]:
		wholescore = KeepTogether([table, smallspace])
	elif counter == 4:
		wholescore = KeepTogether([table, space])
	
	story.append(wholescore)
	
	return story

def addSuperSimpleTable(text, story):
	content = []
	content.append(['', text])
	table = Table(
		content, 
		colWidths=['20%','80%'],
		)
	style = [
		# ('GRID', (0, 0), (-1, -1), 0.25, (1, 1, 1, 1)),
		('VALIGN',(0, 0), (-1, -1), 'TOP'),
		# ('BACKGROUND', (1, 0), (-1, 0), (0, 0, 0, 0.05)),
		('BOTTOMPADDING', (0, 0), (-1, 0), 8),
		('TOPPADDING', (0, 0), (-1, 0), 5),
	]
	table.setStyle(TableStyle(style))
	space = Spacer(50*mm, 6.5)

	wholesbox = KeepTogether([table, space])
	story.append(wholesbox)
	return story

def drawColorTests(canvas, doc):
	canvas.saveState()	

	colorsset = [
		(0.5, 1, 0, 0),
		(0, 0.6, 0, 0),
		(1, 0, 1, 0),
		(0, 0.25, 1, 0),

		(0.75, 0.75, 0, 0),
		(0, 0.5, 0, 0),
		(1, 0, 0.75, 0),
		(1, 0.25, 0, 0),
	
		(0.75, 1, 0, 0),
		(0, 0.7, 0, 0),
		(0.75, 0, 1, 0),
		(0, 0.5, 1, 0),
	
		(0, 1, 1, 0),
		(0, 1, 0, 0),
		(1, 0, 0, 0),
		(0, 0.75, 1, 0),

		(1, 0.1, 0, 0),
		(0, 0.7, 0, 0),
		(0.75, 0, 1, 0),
		(0, 0.5, 1, 0),
	
		(0, 0, 0, 0),
		(0, 0, 0, 0),
		(0, 0, 0, 0),
		(0, 0, 0, 0),
	
		xcurves,
		xcolors,
		xtext,
		xabsences,
	
		(1,0,0,0),
		xcolors,
		xtext,
		xabsences,
	]
	xx = 0
	yy = 0
	for c,m,y,k in colorsset:
		if xx % 4 == 0:
			xx = 0
			yy += 1
		print(xx)
		print(yy)
		canvas.setFillColorCMYK(c,m,y,k)
		canvas.rect(40+(xx*20)*mm, yy*15*mm, 20*mm, 15*mm, stroke=0, fill=1)

		xx += 1

	canvas.restoreState()
