from reportlab.lib.styles import ParagraphStyle, StyleSheet1
from reportlab.lib import colors
from reportlab.platypus.paragraph import TA_LEFT, TA_CENTER, TA_RIGHT
from reportlab.lib.units import mm

from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.pdfbase import pdfmetrics

# from colors import * 
from os import path 
from variables import *

# registerFont(TTFont('font', './assets/FantasqueSansMono-Regular.ttf'))
# registerFont(TTFont('font-i', './assets/FantasqueSansMono-Italic.ttf'))
# registerFont(TTFont('font-b', './assets/FantasqueSansMono-Bold.ttf'))
# registerFont(TTFont('font-bi', './assets/FantasqueSansMono-BoldItalic.ttf'))

# registerFont(TTFont('font', './assets/cmunorm.ttf'))
registerFont(TTFont('font', './assets/concrete-reg-arrow.ttf'))
registerFont(TTFont('font-i', './assets/cmunoti.ttf'))
registerFont(TTFont('font-b', './assets/concrete-bold-arrow.ttf'))
# registerFont(TTFont('font-b', './assets/cmunobx.ttf'))
registerFont(TTFont('font-bi', './assets/cmunobi.ttf'))

registerFont(TTFont('brazil', './assets/Brazil-Regular-extraglyphs.ttf'))
registerFont(TTFont('mono', './assets/LiberationMono.ttf'))
registerFont(TTFont('unifont', './assets/unifont-11.0.03.ttf'))
registerFont(TTFont('fantasque', './assets/FantasqueSansMono-Regular.ttf'))
registerFont(TTFont('concreteitalic', './assets/cmunoti.ttf'))

# registerFontFamily('font', main='r', bold='b', italic='i', boldItalic='bi')
# did not work :/

from reportlab.lib.fonts import addMapping

addMapping('font', 0, 0, 'font') #main
addMapping('font', 0, 1, 'font-i') #italic
addMapping('font', 1, 0, 'font-b') #bold
addMapping('font', 1, 1, 'font-bi') #italic and bold 

addMapping('unifont', 0, 0, 'unifont') #main
addMapping('unifont', 0, 1, 'concreteitalic') #italic
# addMapping('mono', 1, 0, 'font-b') #bold
# addMapping('mono', 1, 1, 'font-bi') #italic and bold 

stylesheet = StyleSheet1()

# alignment = 0
# allowOrphans = 0
# allowWidows = 1
# backColor = None
# borderColor = None
# borderPadding = 0
# borderRadius = None
# borderWidth = 0
# bulletAnchor = start
# bulletFontName = Helvetica
# bulletFontSize = 10
# bulletIndent = 0
# embeddedHyphenation = 1
# endDots = None
# firstLineIndent = 0
# fontName = Helvetica
# fontSize = 10
# hyphenationLang = en_GB
# justifyBreaks = 0
# justifyLastLine = 0
# leading = 12
# leftIndent = 0
# linkUnderline = 0
# rightIndent = 0
# spaceAfter = 0
# spaceBefore = 0
# spaceShrinkage = 0.05
# splitLongWords = 1
# strikeGap = 1
# strikeOffset = 0.25*F
# strikeWidth =
# textColor = Color(0,0,0,1)
# textTransform = None
# underlineGap = 1
# underlineOffset = -0.125*F
# underlineWidth =
# uriWasteReduce = 0.3
# wordWrap = None

stylesheet.add(ParagraphStyle(name='main',
							fontName='font',
							fontSize=10,
							leading=13,
							letterSpacing=50*mm,
							spacebefore=0, 
							spaceAfter=13, 
							allowOrphans=0,
							allowWidows=0,
							underlineGap = 2*mm,
							underlineOffset = -0.3*mm,
							underlineWidth = 0.1*mm,
							hyphenationLang='en_GB',
							hyphenationMinWordLength=5,
							uriWasteReduce=0.25), # which means that we will try and split a word that looks like a uri if we would waste at least half of the line.
						)

stylesheet.add(ParagraphStyle(name='h1',
							  parent=stylesheet['main'],
							  fontName='brazil',
							  fontSize=36,
							  leading=45,
							  textColor=(0,0,0,1),
							  hyphenationMinWordLength=10,
							  alignment=TA_CENTER,
							  spaceBefore=45, # does not respond?
							  spaceAfter=45),
						alias='h1')

stylesheet.add(ParagraphStyle(name='h1white',
							  parent=stylesheet['main'],
							  fontName='brazil',
							  fontSize=32,
							  leading=44,
							  textColor=(0,0,0,0),
							  hyphenationMinWordLength=10,
							  alignment=TA_CENTER,
							  spaceBefore=22, # does not respond?
							  spaceAfter=22),
						alias='h1white')

stylesheet.add(ParagraphStyle(name='h1xdex',
							  parent=stylesheet['main'],
							  fontName='brazil',
							  fontSize=26,
							  leading=44,
							  textColor=xdexcolor2,
							  hyphenationMinWordLength=10,
							  alignment=TA_CENTER,
							  spaceBefore=22,
							  spaceAfter=22),
						alias='h1xdex')

stylesheet.add(ParagraphStyle(name='h1xdexcover',
							  parent=stylesheet['main'],
							  fontName='brazil',
							  fontSize=32,
							  leading=44,
							  textColor=[0,0,0,0],
							  # textColor=[1,1,1,1],
							  hyphenationMinWordLength=10,
							  alignment=TA_CENTER,
							  spaceBefore=22,
							  spaceAfter=22),
						alias='h1xdexcover')

stylesheet.add(ParagraphStyle(name='h2',
							parent=stylesheet['main'],
							fontName='brazil',
							fontSize=15,
							leading=26,
							textColor=(0,0,0,1),
							alignment=TA_CENTER,
							spaceBefore=19.5,
							spaceAfter=13),
						alias='h2')

stylesheet.add(ParagraphStyle(name='h2white',
							parent=stylesheet['main'],
							fontName='brazil',
							fontSize=15,
							leading=22,
						 	leftIndent=5*mm,
							textColor=(0,0,0,0),
							alignment=TA_CENTER,
							spaceBefore=22,
							spaceAfter=11),
						alias='h2white')

stylesheet.add(ParagraphStyle(name='h2xdex',
							parent=stylesheet['main'],
							fontName='brazil',
							fontSize=15,
							leading=22,
						 	leftIndent=5*mm,
							textColor=xdexcolor2,
							alignment=TA_CENTER,
							spaceBefore=0,
							spaceAfter=11),
						alias='h2xdex')

stylesheet.add(ParagraphStyle(name='h3',
							parent=stylesheet['main'],
							fontName='font-b',
							fontSize=10,
							leading=13,
							textColor=(1,1,1,1),
							spaceBefore=0,
							spaceAfter=0),
						alias='h3')

stylesheet.add(ParagraphStyle(name='backcover',
							  parent=stylesheet['main'],
							  fontName='brazil',
							  fontSize=15,
							  leading=22,
							  textColor=(0,0,0,1),
							  leftIndent=5*mm,
							  rightIndent=5*mm,
							  hyphenationMinWordLength=15,
							  alignment=TA_CENTER,
							  spaceBefore=22,
							  spaceAfter=22),
						alias='backcover')

stylesheet.add(ParagraphStyle(name='backcoverxdex',
							  parent=stylesheet['main'],
							  fontName='brazil',
							  fontSize=15,
							  leading=22,
							  textColor=xtextback,
							  leftIndent=5*mm,
							  rightIndent=5*mm,
							  hyphenationMinWordLength=15,
							  alignment=TA_CENTER,
							  spaceBefore=22,
							  spaceAfter=22),
						alias='backcoverxdex')

stylesheet.add(ParagraphStyle(name='small',
							parent=stylesheet['main'],
							fontName='font-i',
							fontSize=7,
							leading=10,
							firstLineIndent=0*mm,
							leftIndent=5*mm,
							spaceBefore=13,
							spaceAfter=13),
						alias='small')

stylesheet.add(ParagraphStyle(name='smallwhite',
							parent=stylesheet['main'],
							fontName='font-i',
							fontSize=7,
							textColor=(0,0,0,0),
							leading=10,
							firstLineIndent=0*mm,
							leftIndent=5*mm,
							spaceBefore=13,
							spaceAfter=13),
						alias='smallwhite')

stylesheet.add(ParagraphStyle(name='blockquote',
							  parent=stylesheet['main'],
							  fontName='font-i',
							  fontSize=10,
							  leading=13,
							  spaceBefore=0,
							  spaceAfter=13),
						alias='blockquote')

stylesheet.add(ParagraphStyle(name='blockquotewhite',
							  parent=stylesheet['main'],
							  fontName='font-i',
							  fontSize=8.5,
							  leading=11,
							  textColor=(0,0,0,0),
							  spaceBefore=11,
							  spaceAfter=11),
						alias='blockquotewhite')

stylesheet.add(ParagraphStyle(name='li',
							  parent=stylesheet['main'],
							  fontName='font',
							  fontSize=10,
							  leading=13,
							  firstLineIndent=-6.25*mm,
							  allowWidows=0,
							  allowOrphans=0,
							  spaceBefore=0,
							  spaceAfter=0),
						alias='li')

stylesheet.add(ParagraphStyle(name='df',
							  parent=stylesheet['main'],
							  fontName='font',
							  fontSize=10,
							  leading=13,
							  leftIndent=0*mm,
							  bulletIndent=-7*mm,
							  allowWidows=0,
							  allowOrphans=0,
							  spaceBefore=15,
							  spaceAfter=15),
						alias='df')

stylesheet.add(ParagraphStyle(name='footnotes',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=7,
							leading=10,
							firstLineIndent=-6*mm,
							leftIndent=5*mm,
							# bulletIndent=-7*mm,
							spaceBefore=0,
							spaceAfter=4),
						alias='footnotes')

stylesheet.add(ParagraphStyle(name='comment',
							  parent=stylesheet['main'],
							  fontName='unifont',
							  fontSize=7,
							  leading=9.75,
							  # firstLineIndent=0*mm,
							  # firstLineIndent=-9.75*mm,
							  # leftIndent=9.75*mm,
							  leftIndent=5*mm,
							  # textColor=(0,0,0,0.4),
							  # leftIndent=50*mm,
							  rightIndent=5*mm,
							  # rightIndent=-12*mm,
							  spaceBefore=0,
							  spaceAfter=13),
						alias='comment')

# -------- CUSTOM STYLES PER CONTRIBUTION --------------

stylesheet.add(ParagraphStyle(name='spell',
							  parent=stylesheet['main'],
							  fontName='font',
							  fontSize=10,
							  leading=13,
							  spaceBefore=0,
							  spaceAfter=0,
							  firstLineIndent=5*mm,
							  allowWidows=0,
							  allowOrphans=0,
							  hyphenationLang='en_GB'),
						alias='spell')

stylesheet.add(ParagraphStyle(name='rica',
							  parent=stylesheet['main'],
							  fontName='font',
							  fontSize=11,
							  leading=14,
							  spaceBefore=0,
							  spaceAfter=14,
							  # hyphenationMinWordLength=7,
							  # uriWasteReduce=1,
							  allowWidows=1,
							  allowOrphans=1),
						alias='rica')

stylesheet.add(ParagraphStyle(name='spideralex',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=10,
							leading=13,
							spaceBefore=0,
							spaceAfter=0,
							firstLineIndent=5*mm,
							allowWidows=0,
							allowOrphans=0,
							uriWasteReduce=0.25,
							hyphenationMinWordLength=7,
							hyphenationLang='es_ES'),
						alias='spideralex')

stylesheet.add(ParagraphStyle(name='behuki',
							  parent=stylesheet['main'],
							  fontName='font',
							  fontSize=10,
							  leading=13,
							  firstLineIndent=5*mm,
							  spaceBefore=0,
							  spaceAfter=0,
							  textColor=(0,0,0,0.2),
							  allowWidows=0,
							  allowOrphans=0,
							  hyphenationLang='en_GB',
							  uriWasteReduce=0.25), # which means that we will try and split a word that looks like a uri if we would waste at least half of the line.
						alias='behuki')

stylesheet.add(ParagraphStyle(name='collectiveconditions',
							  parent=stylesheet['main'],
							  fontName='font',
							  fontSize=10,
							  leading=13,
							  spaceBefore=0,
							  spaceAfter=13,
							  allowWidows=0,
							  allowOrphans=0,
							  hyphenationLang='en_GB',
							  uriWasteReduce=0.25), # which means that we will try and split a word that looks like a uri if we would waste at least half of the line.
						alias='collectiveconditions')

stylesheet.add(ParagraphStyle(name='scores',
							  parent=stylesheet['main'],
							  fontName='font',
							  fontSize=10,
							  leading=13,
							  firstLineIndent=5*mm,
							  spaceBefore=0,
							  spaceAfter=0,
							  allowWidows=0,
							  allowOrphans=0,
							  hyphenationLang='en_GB',
							  uriWasteReduce=0.25), # which means that we will try and split a word that looks like a uri if we would waste at least half of the line.
						alias='scores')

stylesheet.add(ParagraphStyle(name='introcolumnsCAT',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							alignment=TA_LEFT,
							textColor=(0,0,0,0),
							firstLineIndent=5*mm,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5,
							hyphenationLang='ca',
							allowOrphans=0,
							allowWidows=0),
						alias='introcolumnsCAT')
stylesheet.add(ParagraphStyle(name='introcolumnsDE',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							alignment=TA_LEFT,
							textColor=(0,0,0,0),
							firstLineIndent=5*mm,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5,
							hyphenationLang='de_AT',
							allowOrphans=0,
							allowWidows=0),
						alias='introcolumnsDE')
stylesheet.add(ParagraphStyle(name='introcolumnsEN',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							alignment=TA_LEFT,
							textColor=(0,0,0,0),
							firstLineIndent=5*mm,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5,
							hyphenationLang='en_GB',
							allowOrphans=0,
							allowWidows=0),
						alias='introcolumnsEN')
stylesheet.add(ParagraphStyle(name='introcolumnsES',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							alignment=TA_LEFT,
							textColor=(0,0,0,0),
							firstLineIndent=5*mm,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5,
							hyphenationLang='es',
							allowOrphans=0,
							allowWidows=0),
						alias='introcolumnsES')
stylesheet.add(ParagraphStyle(name='introcolumnsFR',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							alignment=TA_LEFT,
							textColor=(0,0,0,0),
							firstLineIndent=5*mm,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=12,
							hyphenationLang='fr',
							allowOrphans=0,
							allowWidows=0),
						alias='introcolumnsFR')
stylesheet.add(ParagraphStyle(name='introcolumnsNL',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							alignment=TA_LEFT,
							textColor=(0,0,0,0),
							firstLineIndent=5*mm,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5,
							hyphenationLang='nl_NL',
							allowOrphans=0,
							allowWidows=0),
						alias='introcolumnsNL')

stylesheet.add(ParagraphStyle(name='xdexcolumnsCAT',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							firstLineIndent=5*mm,
							textColor=xdexcolor2,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5, # works!
							hyphenationLang='ca',
							allowOrphans=0,
							allowWidows=0),
						alias='xdexcolumnsCAT')
stylesheet.add(ParagraphStyle(name='xdexcolumnsDE',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							firstLineIndent=5*mm,
							textColor=xdexcolor2,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5, # works!
							hyphenationLang='de_AT',
							allowOrphans=0,
							allowWidows=0),
						alias='xdexcolumnsDE')
stylesheet.add(ParagraphStyle(name='xdexcolumnsEN',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							firstLineIndent=5*mm,
							textColor=xdexcolor2,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5,
							hyphenationLang='en_GB',
							allowOrphans=0,
							allowWidows=0),
						alias='xdexcolumnsEN')
stylesheet.add(ParagraphStyle(name='xdexcolumnsES',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							firstLineIndent=5*mm,
							textColor=xdexcolor2,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5, 
							hyphenationLang='es',
							allowOrphans=0,
							allowWidows=0),
						alias='xdexcolumnsES')
stylesheet.add(ParagraphStyle(name='xdexcolumnsFR',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							firstLineIndent=5*mm,
							textColor=xdexcolor2,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=12, 
							hyphenationLang='fr',
							allowOrphans=0,
							allowWidows=0),
						alias='xdexcolumnsFR')
stylesheet.add(ParagraphStyle(name='xdexcolumnsNL',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=8,
							leading=11,
							firstLineIndent=5*mm,
							textColor=xdexcolor2,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5, 
							hyphenationLang='nl_NL',
							allowOrphans=0,
							allowWidows=0),
						alias='xdexcolumnsNL')

stylesheet.add(ParagraphStyle(name='smallxdex',
							parent=stylesheet['main'],
							fontName='font-i',
							fontSize=7,
							leading=11,
							firstLineIndent=-2.5*mm,
							leftIndent=5*mm,
							spaceBefore=11,
							spaceAfter=0),
						alias='smallxdex')

stylesheet.add(ParagraphStyle(name='xdexcolumntables',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=7,
							leading=9.5,
							textColor=xdexcolor2,
							spaceBefore=0,
							spaceAfter=0,
							hyphenationMinWordLength=5, # works!
							hyphenationLang='en_GB',
							allowOrphans=0,
							allowWidows=0),
						alias='xdexcolumntables')

stylesheet.add(ParagraphStyle(name='xdexlistitem',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=8,
							leading=11,
							textColor=xdexcolor2,
							firstLineIndent=-11.25*mm,
							leftIndent=11.25*mm,
							spaceBefore=11,
							spaceAfter=11,
							hyphenationMinWordLength=5, # works!
							hyphenationLang='en_GB'),
						alias='xdexlistitem')

stylesheet.add(ParagraphStyle(name='xdexscores',
							  parent=stylesheet['main'],
							  fontName='font',
							  fontSize=10,
							  leading=13,
							  textColor=xdexcolor2,
							  spaceBefore=5*mm,
							  spaceAfter=5*mm,
							  leftIndent=5*mm,
							  rightIndent=5*mm,
							  allowWidows=0,
							  allowOrphans=0,
							  hyphenationLang='en_GB',
							  hyphenationMinWordLength=7,
							  uriWasteReduce=0.25),
						alias='xdexscores')

stylesheet.add(ParagraphStyle(name='xdexscoremetadataheader',
							parent=stylesheet['main'],
							fontName='font-i',
							fontSize=7,
							leading=11,
							hyphenationLang='en_GB',
							textColor=xdexcolor2,
							spaceBefore=0,
							spaceAfter=0),
						alias='xdexscoremetadataheader')

stylesheet.add(ParagraphStyle(name='xdexscoremetadata',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=7,
							leading=9.5,
							hyphenationLang='en_GB',
							textColor=xdexcolor2,
							hyphenationMinWordLength=9,
							spaceBefore=0,
							spaceAfter=0),
						alias='xdexscoremetadata')

# ---- colors ----

stylesheet.add(ParagraphStyle(name='color-curves',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=7.5,
							leading=11,
							hyphenationLang='en_GB',
							textColor=xcurves,
							spaceBefore=11,
							spaceAfter=0),
						alias='color-curves')

stylesheet.add(ParagraphStyle(name='color-colors',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=7.5,
							leading=11,
							hyphenationLang='en_GB',
							textColor=xcolors,
							spaceBefore=11,
							spaceAfter=0),
						alias='color-colors')

stylesheet.add(ParagraphStyle(name='color-text',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=7.5,
							leading=11,
							hyphenationLang='en_GB',
							textColor=xtext,
							spaceBefore=11,
							spaceAfter=0),
						alias='color-text')

stylesheet.add(ParagraphStyle(name='color-absences',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=7.5,
							leading=11,
							hyphenationLang='en_GB',
							textColor=xabsences,
							spaceBefore=11,
							spaceAfter=0),
						alias='color-absences')

# -----------

stylesheet.add(ParagraphStyle(name='columns',
							parent=stylesheet['main'],
							fontName='font',
							fontSize=10,
							leading=13,
							alignment=TA_LEFT,
							spaceBefore=0,
							spaceAfter=15,
							hyphenationMinWordLength=5, # works!
							hyphenationLang='en_GB',
							allowOrphans=0,
							allowWidows=0),
						alias='columns')

stylesheet.add(ParagraphStyle(name='mono',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=7,
							leading=10,
							spaceBefore=0,
							spaceAfter=10),
						alias='mono')

stylesheet.add(ParagraphStyle(name='timeline',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=8,
							leading=9.5,
							hyphenationMinWordLength=5),
						alias='cover')

stylesheet.add(ParagraphStyle(name='colophon',
							parent=stylesheet['main'],
							fontName='unifont',
							fontSize=5,
							leading=8,
							textColor=(0,0,0,0),
							hyphenationMinWordLength=10, # works!
							uriWasteReduce=0.75,
							wordWrap=1,
							alignment=TA_LEFT,
							spaceBefore=0,
							spaceAfter=8),
						alias='colophon')

# ------------------------

# Traces styles

# ------------------------

stylesheet.add(ParagraphStyle(name='trace',
							fontName='unifont',
							fontSize=15.5,
							leading=20,
							spacebefore=0, 
							spaceAfter=13, 
							textColor=xtext,
							hyphenationLang='en_US',
							hyphenationMinWordLength=5),
						)
stylesheet.add(ParagraphStyle(name='tracequote',
							fontName='unifont',
							fontSize=15.5,
							leading=20,
							spacebefore=0, 
							spaceAfter=16, 
							textColor=xtext,
							hyphenationLang='en_US',
							hyphenationMinWordLength=10),
						)
stylesheet.add(ParagraphStyle(name='tracequestions',
							fontName='unifont',
							fontSize=7,
							leading=8,
							spacebefore=0, 
							spaceAfter=13, 
							textColor=xtext,
							alignment=TA_RIGHT,
							hyphenationLang='en_US',
							hyphenationMinWordLength=5),
						)
stylesheet.add(ParagraphStyle(name='absences',
							fontName='unifont',
							fontSize=7,
							leading=8.5,
							spacebefore=0, 
							spaceAfter=4.5, 
							leftIndent=2*mm,
							rightIndent=2*mm,
							# firstLineIndent=-5*mm,
							textColor=xabsences,
							hyphenationLang='en_US',
							hyphenationMinWordLength=5),
						)
stylesheet.add(ParagraphStyle(name='tracesmall',
							fontName='unifont',
							fontSize=8,
							leading=10,
							spacebefore=0, 
							spaceAfter=0, 
							textColor=xtext,
							hyphenationLang='en_US',
							hyphenationMinWordLength=8),
						)
stylesheet.add(ParagraphStyle(name='tmporalties',
							fontName='unifont',
							fontSize=6,
							leading=6.5,
							spacebefore=0, 
							spaceAfter=0, 
							textColor=[0,1,1,0],
							hyphenationLang='en_US',
							hyphenationMinWordLength=8),
						)
stylesheet.add(ParagraphStyle(name='glossary',
							fontName='unifont',
							fontSize=5.85,
							leading=7,
							leftIndent=2.5*mm,
							spacebefore=0, 
							spaceAfter=0, 
							allowWidows=0,
							allowOrphans=0,
							# textColor=[0,0,0,0],
							textColor=xtext,
							alignment=TA_CENTER,
							hyphenationLang='en_US',
							hyphenationMinWordLength=15),
						)
stylesheet.add(ParagraphStyle(name='normal',
							fontName='unifont',
							fontSize=10,
							leading=12.5,
							spacebefore=0, 
							spaceAfter=0, 
							textColor=xtext,
							hyphenationLang='en_US',
							hyphenationMinWordLength=8),
						)
stylesheet.add(ParagraphStyle(name='wordblocks',
							fontName='unifont',
							fontSize=10,
							leading=12,
							spacebefore=0, 
							spaceAfter=0, 
							# textColor=[0,0,0,0],
							leftIndent=2.5*mm,
							alignment=TA_CENTER,
							hyphenationLang='en_US',
							hyphenationMinWordLength=5),
						)