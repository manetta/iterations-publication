#!/usr/bin/sh

MDDIR="md/"
HTMLDIR="html/"
RMLDIR="rml/"

MDS=$(for f in $MDDIR*.md; do echo "${f}"; done)
# echo $MDS

# convert MD to HTML
echo "MD → HTML (converting with pandoc)"
for md in $MDS; do 
	echo ... $md
	html=$(echo $md | sed "s/md/html/g")
	echo ... $html
	pandoc -f markdown -t html $md -o $html
done

# copy HTML folder to a RML folder 
echo "HTML → RML (copying)"
mkdir rml
cp -r html/* rml/

# convert HTML to RML, replace <p> with <para>
echo "HTML → RML (converting with sed)";
RMLS=$(for f in $RMLDIR*.html; do echo "${f}"; done)
for rml in $RMLS; do 
	echo "... $rml"

	# pff... sed & non-greedy matching :/
	# using perl instead!

	perl -pi -e 's|(<p>)|<para>|g' $rml
 	perl -pi -e 's|(<\/p>)|<\/para>|g' $rml

	# change "underline" classes to <u> elements 
	# !! important to do this before the font changes below !!
	perl -pi -e 's|(<span class="underline)|<font style=underline|g' $rml

	# change (all!) span's into font elements + add class as color specificity
	perl -pi -e 's|(<span class=)|<font bgcolor=|g' $rml
	perl -pi -e 's|(<\/span>)|<\/font>|g' $rml
	
	# remove id's, classes, alt's
	perl -pi -e 's|(class=".*?")||g' $rml
	perl -pi -e 's|(id=".*?")||g' $rml
	perl -pi -e 's|(alt=".*?")||g' $rml
	
	# remove quotes
	perl -pi -e 's|"||g' $rml

	# remove double spaces
	perl -pi -e 's|(  )| |g' $rml
	perl -pi -e 's|( >)|>|g' $rml

	# remove <para> elements within <li> elements
	perl -pi -e 's|(<li><para>)|<li>|g' $rml
	perl -pi -e 's|(<\/para><\/li>)|<\/li>|g' $rml

	# remove linebreaks after <br /> elements
	perl -pi -e 's|(<br \/>\n)|<br \/>|g' $rml

	# remove all backlinks on footnotes
	perl -pi -e 's|<a href=.*?>↩</a>||g' $rml
	perl -pi -e 's|<a .*?><sup>|<sup>|g' $rml
	perl -pi -e 's|<\/sup><\/a>|<\/sup>|g' $rml

	# modify images
	# perl -pi -e 's|<img|<img width=265 height=425 valign=-400|g' $rml

	# remove <para> from <blockquotes>
	perl -pi -e 's|<blockquote>\n|XXXXX|g' $rml
	perl -pi -e 's|XXXXX<para>|<blockquote>|g' $rml
	sed ':begin;$!N;s/<\/para>\n<\/blockquote>/<\/blockquote>/;tbegin;P;D' --in-place $rml # 1000000*thanks to https://backreference.org/2009/12/23/how-to-match-newlines-in-sed/
	
	# remove comments
	perl -pi -e 's|^<\!--.*?\n||g' $rml
	# perl -pi -e 's|^-->.*?\n||g' $rml

done

cat rml/rica-rickson.bak.html > rml/rica-rickson.html

echo "DONE! :)"
