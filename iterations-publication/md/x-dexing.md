% x-dexing
% Jara Rocha & Manetta Berends

ANINDEXA-<br />CIÓ
===========

En tant que mecanisme iterador, aquesta publicació ofereix als lectors
un conjunt d\’eines d\’*anindexació*. L\’anindexació és un mode de
navegació amb un nombre obert de trajectòries. Atén a aspectes
transversals a les diverses contribucions, de tal manera que en fa
paleses les afinitats o les tensions. En un joc de paraules respecte de
la indexació ---que produeix, registra i classifica les relacions
partint de la possibilitat pretesa d’obtenir una visió general de les
coses---, l\’anindexació és, abans que res, una invitació a llegir
activament des dels marges i a través. La lectura es considera una
pràctica que comprèn tant el bon ús com el mal ús; implica atenció,
inventiva, afecte i projecció. L\’anin-<br />dexació és un acte transformatiu
lligat íntimament i per partida doble al material al qual s\’aplica, ja
que l\’aníndex es veu influït pel mateix material, però, alhora, hi
influeix.

Si la indexació consisteix a obtenir accés mitjançant la il·lusió de la
integritat, l\’anindexació opta pels desplegaments situats, per
despren-<br />dre\’s de la rigidesa i prolongar els relleus; promou una forma
de relacionalitat generativa que no atorga cap control ni indicació,
sinó que dona peu a enjogassar-se imaginativament i embolicar-se cada
vegada més. Els agents que comparteixen els materials no són els únics
que aporten perspectives, sensacions, nocions estètiques o desassossec,
sinó que també ho fa la persona anindexadora. Plegats, contribueixen a
crear un conjunt de recursos explícits per gestionar models de
diversitat, funcionar amb grans absències i orquestrar preguntar
obertes.

L\’anindexació va néixer com un mecanisme relacional la finalitat del
qual és estructurar les relacions entre objectes, qüestions i
esdeveniments d\’una manera no tancada ni fixa. En aquest aníndex
s\’inclouen una sèrie de nanses (*handles*), formes, partitures i traces
que no representen aquesta publicació ni n\’ofe-<br />reixen una visió general,
tot i que en diuen molt. Fan de punt de partida o proposta per travessar
els materials com una nova creació, una invitació a tornar, reaprendre,
funcionar d\’altres maneres i iterar en altres llocs.

------

## NANSES

Es van proposar quatre “nanses” a les persones convidades a col·laborar.
El format d\’aquestes nanses fa referència, entre altres coses, a la
programació, en la qual s\’utilitzen *handles* o “identificadors” per
referir-se de manera abstracta a un recurs gestionat en un altre lloc,
cosa que permet connectar diverses temporalitats i ubicacions anteriors.
Alhora, aquests nanses/identificadors són punts de partida o
d\’ancoratge des dels quals una participant pot evocar idees que adrecen
específicament assumptes relacionats amb el treball en comú, les
tensions de les col·lectivitats o les materialitats de la tasca
creativa. Algunes aportacions remeten explícitament a la nansa de la
qual emanen; d\’altres, en canvi, se centren en les nanses de manera més
implícita.

Sigui com sigui, cada nansa actua com a catalitzador d\’un clúster de
preguntes:

-   Nansa 1: *temps*. Quines temporalitats conflueixen en les formes de
    companyonia? Quins són els durant, els després, els abans i els ja
    de les iniciatives de grup?
-   Nansa 2: *nosaltres*. Com emergeix, es delimita, esdevé un problema o
    es reivindica el sentit del nosaltres? Quins són els límits, els
    modes i les genealogies d\’aquest *nosaltres*?  
-   Nansa 3: *com*. De quines possibilitats materials i semiòtiques es
    disposa per conviure? Com s\’expressen i funcionen, i quines
    interdependències posen en relleu?
-   Nansa 4: *transicions*. Què succeeix en els moments de (o per a la)
    transició, mutació, recomposició, entrega o relleu?

Els col·laboradors van triar una nansa, o diverses, i van fer-la servir
com a fonament per plantejar les seves reflexions, amplificacions o
indagacions.

Finalment, les aportacions d\’aquesta publicació s\’han dut a terme
cercant un equilibri entre imatge i text (per descomptat, es podia
contribuir amb text simple, però s\’admetien igualment diagrames i
contribucions basades en imatges) i la narrativa lineal no va ser un
requisit imprescindible en cap cas.

------

## FORMES

Les formes s\’han emprat per fer atenció a l\’aparença textual i visual
de les aportacions.

-   *Corbes*: es tracen les corbes significatives de cada
    col·laboració. Les corbes poden estar presents visualment o bé
    aparèixer d\’altres maneres, per exemple, mitjançant corbes textuals
    o temporals. Ja en les sinuositats tipogràfiques ja en les
    ondulacions que connecten les imatges o metàfores, hi ha un embull
    de corbes lligades les unes a les altres.
-   *Colors*: es juga amb colors reveladors, que donen un sentit
    particular o que tenen una funció concreta en l\’aportació. Entre
    les accions, s\’inclou trobar colors, injectar-los o combinar-los; o
    bé usar les tonalitats triades per destacar, superposar-se,
    relacionar o anotar.
-   *Text*: es fomenta la implicació textual amb les aportacions
    mitjançant un dels formats de text següents: anècdotes, preguntes,
    etiquetes o entrades de glossari. El text pot usar-se per
    escriure-hi a sota, al costat o a sobre. També és possible refer-ne
    la composició, combinar les textualitats de diverses parts o
    aprofundir-hi gràcies a preguntes, explicacions, arguments, etc.
-   *Absències*: es comprova què falta, se\’n pren nota o s\’avisa.
    Les absències també poden reclamar-se.

## PARTITURES

Les partitures, com a instruccions específiques que van recopilar un
grup d\’anindexadors durant un *book sprint*, assisteixen a tots i totes
les participants posant a la seva disposició eines i nanses. En aquesta
publicació es presenten en versió beta a tall d\’assaig que s\’haurà de
posar a prova i, esperem, reprovar.

<!-- <small>\* Podeu trobar exemples de partitures aquí: xxxxxx</small> -->

## TRACES

Les traces són les marques de contingut enriquit que deixa
l\’anindexador ---un cúmul d\’esbossos, *scripts* o idees que sorgeixen
en un moment d\’anindexació. A qui anindexa, li serveixen de servei
temporal o instrument de processament per facilitar la composició d\’una
partitura.

<framebreak>

## COM S\’ANINDEXA?

L\’anindexació es pot dur a terme de manera concentrada, implicant-se
amb els materials sobre la base de partitures donades o inventades i
aprofitant aquestes formes o altres. Tanmateix, també es pot dur a terme
pausadament, al llarg del temps i de manera continuada. Per fer-ho, es
poden seguir els passos següents:

-   Pas 1: *seleccioneu* dos elements que treballareu en aquesta tanda
    (una nansa/una forma/una aportació).
-   Pas 2: *identifiqueu* l\’element variable (una nansa/una forma/una
    aportació).
-   Pas 3: *executeu* el vostre aníndex.
-   Pas 4: escriviu una *partitura*.
-   Pas 5: afegiu els vostres *comentaris* o *observacions*.


<!-- ## ANINDEXACIÓ VERSIÓ BETA Aquesta versió beta es va produir el febrer de 2020 durant un *book sprint* a la ciutat de Barcelona. Les persones anindexadores presents van ser en Lluís Nacenta, la Nayarí Castillo, la Ludovica Michelin, en Peter Westenberg, la Femke Snelting, la Jara Rocha i la Manetta Berends. -->


X-dizierung
===========

Als Iterationshilfe stellt diese Publikation den Leser\*innen eine Reihe
von x-dexikalischen Werkzeugen zur Verfügung. X-dexing (X-Dizierung) ist
eine Navigationsart mit einer offenen Anzahl von Bewegungsrichtungen.
Sie behandelt transversale Aspekte zwischen mehreren Beiträgen, wodurch
sowohl Gemeinsamkeiten als auch Spannungsfelder deutlich werden können.
Im Gegensatz zur Indizierung, die einen vermeintlichen Überblick über
die Möglichkeiten der Inbeziehungsetzung erzeugt, registriert und
kategorisiert, lädt X-dexing zuvorderst einmal zu einer Form des
engagierten Quer-Lesens ein. Lesen wird hier als Praxis des „richtigen
und des falschen” Umgangs betrachtet, das Aufmerksamkeit, Erfindung,
Zuneigung und Projektion umfasst. X-dexing ist transformativ und
wechselseitig mit dem Material verbunden, auf das es angewandt wird.

Während es beim Indizieren darum geht, durch die Illusion der
Vollständigkeit *Zugang* zu verschaffen, geht es beim X-dex um situierte
Entfaltungen, darum, etwas Fixiertes loszulassen und die Übergabe an die
Ordnung etwas hinauszuziehen. Es ist eine Form der sich verändernden
Beziehungen untereinander, einer generativen Relationalität, die weder
Kontrolle noch Hinweise gibt, sondern mit einer Art Verspieltheit und
fantasievoller Wiederverschränkung aufwartet. Perspektiven, Gefühle,
Ästhetik oder Unbehagen werden nicht nur von den Akteur\*innen
(Autor\*innen), welche die Materialien zur Verfügung stellen, sondern
auch von dem/der in Aktion tretenden X-Dexer\*in präsentiert. Gemeinsam
tragen alle zu einem Instrumentarium für den Umgang mit Differenzmustern
bei, operieren mit Abwesenheiten und ordnen offene Fragen neu.

X-dexing ist zu einem relationalen Instrument geworden, mit dem sich
Beziehungen zwischen Objekten, Fragen und Ereignissen auf eine weder
abgeschlossene noch fixierte Weise strukturieren lassen. Dieses X-dex
enthält Handles, Formen, Partituren und Pfade, welche die Publikation
weder repräsentieren noch einen Überblick über sie geben, auch wenn sie
in vielerlei Hinsicht über sie sprechen. Stattdessen dienen sie als
Ausgangspunkt, als Vorschlag, die Materialien durchzugehen, um etwas
Neues zu gestalten, als Einladung, zurückzukehren, neu zu lernen, anders
zu arbeiten und anderswo zu iterieren:

HANDLES
-------

Die eingeladenen Autor\*innen erhielten vier sogenannte Handles. *Handle* 
ist (unter anderem) ein Begriff aus der Software-Programmierung und
bezeichnet einen abstrakten Verweis auf eine Ressource, mit der an
anderer Stelle agiert wird, wodurch eine Verbindung zu früheren Orten
und Zeiträumen möglich ist. Gleichzeitig sind Handles Ausgangspunkte
oder Anker für weiterführende oder vertiefende Fragestellungen. Einige
Beiträge beziehen sich explizit auf einen Handle, andere wiederum
verwenden Handles eher implizit.

Jeder Handle wirkt als Katalysator für einen Fragen-Cluster:

-   Handle 1: *Zeit*. Welche unterschiedlichen Zeitlichkeiten sind mit
    Formen des Zusammenseins verbunden? Wie sehen Dauer, Vor- und
    Nachspiele und Vorbereitungen von Gruppenunternehmungen aus?
-   Handle 2: *Wir*. Wie wird ein Gefühl des Wir erzeugt, abgegrenzt,
    problematisiert und/oder neu beansprucht? Welche Grenzen, Formen und
    Genealogien hat dieses Wir?
-   Handle 3: *Wie*. Welche materiellen und semiotischen Möglichkeiten der
    Koexistenz bestehen? Wie werden diese artikuliert, wie funktionieren
    sie und welche Interdependenzen werden sichtbar gemacht?
-   Handle 4: *Übergänge*. Was passiert in Momenten des Übergangs, der
    Mutation, der Neuordnung, der Übergabe und der Übernahme?

Die Teilnehmer\*innen wählen einen oder mehrere Handles aus und stellen
davon ausgehend Überlegungen an, versuchen sich an Erweiterungen und
formulieren Fragen dazu.

Bei Erstellung der Beiträge dieser Publikation wurde auf ein
Gleichgewicht aus Text und Bild geachtet (d.h. man konnte einen
Textbeitrag einsenden, oder auch Diagramme oder bildbasierte Beiträge),
und die Beiträge mussten keine li-<br />neare Erzählform aufweisen.

FORMEN
------

*Formen* werden verwendet, um das visuelle und textliche
Erscheinungsbild der Beiträge darzustellen.

-   *Kurven*: Verfolgen der Kurven, die für einen Beitrag von
    Bedeutung sind. Kurven können optisch oder in anderer Form
    auftreten, zum Beispiel als Text- oder Zeitkurven. Das kann die
    Kurve eines typografischen Wortes sein oder eine Kurve, die Bilder
    oder Metaphern miteinander verbindet, eine reiche Verflechtung
    geschwungener Linien.
-   *Farben*: Auseinandersetzung mit Farben, die aussagekräftig sind,
    die eine bestimm-<br />te Bedeutung ent-<br />wickeln können oder eine bestimmte
    Rolle im Beitrag spielen. Mögliche Aktionen sind das Finden, das
    Injizieren, das Kombinieren von Farben usw. Farben zum Hervorheben,
    Überkreuzen, Verknüpfen oder Kommentieren.
-   *Text*: Herangehen an die Beiträge durch eines der folgenden
    textbasierten Formate: Anekdoten, Fragen, Etiketten oder
    Glossareinträge. Der Text kann darunter, daneben oder überschrieben
    werden. Er kann auch anders angeordnet werden, wodurch zum Beispiel
    Textinhalte verschiedener Teile kombiniert oder in Form von Fragen,
    Erklärungen, Kontrapunkten usw. zur Vertiefung verwendet werden
    können.
-   *Auslassungen*: Nachschauen, was fehlt, es anmerken oder
    markieren. Auch Auslassungen oder Löschungen können eingefordert
    werden.

SCORES
------

*Scores* sind spezifische Anweisungen, Partituren, die während eines
redaktionellen Sprints von einer Gruppe von X-Dexer\*innen gesammelt und
mit den zur Verfügung stehenden Werkzeugen und Handles auf alle Beiträge
angewendet wurden. In dieser Publikation erscheinen sie in einer
Beta-Version; als Experimente, die getestet und hoffentlich angefochten
werden.

<small>\* Beispiele für Scores gibt es am Ende dieser Abschnitt.</small>

TRACES
------

*Traces* sind mediale Spuren, die der/die Operator\*in des X-dex
hinterlässt, eine Ansammlung von Skizzen, Skripten und Ideen, die aus
einem X-dexing-Moment entstehen. Sie dienten dem/der X-Dexer\*in als
temporäre Hilfsprogramme, als Ver-<br />arbeitungswerkzeuge beim Erstellen eines Scores.

<framebreak>

WIE FUNKTIONIERT X-DEXING?
--------------------------

*X-Dexing* ist in konzentrierter Form möglich, indem man von Materialien
mit vorgegebenen oder erfundenen Scores ausgeht und die hier genannten
oder auch andere Formen verwendet. Man kann es aber auch über einen
längeren Zeitraum, kontinuierlich betreiben. Die folgenden Schritte
dienen als Orientierung:

-   Schritt 1: *Auswahl* von zwei Elementen, die in diesem Durchgang
    fixiert werden sollen (Handle / Form / Beitrag)
-   Schritt 2: *Identifizieren *des lockeren Elements (Handle / Form /
    Beitrag)
-   Schritt 3: *Durchführen* des x-dex!
-   Schritt 4: *Score* schreiben
-   Schritt 5: *Kommentare* und *Beobachtungen* hinzufügen


<!-- BETA-VERSION VON X-DEXING -------------------------- Diese Betaversion entstand im Februar 2020 während eines Book Sprint in Barcelona. Die teilnehmenden X-Dexer\*innen waren Lluís Nacenta, Nayarí Castillo, Ludovica Michelin, Peter Westenberg, Femke Snelting, Jara Rocha, Manetta Berends. -->


X-DEXING
========

By way of an \“iterating\” device, this publication provides readers
with a set of x-dexical tools. X-dexing is a mode of navigation with an
open number of trajectories. It attends to transversal aspects
in-between multiple contributions, making common grounds or tensions
emerge. In a play on the way \“indexing\” produces, registers and
categorises relations from the assumed possibility of having an
overview, \“x-dexing\” is first of all an invitation to an engaged
reading from the sides-and-through. Reading here is considered as a
practice of handling and mishandling; it includes attention, invention,
affection, and projection. X-dexing is transformative and in a two-way
manner intimately connected to the material it is applied to: the x-dex
is influenced by the material itself, but provokes an effect on it as
well.

If \“indexing\” would be about gaining *access* through the illusion of
completeness, the x-dex is about situated unfoldings, about letting go
of fixitude and about handing over for a little longer; a form of
generative relationality that is not providing with control nor
indication, but a sort of playfulness and imaginative re-entanglement.
Perspectives, feelings, aesthetics or uneasiness are not only brought to
the table by the agents that share materials, but by the emergent
x-dexer as well. Together they contribute to an explicit toolset for
handling difference patterns, operate with worldly absences, and score
open questions.

X-dexing emerged as a relational device to structure relations between
objects, questions and events in a way that is not closed or fixed.
Included in this x-dex are handles, forms, scores and traces that do not
represent nor give an overview of the publication, even if they speak
about it in many ways. They act as departure points, a proposition to
traverse the materials as a new making, an invitation to return to,
relearn from, operate otherwise and iterate elsewhere:

HANDLES
-------

Four handles were given to the invited contributors. The format of the
*handle* refers (amongst others) to computer programming where a handle
is used as an abstract reference to a resource which is taken care of
elsewhere, therefore allowing to connect to previous locations and
temporalities. At the same time, the handles are departure points or
anchors from where a contributor could evoke ideas that specifically
attend to questions about working together, tensions of collectivities,
or materialities of creative work. Some contributions refer explicitly
to a handle they were thought from, some others take the handles more
implicitly.

Each handle catalyzes a cluster of questions:

-   Handle 1: *Time*. Which are the temporalities involved in forms of
    togetherness? What are the durings, the afters, the befores, the
    alreadies of groupal endeavors?
-   Handle 2: *We*. How is a sense of \“we\” emerging, being delimited,
    problematized and/or reclaimed? What are the limits, the modes and
    the genealogies of those we’s?
-   Handle 3: *How*. What material and semiotic possibilities for
    co-existance? How are they articulated, how do they function and
    which interdependencies are made evident?
-   Handle 4: *Transitions*. What happens in moments for/of transition,
    mutation, rearrangement, handing over or taking on?

Contributors chose one or several handles and departed from them to make
their reflections, try amplifications or formulate their inquiries.

The contributions in this publication were fabricated with a text-image
equilibrium in mind (e.g.: of course one option was to contribute with
plain text, but also diagrams or image-based contributions were
welcome), and linear narrative was not a must neither.

FORMS
-----

*Forms* are used to attend to the visual and textual appearance
contributions.

-   *Curves*: Tracing the curves that are significant for a
    contribution. Curves can be visually present but can also appear in
    other forms, for example textual or temporal curves.From the curves
    of a typographic word to those that connect images or metaphors,
    there is a rich intertwining of curved lines in there.
-   *Colors*: Engaging with colors that are telling, develop a
    specific meaning, or play a specific role in the contribution.
    Actions can include finding colors, injecting color, combining
    colors, etc. Or: use chosen colors to highlight, cross over, link or
    annotate.
-   *Text*: Engaging with the contributions textually through one of
    the following text-based formats: anecdotes, questions, tags or
    glossary entries. Text can be used to write under, aside or on top.
    It can also be rearranged differently, to combine textualities of
    different parts; or deepened in the form of questions, explanations,
    counterpoints etc.
-   *Absences*: Checking what is missing, and noting or signaling it.
    Absences can also be reclaimed.

TRACES
------

Traces are rich-media marks left by the operator of the x-dex, a
pile of sketches, scripts or ideas from a moment of
x-dexing. They have functioned as temporary utilities for the x-dexer,
as processing tools to support the writing of a score.

SCORES
------

As specific instructions collected during an editorial sprint by a group
of x-dexers, *scores* attend to all contributions with the tools and
han-<br />dles at hand. In this publication they appear in a beta-version; as
experiments to be tested and hopefully contested.

<small>\* Examples of scores can be found at the end of this booklet.</small>

HOW TO X-DEX?
-------------

X-dexing can happen in a concentrated manner, engaging with materials, given scores or invented ones, using these forms or others. But it
can also be operated slowly, along time, in an ongoing manner. The
following steps can be followed:

-   Step 1: *select* two elements that you will fix for this round (a
    handle / a form / a contribution)
-   Step 2: *identify* your loose element (a handle / a form / a
    contribution)
-   Step 3: *operate* your x-dex!
-   Step 4: write a *score*
-   Step 5: add your *comments* or *observations*

EXDEXA-<br />CIÓN
==========

En tanto que artefacto \“iterador\”, esta publicación ofrece a les
lectores un conjunto de herramientas de exdexación. La exdexación es un
modo de navegación con un número abierto de trayectorias. Atiende a
aspectos transversales a las múltiples contribuciones, haciendo que
surjan las bases comunes o las tensiones. Haciendo un juego de palabras
o conceptos con la forma en que la \“indexación\” produce, registra y
categoriza relaciones partiendo de la supuesta posibilidad de obtener
una visión general, la \“exdexación\” es antes que nada una invitación a
una lectura participativa desde los lados y a través. Consideramos leer
como una práctica de manejar bien y no tan bien; incluye atención,
inventiva, implicación y proyección. La exdexación es transformadora y
está estrechamente vinculada, por partida doble, al material al que se
aplica: el \“éxdice\” está influido por el propio material, pero también
provoca un efecto en el mismo.

Si la \“indexación\” consiste en obtener *acceso* mediante una ilusión
de compleción, el éxdice trata de despliegues situados, de desaprender
la rigidez y de demorarse, dejándose relevar; una forma de
relacionalidad generadora que no consiste en ofrecer control ni
indicación alguna, sino en dar pie a retozar imaginativamente, a
enmarañarse más y más. Les agentes que comparten los materiales no son
les úniques que aportan perspectivas, sentimientos, estética o
desasosiego, sino que también lo hacen las exdexadoras. Juntes
contribuyen a un conjunto explícito de herramientas para manejar los
patrones de diferencias, operar con ausencias mundanas y registrar
preguntas abiertas.

La exdexación surgió como un artefacto relacional para estructurar las
relaciones entre objetos, cuestiones y eventos de una manera que no
fuera cerrada ni fija. En este éxdice se incluyen asideros (*handles*),
formas (*forms*), partituras (*scores*) y rastros (*traces*) que no
representan ni ofrecen una visión general de esta publicación, aunque
hablen de ella de muchas maneras. Actúan como puntos de partida,
propuestas para atravesar los materiales como una nueva creación, una
invitación a regresar, volver a aprender, operar de otras maneras e
iterar en cualquier parte:

ASIDEROS
--------

Se propusieron cuatro asideros a las personas invitadas a contribuir. El
formato de *asidero* se refiere (entre otras cosas) a la programación
informática, donde se usa un asidero (\“handle\”) como referencia
abstracta a un recurso que se trata en otro lugar, y por consiguiente
permite conectar con ubicaciones y temporalidades previas. Al mismo
tiempo, los asideros son puntos de partida o anclas, desde donde une
participante puede evocar ideas que responden específicamente a
cuestiones relacionadas con el trabajo en común, las tensiones de la
colectividad o los materiales del trabajo creativo. Algunas
contribuciones se refieren explícitamente al asidero desde el que
partieron, otras abordan los asideros de manera más implícita.

Cada asidero cataliza un conglomerado de preguntas:

-   Asidero 1: *Tiempo*. ¿Cuáles son las temporalidades implicadas en
    formas de unidad? ¿Cuáles son los “durante”, los “después”, los “antes”,
    los “ya” de los esfuerzos grupales?
-   Asidero 2: *Nosotres*. ¿De qué manera surge, se delim-<br />ita, se convierte
    en problema y/o se reclama un sentido de \“nosotres\”? ¿Cuáles son
    los límites, los modos y las genealo-<br />gías de ese \“nosotres\”?
-   Asidero 3: *Cómo*. ¿Qué posibilidades materiales y semióticas hay de
    coexistir? ¿Cómo se articulan, cómo funcionan y qué
    interdependencias ponen de relieve?
-   Asidero 4: *Transiciones*. ¿Qué ocurre en los momentos para/de
    transición, mutación, recomposición, entrega o toma del relevo?

Les participantes escogen uno o varios asideros y parten de él para
plantear sus reflexiones, intentar amplificaciones o formular sus
indagaciones.

Las contribuciones a esta publicación se realizaron teniendo presente un
equilibrio texto-imagen (por supuesto era posible contribuir con un
texto corrido, pero se aceptaban de buena gana igualmente diagramas o
contribuciones basadas en imágenes); la narrativa lineal tampoco era
necesaria.

FORMAS
------

Las *formas* se usan para atender a la apariencia visual y textual de
las contribuciones.

-   *Curvas*: Trazar las curvas que tienen relevancia para una
    contribución. Las curvas pueden estar presentes visualmente, pero
    también pueden aparecer de otras formas, por ejemplo curvas
    textuales o temporales. Desde las curvas tipográficas de una palabra
    hasta aquellas que conectan imágenes o metáforas, hay una maraña de
    curvas entrelazadas.
-   *Colores*: Involucrarse con colores que están contando algo,
    desarrollando un significado específico o que desempeñan un papel
    concreto en la contribución. Las acciones incluyen encontrar
    colores, inyectar color, combinar colores, etc. O bien usar los
    colores escogidos para destacar, atravesar, relacionar o anotar.
-   *Texto*: Implicarse con las contribuciones de manera textual por
    medio de uno de los siguientes formatos de texto: anécdotas,
    preguntas, etiquetas o entradas del glosario. El texto se puede usar
    para escribir debajo, encima o al lado. También es posible rehacer
    la composición, combinar textualidades de distintas partes; o
    profundizar en forma de preguntas, explicaciones, contraargumentos,
    etc.
-   *Ausencias*: Comprobar qué es lo que falta, comentarlo o
    señalarlo. Las ausencias también se pueden reclamar.

PARTITURAS
----------

Como instrucciones específicas recopiladas durante el *book sprint* por
un grupo de exdexadores, las *partituras* asisten a todas las
participantes, teniendo a mano las herramientas y asideros. En esta
publicación aparecen en versión beta, como experimentos por probar y,
esperamos, reprobar.

<small>* Ejemplos de los scores al final de este folleto.</small>

RASTROS
-------

Los rastros son marcas de *rich media* que la persona exdexadora deja: una
batería de bocetos, guiones o ideas que surgen de un momento de
exdexación. Funcionan como utilidad temporal para quien exdexa, como
herramientas de procesado para apoyar la escritura de una partitura.

<framebreak>

¿CÓMO EXDEXAR?
--------------

La exdexación se puede realizar de manera concentrada, implicándose con
los materiales en base a partituras dadas o inventadas, usando estas
formas u otras. Pero también se puede realizar lentamente, a lo largo
del tiempo, de manera continua. Se pueden seguir los siguientes pasos:

-   Paso 1: *seleccionar* dos elementos que fijarás para esta vuelta
    (un asidero / una forma / una contribución)
-   Paso 2: *Identificar* el elemento laxo (un asidero / una forma /
    una contribución)
-   Paso 3: ¡*opera* tu éxdice!
-   Paso 4: escribe una *partitura*
-   Paso 5: añade tus *comentarios* u *observaciones*

<!-- EXDEXACIÓN - VERSIÓN BETA ------------------------- Esta versión beta se ha producido durante un *book sprint* realizado en Barcelona en febrero de 2020. Las personas exdexadoras presentes fueron Lluís Nacenta, Nayarí Castillo, Ludovica Michelin, Peter Westenberg, Femke Snelting, Jara Rocha, Manetta Berends. -->

X-DEXAGE
========

Au moyen d’un dispositif « itérant », cette publication fournit aux
lecteur·rice·s un ensemble d’outils x-diciel. L’X-dexage est un mode de
navigation avec un nombre ouvert de trajectoires. Il s’occupe des
aspects transversaux entre contributions multiples, faisant émerger des
terrains communs ou des tensions. Dans un jeu sur la façon dont
« l’indexage » produit, enregistre et catégorise les relations à partir
de la possibilité supposée d’obtenir une synthèse, l’« x-dexage » est
avant tout une invitation à une lecture engagée de tous les côtés. La
lecture est ici considérée comme une pratique de gestion et mauvaise
gestion, elle comprend l’attention, l’invention, l’affection et la
projection. L’X-dexage est une manière transformatrice et
bidirectionnelle intimement connectée au matériel à laquelle elle
s’applique : l’x-dex est influencé par le matériel lui-même, mais
provoque également un effet dessus.

Si « l\’indexation » concerne l’obtention d’un *accès* au travers de
l’illusion de la complétude, l’x-dex a à voir avec des déroulements
situés, avec l’abandon de la fixité et le passage de témoin pour un peu
plus de temps ; une forme de réciprocité générative qui n’apporte ni
contrôle ni indication, mais une sorte de ludisme et ré-enchevêtrement
imaginatif. Les perspectives, les sentiments, l’esthétique ou le malaise
ne sont pas seulement mis sur la table par les agents partageant les
matériels, mais également par l’x-dexeur·se. Ensemble, iels contribuent
à des outils explicites de traitement des schémas de différence, opèrent
avec les absences matérielles et enregistrent des questions ouvertes.

L’X-dexage apparaît comme un dispositif relationnel permettant de
structurer les relations entre objets, questions et événements d’une
façon qui n’est ni fermée, ni fixe. Cet x-dex contient des poignées,
formes, partitions et traces qui ne représent-<br />ent ni ne donnent d’aperçu
de la publication, même si ces éléments en parlent de nombreuses façons.
Elles agissent comme points de départ, comme une proposition de
traversée des matériaux comme une nouvelle création, une invitation <br />à
revenir, à réapprendre auprès de/à partir de, à opérer autrement et à
itérer ailleurs :

<framebreak>

POIGNÉES
--------

Quatre poignées ont été données aux contributeur·rice·s invité·e·s. Le
format de la *poignée* fait référence (entre autres) à la programmation
informatique où une poignée est utilisée comme référence abstraite pour
une ressource qui est traitée ailleurs, permettant donc de connecter les
emplacements et temporalités précédentes. En même temps, les poignées
sont des points de départ ou des ancres à partir desquelles un·e
contributeur·rice peut évoquer des idées abordant les questions du
travail ensemble, des tensions des collectivités ou des matérialités du
travail créatif. Certaines contributions font une référence explicite à
une poignée à partir de laquelle elles ont été pensées, d’autres
envisagent les poignées de façon plus implicite.

Chaque poignée catalyse une grappe de questions :

-   Poignée 1 : Le temps. Quelles sont les temporalités impliquées dans
    les formes d’union et de solidarité ? Quels sont les « durant », les
    « après », les « avant », les  « déjà » des efforts de groupe ?
-   Poignée 2 : Nous. Comment un sens du « nous » émerge-t-il, est-il
    délimité, problématisé, et/ou récupéré ? Quelles sont les limites,
    les modalités et les généalogies de ces nous ?
-   Poignée 3 : Comment. Quels matériaux et possibilités sémiotiques
    pour la co-existence ? Comment sont-ils articulés, comment
    fonctionnent-ils et quelles interdépendances sont rendues
    manifestes ?
-   Poignée 4 : Transitions. Qu’arrive-t-il dans des moments de/pour la
    transition, mutation, réorganisation, transmission ou prise en
    charge ?

Les contributeur·rice·s ont choisi une ou plusieurs poignées et s’en sont
écarté·e·s pour effectuer leurs réflexions, essayer des amplifications
ou formuler leurs interrogations.

Les contributions de cette publication ont été fabriquées selon un
équilibre texte-image en tête (par ex. : bien entendu, une option était
de contribuer avec un texte simple, mais des diagrammes ou des
contributions fondées sur des images étaient également les bienvenus), 
et la narration linéaire n’était pas non plus recommandée.

FORMES
------

Les *Formes* sont utilisées pour aborder l’apparence visuelle et
textuelle des contributions.

-   *Courbes* : tracer les courbes qui font sens pour la contribution.
    Les courbes peuvent être présentes visuellement, mais également
    apparaître sous d’autres formes, par exemple des courbes textuelles
    ou temporelles. Depuis les courbes de la typographie d’un mot à
    celles qui connectent les images ou les métaphores, il existe un
    riche entrelacement de lignes courbes.
-   *Couleurs* : engager un dialogue avec des couleurs évocatrices,
    qui développent un sens spécifique ou jouent un rôle spécifique dans
    la contribution. Les actions peuvent comprendre la recherche de
    couleurs, l\’injection de couleurs, la combinaison de couleurs, etc.
    Ou : faire usage de couleurs pour souligner, traverser, lier ou
    commenter.
-   *Texte* : s’impliquer textuellement dans les contributions par le
    <br />biais de l’un des formats suivants ayant le texte pour fondement :
    anecdotes, questions, étiquettes ou rubriques de glossaire. Le texte
    peut être utilisé pour écrire dessous, à côté ou par-dessus. Il peut
    également être réagencé différemment, pour combiner les textualités
    de différentes parties ; ou approfondi sous la forme de questions,
    explications, contrepoints, etc.
-   *Absences* : vérifier ce qui manque et le noter ou le signaler.
    Les absences peuvent également être récupérées.

PARTITIONS
----------

En tant qu’instructions spécifiques recueillies lors d’un *sprint*
éditorial par un groupe d’x-dexeur·ses, les *partitions* s’occupent de
toutes les contributions avec les outils et les poignées à portée de
main. Dans cette publication, elles apparaissent en version-bêta, comme
des expériences à tester et à contester, avec un peu de chance.

<small>\* Exemples de les partitures al final d’aquest follet.</small>

<framebreak>

TRACES
------

Les traces sont les marques de médias interactifs laissées par
l’opérateur·rice de l’x-dex, une pile de croquis, scripts ou idées qui
émergent d’un moment d’x-dexage. Elles ont fonctionné comme équipements
temporaires pour l’x-dexeur·se, comme instruments de traitement pour
appuyer l’écriture d’une partition.

<!-- <framebreak> -->

COMMENT X-DEXER ?
-----------------

L’X-dexage peut avoir lieu de façon concentrée, entrer en contact avec
des matériaux dotés de partitions données ou inventées, utiliser ces
formes ou d’autres. Il peut également s’effectuer lentement, avec le
temps, de façon continue. Les étapes suivantes peuvent être employées :

-   Étape 1 : *sélection* de deux \
    éléments que vous \
    réparerez pour ce tour (une poignée / une \
    forme / une contribution)
-   Étape 2 : *identification *de \
    votre élément lâche \
    (une poignée / une forme / une contribution)
-   Étape 3 : *mise en marche \
    *de votre x-dex !
-   Étape 4 : écriture d’une \
    *partition*
-   Étape 5 : ajout de vos *commentaires* ou *observations*


X-DEXEREN
=========

Bij wijze van Iteratief gereedschap biedt deze publicatie een set van
x-dexical hulpmiddelen aan. <br />X-dexeren is een manier om te navi-<br />geren
waarbij het aantal mogelijke trajecten open is. Het vraagt aandacht voor
transversale aspecten tussen meervoudige bijdragen, en creëert
gemeenschappelijke raakvlakken of spanningen. Als een spel met de manier
waarop een index relaties creëert, registreert en categoriseert, vanuit de veronderstelling dat overzicht mogelijk is. X-dexeren is in de eerste
plaats een uitnodiging tot geëngageerd lezen, van de ene kant naar de
andere en dwars erdoorheen. Lezen wordt opgevat als een praktijk van
hanteren en mis-handelen; het bestaat uit aandacht besteden, iets
uitvinden, uit affectie en projectie. X-dexeren is transformatief en op
een dubbele manier nauw verbonden met het materiaal waarop het wordt
toegepast: de x-dex wordt beïnvloed door het materiaal zélf, maar
genereert ook een effect op dat materiaal.

<framebreak>

Als “indexeren” gaat over het verkrijgen van *toegang* gebaseerd op een
illusie van volledigheid, dan gaat de x-dex over het ontvouwen van het
gevestigde, over het loslaten van vastigheid en het nog iets langer uit
handen geven; een vorm van generatieve relationaliteit die geen controle
of indicatie biedt, maar die op een speelse en imaginaire manier dingen
opnieuw met elkaar verstrengelt. Perspectieven, gevoelens, esthetiek of
onbehagen worden niet enkel door diegenen die materiaal in deze
publicatie delen op tafel gelegd, maar ook door de x-dexer in wording.
Samen vormen ze een expliciete hulpmiddelen-set om met verschillende
patronen om te gaan, om te werken met wat niet aanwezig is en om scores
te maken op basis van open vragen.

X-dexeren ontstond als een apparatus om relaties tussen objecten, vragenhandvat
en gebeurtenissen open te structureren, op een manier die niet van
tevoren is vastgelegd. In de x-dex zijn handvatten, vormen, scores en
sporen opgenomen die niets representeren noch een overzicht geven van de
publicatie, zelfs al spreken ze erover op vele manieren. Ze functioneren
als startpunt, een voorstel om het materiaal te doorkruisen als iets
nieuws-in-de-maak, een uitnodiging om terug te komen, om er opnieuw van
te leren, om het anders te doen en ergens anders te itereren.

<framebreak>

Handvatten
----------

Aan de deelnemers aan de publicatie werden vier handvatten gegeven. Het
format van het *handvat* verwijst (onder andere) naar
computerprogrammering waarbij de term gebruikt wordt als een abstracte
verwijzing naar een hulpbron waar ergens anders zorg voor wordt
gedragen, en die het mogelijk maakt verbindingen te leggen tussen
eerdere locaties en tijdstippen. Tegelijkertijd zijn de handvatten
startpunten of ankers. Ze kunnen ideeën genereren die zijn gerelateerd
aan kwesties over samenwerken, spanningen bij collectiviteiten of de
materialiteit van creatief werk. Sommige bijdragen refereren expliciet
naar het handvat van waaruit ze bedacht zijn, in andere zijn de
handvatten meer impliciet.

Ieder handvat katalyseert een cluster van vragen:

-   Handvat 1: *Tijd*. Welke temporaliteiten zijn be-<br />trokken bij 
    vormen van samenzijn? Wat zijn de gedurende’s, de erna’s, de daarvoor’s, de nu al’s van groepsinspanningen?
-   Handvat 2: *Wij*. Hoe komt een gevoel van “wij” naar boven, hoe wordt
    het afgebakend, geproblematiseerd en / of opgeëist? Wat zijn de
    limieten, de vormen en de genealogieën van die wij’s?
-   Handvat 3: *Hoe*. Welke materiële en semiotische mogelijkheden zijn er
    voor co-existentie? Hoe worden ze verwoord, hoe functioneren ze en
    welke onderlinge afhankelijkheden worden duidelijk gemaakt?
-   Handvat 4: *Transities*. Wat gebeurt er in momenten voor <br />/ van
    transitie, mutatie, herschikking, overdracht of overname?

Deelnemers kozen één of meerdere handvatten en gingen daarmee aan de
slag. Ze probeerden deze te verbuigen of hun vragen openieuw te formuleren.

De bijdragen van deze publicatie werden gemaakt met het idee een
evenwicht te vinden tussen het tekst en het beeldgedeelte (het was
natuurlijk een optie om bij te dragen met een tekst maar ook diagrammen
of beeldgebaseerde bijdragen waren welkom), en een lineair relaas was
ook geen absolute vereiste.

VORMEN
------

*Vormen* worden gebruikt om aandacht te kunnen besteden aan de visuele
en tekstuele verschijningsvorm van de bijdragen.

-   *Rondingen*: Het traceren van de rondingen die belangrijk zijn
    voor een bijdrage. Rondingen kunnen visueel aanwezig zijn maar ook
    verschijnen in andere vormen, bijvoorbeeld bij tekstuele of
    tijde-<br />lijke rondingen. Van de rondingen van een typografisch woord
    tot de rondingen die beelden of metaforen met elkaar verbinden: er
    is een uitgebreide verstrengeling van ronde lijnen aanwezig.
    betekenis ontwikkelen, of een specifieke rol spelen in de bijdrage.
    Acties kunnen bestaan uit het vinden van kleuren, het injecteren van
-   *Kleuren*: Omgaan met kleuren die veelzeggend zijn, een specifieke
    kleur, het combineren van kleuren, enz. Of: gebruik de gekozen
    kleu-<br />ren om te accentueren, te kruisen, te linken of te annoteren.
-   *Tekst*: Omgaan met tekstuele elementen door middel van de
    volgende tekst gebaseerde formats: anekdotes, vragen, tags of
    terminologie-notities. Tekst kan worden gebruikt om onder, naast of
    er overheen te schrijven. Het kan ook anders worden gerangschikt, om
    zo de tekstuele kenmerken van verschillende onderdelen te
    combineren; of te verdiepen in de vorm van vragen, toelichtingen,
    contrapunten etc.
-   *Absenties*: Nagaan wat er ontbreekt, omissies noteren of
    signaleren. Absenties kunnen ook worden toegeëigend.

SCORES
------

De scores werden als specifieke instructies verzameld tijdens een
redactionele sprint door een groep x-dexers. Ze dragen bij aan alle
bijdragen met de hulpmiddelen en handvatten bij de hand. In deze
publicatie verschijnen ze in een beta-versie; als experimenten om te
testen en hopelijk verder te bespreken.

<small>\* Voorbeelden van scores zijn te vin-<br />den aan het einde van dit katern.</small>

SPOREN
------

Sporen zijn multi-media tekens achtergelaten door de operator van de
x-dex: een stapel schetsen, scripts en ideeën die opkwamen tijdens het
x-dexen. Ze hebben gefunctioneerd als tijdelijk hulpmiddel voor de
x-dexers, als verwerkingsgereedschap om het proces van score-schrijven
te ondersteunen.

HOE TE X-DEXEN?
---------------

X-dexeren kan op een geconcentreerde manier worden gedaan, met behulp van
al het gegeven materiaal, met bestaande of verzonnen scores,
gebruikmakend van de ene vorm of van een andere. Maar het kan ook
langzaam worden gedaan, in de loop van de tijd, op een doorlopende
manier. De volgende stappen kunnen worden gevolgd:

-   Stap 1: *selecteer* twee elementen die je voor deze ronde vastlegt
    (een handvat / een vorm / een bijdrage)
-   Stap 2: *identificeer* je losse element (een handvat / een vorm /
    een bijdrage)
-   Stap 3: *ga aan de slag* met je <br />x-dex!
-   Stap 4: schrijf een *score*
-   Stap 5: voeg je *commentaar* <br />of *opmerkingen* toe


<!-- X-dexeren BETA-VERSION --------------------- Deze beta-versie werd geproduceerd tijdens een boeksprint in Barcelona in februari 2020. De aanwezige x-dexers waren Lluís Nacenta, Nayarí Castillo, Ludovica Michelin, Peter Westenberg, Femke Snelting, Jara Rocha, Manetta Berends. -->

-----------

# X-dexing <br />BETA-VERSION