% Scores
% common ground

# scores

Ear Worm sound rotating round the space. Dynamics increasing and decreasing over time. Vibrations of the floor are picked up by an earthquake sensor adding further sound to the soundscape. Soil is put on the floor. The diagram on upper left corner shows growing of plants during the exhibition’s time span. Slogans and ideas are written on the windows.

Within this composition we intend to project different musical reflections upon our collective practice and the exhibition within the Iteration in Graz into the concrete physical space. Intentionally, we started from different compositional approaches, to destabilize the dominant Image of the **authentic** Composer, whose **individual** work is stylistically **coherent**. 

We hereby &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; oppose (propose) a spatial conception, that might be **inconsequent**, but therefore **open** for Interpretation and **Improvisation**.

![](images/scoreESC-1.jpg)

![](images/scoreESC-2.jpg)

![](images/scoreESC-3.jpg)

![](images/scoreESC-4.jpg)

![](images/scoreESC-5.jpg)

![](images/scoreESC-6.jpg)

![](images/scoreESC-7.jpg)

![](images/scoreESC-8.jpg)