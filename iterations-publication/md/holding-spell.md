% holding spell
% Kym Ward 

# holding spell

<small>We call on you elemental Earth \
To give us \
Fermentation and fertility
In the style of bottom friction \
</small>

<small>
Depart in peace elemental earth \
Our blessings take with you
</small>

To be enchanted, So Mayer says, in their introduction to *Spells -
21^st^ Century Occult Poetry,* happens at intersectional moments. When
we meet each other: *Enchantee! Encantadx!* The convergence of two or more
new worlds. It can happen when taking care in the words we use, at the
intersection of wording and worlding: making magic palpable by bringing
what was not there before, into being.

Enchantment can happen anachronistically -- falling in love with your
people, subversively through history, can show us ways of being today.
The witch, the feminist, the differently (dis/) abled person, trans
people and anarcho-<br />outsiders, can form communities of affiliation, whole
life-spans apart, through appreciating the different cloaks they’d have
had to wear.

This practice -- of enchanting and being enchanted - finds one of it’s
forms in rituals of spellcraft. Now writing as “I”, and only giving
small glimpses of very private practices I’ve had the fortune to be part
of, I’ll share some thoughts on when magic worked. By that I mean --
when the converging of a group, who were either willing, antagonistic,
bemused, sceptical or enthusiastic by turns -- gave of themselves, just
a small amount, to participate. And that mode of sharing, the loose
formality and insurgency of practices connected to witchcraft, produced
it’s own momentary occultism, which had a real effect on communication,
on visibility of issues, on correspondent feelings within a group.

<small>We call on you elemental Air \
To give us \
Breath & oxygenation
In the style of a storm surge \
</small>

<small>
Depart in peace elemental Air \
Our blessings take with you
</small>

Circlework as a communal practice, is envi-<br />ronment-responsive, in that it
historically takes place at cyclical moments. This can be in response to
the waxing or waning moon, or to the celebration of a festival connected
to changing seasons. The chosen moment has import, this one time is the
only time, and also one of infinite similar moments which will flow
after we have stopped.

The schema to follow -- open the circle / cast the spell / close the
circle, creates a secretive inside and outside of a practice, of
temporal community disbanded after the ritual ends. It can be as
spiritual or community-focused as its practitioners. Embedded in
practices I’ve seen, is a reciprocal gratitude and wonder at the
formation of the universe, and an awareness of what and how you “take”:
from others, from the world around you.

<small>We call on you elemental fire \
To give us \
Solar radiation that intensifies evaporation
In the style of S2 \
</small>

<small>
Depart in peace elemental fire \
Our blessings take with you
</small>

I’ve been included in ritualistic practice that, with lightness and
laughter, was malleable and could have fit many different groups. And in
larger group-work, where 50 people magnified an organised solemnity. At
times, debate has arisen slap bang in the middle: should we really do
the Trump disappearing spell, if we take into account the Wiccan belief
of doing no harm? Alarm as someone described the rule of return, which
states that a hex comes back to haunt you threefold, could have had
coven-splitting proportions.

I’ve seen people dissolving in tears of laughter at the Earth signs
performing earthiness, as part of casting the circle (the group
characteristic involved a lot of stamping, huffing and snorting). And
there have been shared weights - taking seriously the display and
enactment of care that holding a ritual entails. That we know that when
we make space for one participant’s grief, trouble or heartache, the
structure of the ritual can enable us to “feel with” pain. That
attention, which has a connection to therapy, literally facing inwards
towards the middle of the circle with the issue at the centre focuses
emotional direction. At the same time, I experience the configuration as
showing a respect for the gap between different people’s experiences.
We’re working on an issue that might not be ours, holding open the
possibility of real concentration and mutuality.

<small>We call on you elemental water \
To give us \
Life happiness in freak waves
In the style of M4 \
</small>

<small>
Depart in peace elemental water \
Our blessings take with you
</small>

The suppleness of words used in spellcraft is an accessible poetical
form, which can bind the group in self-reflexive understanding of it’s
own vernacular. This poetry uses what is excessive in language, it can
multiply meaning, dissolve words into sound and compose words as images,
to make space for creativity, intuition, visualisation and meditation.
There are either four or five elements, according to variations in
Eastern and Western philosophy: Earth, Air, Fire and Water, and in
Chinese tradition -- Metal, with Vedic traditions including “aether”, or
void/space, which structure and ground the ritual in location and
compass direction. The elements also serve as social imaginary of form,
essence and mutation, together with an accompanying sensation. In their
symbolic abstraction, they move away from physicality, at the same time
being a celebration of wonder in the physical world. When it works, the
magic is in these hidden resources: within the words, between the
worlds, that gets magnified by the collective acknowledgement of a will
to manifest something different.

<small>Opening the circle and closing the circle \
Esbat for the Ice Moon, February 2020 \
Cast by Political Economists, Oceanographers, Artists, Designers & Data Scientists \
Modelling Waves & Swerves Workgroup</small>

------------

> we share a rich vocabulary; us.  \
> we can speak of  \
> broad skies and magnitudes,  \
> the sound of a blown voice,  \
> through a horn softly \
> on a hill far away, \
> of magnetite swathes  \
> and sparks so sharp, \
> they can be felt by grazing a little finger. \
> we can speak of things that  \
> when we don’t feel strong, we think about \
> and of things that  \
> when we want to feel strong, we think about. \

> little animals, small and quiet and shivering  \
> caressed but  \
> suspended \
> by the tips of whiskers only air. \
> when we speak, noses twitching \
> it can be of \
> separateness, discreteness \
> the spaces between circles \
> overlapped, but still holding. \
> we can multiply interpretations,  \
> and brace against their weight \
> or  \
> we can scoop up abundance \
> and bathe lightly. \

-------

> throats, quivering, sound like cold or terror \
> but they too ask, whether the circles  \
> are organisms micro-vibrating \
> with the energy it takes  \
> to realise squelchy new forms \
> to absorb: experience grows the self grows autonomy \
> by care and love into the blood. \

> our expanding throats  \
> stickily coated with the outside,  \
> wonder \
> whether us-as-earth, will shift magnetic poles again  \
> as we did recently  \
> 780,000 years ago \
> well, not within my living memory but \
> maybe within ours. \

> it’s comforting that a polar shift might mean no more \
> to glottises \
> than huffing in the wet brown mulch from the forest floor; \
> yelling in bluebells until we laugh \
> is anyway more accessible and soothing. \

------

> to you, the piece of my bruised heart: \
> that bright lodged glint  \
> is your intelligence \
> equal to all winds it bends against \
> think of it like a tinder mushroom - \
> cupped, it can keep you warm \
> and light many fires along the way \

> when we loudly open ourselves without \
> insides \
> the world rushes in  \
> unbidden even \
> but making calm salve  \
> by making ourselves space \
> avoids projection.  \
> when we speak, let’s not speak  \
> about identity \
> no -isms or -nesses, \
> just about sheltering  \
> the trembling creatures, we are \
> just about protecting  \
> the vulnerability in our voices. \

> that belief, in self-knowledge \
> might even allow  \
> ample vibrant exchange, iron fizzing, \
> something like a conversation between \
> million year old magnetic traces found inside a tenth of a millimetre of rock \
> and a magnetometer \
> in the key of hysteresis \

> this vocabulary we share,  \
> made of hard and soft sounds \
> of silences  \
> and recurrences \
> (i won’t lie)  \
> has lashed at times, \
> and whipped us round \
> but it has also gifted us  \
> cycles of growth  \
> and non-linear narratives \
> comfortably spiky creations \
> and exuberant wide worlds; \
> so now \
> when we speak, intertwined,  \
> i’m grateful for all those words \
> that bind and support us \
> leaning back away from another,  \
> onto each other,  \
> overlapped but holding, still.