% Dear visitor,
% Behuki

# Dear visitor,

## /boot /home /lost & found 

\
The story goes, Connie bought 4 nispero fruits in Austria where it is
called Wollmispelfrucht because it reminded her of one of the places
she used to live in the past. Past midnight Mia tried some for the first
time in her life, Ulla had some too and the baby in her belly, later known
as Mika, tried some for the first time pre-life as well. Well-formed, round
and smooth; Ulla found the seeds of the fruits surprisingly beautiful
and kept them in the communal kitchen, a small paper cup with them
all inside. Inside esc one evening, before going to drink schnapps with
Norbert, Connie had to video-call another collective she is in. In an
effort to distract her attention back to us, Antonia took two of the
seeds and spat them across the room, where Mia was standing, who
spat them back. Back at work the next day Connie gave these two seeds
to Mia who took them to Brussels where nispero is mostly called néfle
or loquat by the inhabitants. Inhabiting Brussels, Azahara, who knew
Connie and Antonia from a different collective, got one seed, the other
one took many weeks to germinate and springup. Up until now it has
grown, but it has not yet born any fruit other than this story.

-------------

\
Another story starts with some fruits made of almond-paste. Almondpaste 
from Sicily was rolled between hands in Barcelona and Connie
chose to make a Nispero.

Nispero is a spring fruit, and this was autumn, so when Iris and Rosa
searched for it in the shops, they ended up bringing plums home. Home
was the place were Iris was living together with Azahara and Giulia,
so plums were eaten in the morning for breakfast accompanied with
Antonia’s recently made coffee. Coffee was served while awaiting Julia,
coming with her son Victor, and later on Mafe proposed an exercise
that consisted of writing another story. 

![](images/iteration-5-1-5.jpg)

![](images/iteration-5-1-1.jpg)

![](images/iteration-5-1-2.jpg)

![](images/iteration-5-1-3.png)

![](images/iteration-5-1-4.jpg)

![](images/iteration-5-2-1.jpg)

![](images/iteration-5-2-2.jpg)

![](images/iteration-5-2-3.jpg)

![](images/iteration-5-2-4.jpg)

![](images/iteration-5-2-5.jpg)

![](images/iteration-5-2-6.jpg)

![](images/iteration-5-2-7.jpg)

![](images/iteration-5-2-8.jpg)

![](images/transparent.png)

## /var 

![](images/iteration-5-3-1.jpg)

![](images/iteration-5-3-2.jpg)

![](images/transparent.png)


\
Memory is fragile; sometimes a simple smell is enough to bring you back to childhood and sometimes a modest receipt can evoke an entire day full of emotions. Emotions as evidence of a collective working process is hard to document or to picture. Pictures is mostly taken when we’re living what we expect to be a good future memory, or we send postcards to friends to share what we are doing. Doing so, we usually avoid archiving a sad memory or moment of conflict, even though this is important too, maybe even more so. So, how to remember when such a conflict starts, or when it transforms into something more destructive like a break of trust? Trust me, at that point, you’d usually try to escape the situation (as a group, if not individually) as it might lead to the end of the group itself, and then...

Then, who cares, at this moment, to take a picture, a memory?

----------

![](images/transparent.png)
