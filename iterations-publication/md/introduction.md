<!-- __NOPUBLISH__ -->
<!-- CAT -->

<!-- # header1 -->
<!-- ## header2 -->
<!-- *italics* -->
<!-- <https://iterations.space> -->
<!-- > the quote here -->


# Itera-<br />cions

Internet ha esdevingut un espai extraordinàriament complex que pot
interpretar-se com una metàfora del món, per tal com ningú no és capaç
d'aprehendre'l completament. Per a les persones involucrades en l'art i
interessades en la tecnologia, és una raó de pes per agrupar-se i, de
manera col·lectiva, analitzar, utilitzar, desenvolupar o desmantellar la
infraestructura principal de la nostra època; per desmuntar o hackejar
conjuntament les nostres xarxes, per inventar i executar múltiples
internets, i crear futurs en els quals ens agradaria viure.

Les organitzacions sòcies d'Iteracions (Hangar, esc, Constant i
Dyne.org) actuen en el mateix àmbit; tanmateix, cadascuna difereix pel
que fa al seu punt de partida, missió i mode d'operació. Per aquest
projecte, doncs, hem procurat establir uns procediments comuns, que cada
organització ha aplicat a la seva manera.

Iteracions és una col·laboració entre diverses organitzacions dedicades
a l'art i la cultura a Europa i ha estat cofinançada pel programa Europa
Creativa de la Comissió Europea. Tot i que el projecte va sorgir de la
convicció comuna que la col·lectivitat a l'art requereix investigació i
atenció, també s'ha configurat d'acord amb els criteris del marc Europa
Creativa.

Com a mostra, vam introduir-nos en un programa que dona suport a
"projectes transnacionals de cooperació a diversos països", és a dir,
que contribueix al paradigma de la mobilitat creixent. Tant quan decidim
veure'ns i treballar a distància mitjançant eines electròniques com quan
viatgem en tren o en avió, les nostres accions se sustenten en el consum
d'energia i un augment de les emissions de carboni.

Un altre exemple de la influència d'aquest marc en el nostre treball és
el fet que les nostres organitzacions no donen per descomptada la
delimitació geogràfica de les fronteres europees —en moltes altres
situacions, col·laborem amb socis de fora de la Unió Europea. No forma
part, doncs, dels nostres criteris habituals de treball.

Nosaltres, les persones de principi del segle xxi, habitem les ruïnes
postcapitalistes que s'han tornat la nostra llar col·lectiva. Ens oferim
voluntàriament per fer tasques de neteja, manteniment o restauració, raó
per la qual hem d'aprendre, hem de negociar i parlar, lluitar, bregar i,
de vegades, fracassar conjuntament.

Si un dels vectors que ens uneix a llarg termini és accelerar la fi del
capitalisme, què poden aportar-hi projectes com ara Iteracions? En lloc
de donar una resposta explícita en aquesta acció, vam decidir
esforçar-nos per crear espais i oportunitats per al desenvolupament de
pràctiques artístiques crítiques. Per estimular la consciència, la
sensibilitat i la intuïció per a les polítiques relatives a les eines i
les tecnologies tot fomentant la companyonia i l'intercanvi.

En aquest sentit, Iteracions es va concebre com una sèrie de residències
artístiques en diverses localitats d'Europa. Cada edició es va pensar
com una reiteració de l'anterior que, al seu torn, es reiteraria en la
següent. A cada trobada de relleu, les obres, els processos, els
materials i els conceptes es traspassaven del grup d'artistes residents
al grup següent, com un nou punt de partida des del qual torna-ho a
intentar.

El seminari inicial es va celebrar el desembre de 2017 a Hangar, on es
van proposar i debatre alguns principis comuns de partida. A la
primavera de 2018, un grup de més de 30 artistes, hackers, dramaturgs i
músics van marxar a Giampilieri, un petit poble entre mar i muntanya al
terme municipal de Messina, Sicília, per participar en la residència
"Transformatorio", organitzada per Dyne.org. Les obres produïdes durant
l'estada es van presentar per tot el poble en mig d'un ambient festiu.
El material triat perquè el remesclessin els artistes de la pròxima
residència va ser una cançó: la *performance*, en dialecte sicilià, va
introduir els reptes propis del multilingüisme.

A la residència següent, el novembre de 2018 a Barcelona, els artistes
van concentrar-se a formar una personalitat col·lectiva: una persona
artística grupal que van anomenar Rica Rickson, una identitat/entitat
que podia ocupar qualsevol membre del grup o diversos a la vegada.

Pel maig de 2019, després de diverses presentacions, laboratoris i
reunions, un grup d'artistes sota el nom *common ground* van col·laborar
amb l'objectiu de construir una instal·lació ad hoc per a l'exposició
"Collaboration Contamination", que es va celebrar a l'espai d'esc mkl a
Graz. Al llarg de l'estiu de 2019, Constant va organitzar dues
residències a Brussel·les, de les quals va néixer l'exposició "Operating
/ Exploitation" d'octubre al Bozar-Labo, seguida immediatament per la
sessió de treball "Collective Conditions".

> Iteracions investiga el futur de la col·laboració artística en contextos digitals

Aquest ha estat el lema del projecte durant els quatre anys que ha
durat. El concepte bàsic ha estat "iterar" l'acció en diverses
circumstàncies semblants però diferents i enfortir les estratègies
col·laboratives i col·lectives en la pràctica o les pràctiques
artístiques.

Aturem-nos un moment per analitzar amb més cura cadascuna de les
paraules d'aquest lema: "investiga el futur de la col·laboració
artística en contextos digitals".

## Investigar

Altrament dit, el procés d'investigació. Els mitjans d'indagació que es
van desenvolupar es guiaren per la pràctica de fer les coses juntes, fer
la pràctica artística juntes, fer l'art juntes, fer juntes la comprensió de
la interpretació, de l'activitat, de l'actuació, de l'autonomia com a
éssers conscients de si mateixos.

Malgrat que Iteracions va seguir una metodologia fixa, es van posar en
pràctica diverses formes d’investigació: investigació guiada per la
"intuïció", per afinitats, per impulsos i per oposició a la re-cerca
(tornar a cercar el que ja sabem que existeix a fi de demostrar una
tesi). És això el que anomenaríem *experimentar*? Jugar sense una
finalitat particular o explotar els cossos vius en nom de la ciència?
Els participants com a rates de laboratori en un cub blanc convertit en
proveta? Això dista molt del que cercàvem.

Això no obstant, la situació particular d'haver de reunir —en un entorn
artificial en el temps i l'espai— diversos artistes que no havien
col·laborat abans va determinar molts factors de la interacció. En
diverses ocasions en els darrers tres anys, hem fet un pas enrere, hem
restablert les condicions per tornar al punt de partida i ho hem tornat
a provar. Hem reiterat.

El conjunt de condicions inicials i de procediments del projecte
comportava el debat en acció (o l'acció a debat) sobre el procés
artístic col·lectiu, cosa que va afectar el desenvolupament del
projecte. L'evolució concreta, sumada a les veus i els cossos implicats,
van canviar-ne la implementació.

Això també va causar certes friccions. Però és que pot haver-hi
col·laboració sense friccions de cap mena? Són els conflictes un efecte
(secundari) inevitable en un entorn artificial com aquest? És possible
produir en col·lectivitat sense aquests reptes? Sembla complicat
evadir-se de les dinàmiques de grup recurrents, sobretot en un termini
d'experimentació tan curt (normalment, dues setmanes). Amb tot, hi havia
un factor clar des del principi: a Iteracions, l'objectiu de la
investigació no era necessàriament ni principalment l'obra final, sinó
les condicions procedimentals, és a dir, el treball, els intercanvis,
l'*experiència compartida*.

## El futur de

L'inici d'una iteració basada en regles genera nous futurs. En plural.

Aquests futurs tenen repercussions aquí i ara: presències quàntiques i
*difraccions*.

S'aproxima això a allò que el projecte preveia que podrien fer les
artistes? Canviar les perspectives i aportar visions diferents de
conceptes establerts?

És possible que no sapiguem anar juntes en la mateixa direcció, però com
a mínim hem d'aprendre a tractar amb els materials decisius. El futur ha
de ser col·lectiu; o és possible que no n'hi hagi. El futur no com allò
utòpic, equilibrat i prometedor que podem assolir, sinó com un avenir
múltiple proper a l'ara i aquí. No hi ha lapse d'espera abans d'aquest
temps futur, ni marge per a l'esperança o la desesperança. Més aviat, és
una mena d'esdevenidor tangible que deixem de controlar tan bon punt
surt de les nostres mans.

## La col·laboració artística

El projecte Iteracions, inspirat en les formes recurrents de
col·laboració pròpies del desenvolupament de programari lliure i de codi
obert, Free/Libre Open Source Software en anglès (F/LOSS), va aplicar la
repetició i la circularitat —processos en què el resultat d'una
activitat pot servir d'inspiració i font per a la següent, però també
pot descartar-se sense més ni més— als mètodes artístics.

Ens vam guiar, i ens guiem, per les pràctiques de F/LOSS que contemplen
estratègies per contribuir als projectes que cadascú considera
importants, estiguin en la fase en què estiguin. Actualment, patim una
greu infraexposició a la pràctica de la producció i la representació de
l'art contemporani en col·lectivitat, però, alhora, veiem com gran part
de l'elaboració artística de conceptes, eines, mètodes i plantejaments
es duu a terme de manera col·lectiva o en col·laboració.

En conseqüència, la sèrie va experimentar amb la creació col·lectiva,
l'autoria conjunta i les condicions de grup. Cada iteració va esdevenir
una invitació oberta per a la reapropiació. Els materials, el codi, les
imatges, les instruccions i la documentació es van posar en comú i
continuen estant disponibles a la pàgina web del projecte sota
llicències de contingut obert.

En aquest sentit, les "trobadas de relleu" van ser vitals per al procés
artístic col·laboratiu, en tant que moment en el què les obres d'art,
els processos mentals, els materials en brut i els conceptes es
lliuraven al pròxim grup que continuaria la tasca. Les trobadas de
relleu, doncs, van suposar la posada en comú d'una situació i un procés
en curs: un gest per crear vincles i compartir alguns elements de
l'experiència d'iteració viscuda. Així, el relleu suposava en cada cas
una metamorfosi, un acte d'esdevenir. L'intercanvi entre artistes no es
referia necessàriament a coses materials, sinó que també podia implicar
idees, experiències i, de vegades, dades.

En certs moments clau, el grup que conformava Iteracions va decidir
tensar la definició de la paraula *col·laboració* i arribar als fons del
concepte de *col·lectiu*. El canvi de paradigma pot descriure's així: en
un escenari col·laboratiu, diversos individus treballen, produeixen i
actuen conjuntament, de manera que es generen possibles traces
d'autoria, la noció de la qual es fonamenta fins a cert punt en el
context col·lectiu, on la diferenciació entre les diverses aportacions
queda desdibuixada. Això va ocasionar que, en moments determinats, el
desenvolupament del projecte qüestionés la decisió inicial de fer servir
la paraula *col·laboració* en lloc d'acció *col·lectiva*. La necessitat
d'establir uns criteris unificats és un requisit difícil de complir.
Potser hauria estat més eficaç i menys penós facilitar unes condicions o
bases comunes. En tot cas, queda pendent la qüestió —si no en les
dificultats que van sorgir en cada nova implementació, sí en el cor
mateix del projecte Iteracions— de com (aconseguir) actuar plegats.

Durant les diverses fases, vam observar una proliferació de l'ús dels
prefixos *con*–, *com*–, *col*–, *cor*– o *co*– (‘juntament amb’, ‘en comú’). En
el transcurs del projecte, el conflicte esdevingué un element
constructiu. Afrontar conjuntament el problema com a manera de no
deixar-lo passar, de fer lloc a les relacions difícils, necessàries en
les pràctiques que pretenen abraçar la diferència. En efecte, la
*pertinença* pot ser una condició prèvia exigent per assolir una meta;
tanmateix, dona cabuda a les aliances. Al cap i a la fi, mostrar
solidaritat o sentir-se part de quelcom també pot ser una afirmació
col·lectiva, una acció de cossos aliats; corporalitats que conserven la
seva individualitat.

Iteracions va crear conjuntures en les quals les diferències que es
trobaven i debatien es transferien a les peces. En molts casos, les
artistes, organitzadores i altres interessades no havien treballat
juntes en el passat, per la qual cosa es conegueren durant el projecte.
Encara que no sempre és fàcil, és important no ignorar les tensions i
friccions que poden sorgir en aquestes situacions, sinó prendre-se-les
seriosament per generar diferències productives que puguin tenir un lloc
central en la pràctica comuna.

<framebreak>

## En contextos

Què entenem per *context*? Quines (a)simetries conformen la posició del
subjecte? La nostra tasca en l'art i la cultura està vinculada a molts
sectors de la societat, a programes d'innovació tecnològica i a
polítiques ecològiques i econòmiques; està lligada a uns futurs que
potser ni tan sols siguem capaces d'imaginar. Així, concebem el context
com concebem el futur. És aquí mateix, molt a la vora, per bé que també
és una gran obertura difractiva i plural a la immediatesa. Treballem en
entorns diferents: la ciutat, el medi rural, el recés, el centre,
l'esfera privada, l'espai públic.

L'ecologia, l'era de la postveritat, els museus i les exposicions o les
*performances*… Com influeix cada context en les iteracions, les seves
dinàmiques i els seus resultats? L'ecologia com un gran tema i el món
posthumà com a qüestió i mètode. I, amb tot plegat, la necessitat de
crear lligams, de comprendre, completar i contaminar. Els col·lectius es
formen també a força d'intrusions i alteracions, transposicions o
accidents. El context és alhora concret i dislocat; qui és aquí?, qui
tenim al nostre costat?, amb qui estem fent això?, en quin moment està
passant?, a on? La dislocació, la unió intuïtiva de llocs, objectes i
persones segregades, com a mètode per establir el context.

## Digitals

El significat de la paraula *digital* ha canviat al llarg de les últimes
dècades. L'Internet primitiu va suscitar l'aparició de l'art en xarxa i
experiments tecnològics crucials sobre el gènere i la identitat, a més
de fer possible un espai per a infraestructures independents. Avui,
aquella potencialitat esperançadora ha quedat eclipsada per una
mercantilització i un comerç invasius basats en el colonialisme
tecnològic, el solucionisme tècnic, la correlació de dades massives i el
dispendi d'energia. El terme *digital*, així doncs, no pot deixar-se sense
comentaris, sinó que requereix un posicionament crític.

Durant Iteracions, es va fer palès que de tant en tant les participants
preferien apagar els ordinadors i passar una estona juntes, compartint
el menjar, la temperatura, els sorolls i silencis, les energies i les
fatigues. Quan això succeïa, el digital no era sinó una metàfora molt
potent.

## Aquesta publicació

El propòsit d'aquest llibre és fomentar l'obertura de mires, així com
nous conceptes de treball que encara s'han de desenvolupar. Com a
receptable que és, és ple de contingut híbrid que pot extreure's i
reutilitzar-se. Les aportacions d'algunes de les persones participants
en edicions passades i futures d'Iteracions hi concorren en un esforç
conjunt per fer arribar idees a individus i contextos que encara ens són
desconeguts. Així, aquesta publicació podria considerar-se com una
iteració en ella mateixa.

Les aportacions reflecteixen diversos enfocaments, actituds i postures
que van aflorar en motiu del projecte Iteracions. Les editores han
preparat un conjunt de descripcions dels denominadors comuns, les quals
van anomenar-se nanses (*handles*), i han convidat cada participant a
respondre un d'ells. Durant el *book sprint* per processar el material, es
van elaborar unes *partitures* (*scores*) que faciliten la lectura
transversal de les aportacions. Aquesta pràctica s'ha denominat
*anindexació* (les editores Jara Rocha i Manetta Berends donen més detalls
sobre aquesta metodologia en el quadern inclòs en aquesta publicació).

Els idiomes connecten i divideixen, inclouen i exclouen de manera
simultània. Les llengües a les quals s'han traduït els textos d'aquesta
obra, o de les quals s'han traduït, són les pròpies de les
organitzacions sòcies. Tot i que l'anglès no n'és una, està present
aquí, ja que ha estat la principal llengua de comunicació del projecte.
Tot parlant-lo i escrivint-lo, l'hem transformat en un mecanisme
polifònic i mestís. La convivència de més de 30 nacionalitats l'ha
tenyit i impregnat d'espores de les nostres respectives llengües
maternes, que han deixat rastres perceptibles al llarg d'aquesta
publicació.

Algunes de les contribucions es basen en documents sorgits de les
activitats d'Iteracions. *common ground* van esbossar una partitura musical a partir de
les zones destacades al plànol de l'exposició "Collaboration
Contamination", que assenyala els elements acústics de l'exhibició i pot
utilitzar-se com a fonament per a futures peces (sonores).

La residència i exposició "Operating / Exploitation" ha estat la font de
la contribució "Dear visitor," de *Behuki*.

"Three Documents", la introducció redactada per *Collective Conditions*,
presenta una sèrie de pràctiques d'escriptura en grup que van sorgir
juntament amb els tallers Collective Conditions de Constant.

*Rica Rickson* reflectia en multicolor el que acabava de néixer durant la
seva residència a Hangar. En un diàleg interior, Rica es planteja el seu
cos col·lectiu, així com els seus múltiples entrellaçaments amb el món
de l'art, els desitjos, les necessitats i les genealogies.

A més d’aquestes aportacions, es van cursar invitacions per a dos
articles complementaris: a "holding spell", *Kym Ward* apunta que les
trobades entre persones poden ser un moment de convergència i màgia. En
canvi, a "Por debajo y por los lados. Sostener infraestructuras
feministas con ficción especulativa", *spideralex* conjuga tècniques i
formulacions sobre les infraestructures comunitàries per reflexionar al
voltant del futur i la construcció del món.

Aquesta publicació no ha estat concebuda com un catàleg. El treball comú
té el dret a ser difós, a tenir vida pròpia, exposar-se a influències
genials i beneficiar-se de les aportacions d'altres, i transformar-se.
Per això, si teniu interès a aprofundir en les imatges, textos, art,
gravacions sonores, codis font i altres materials originats en el marc
del projecte Iteracions, us encoratgem a explorar la pàgina web del
projecte (<https://iterations.space>) i utilitzar, copiar, modificar i
redistribuir-ne els continguts. En aquestes pàgines web també trobareu
més traduccions dels articles d'aquesta publicació.

El nostre agraïment més gran a tots i totes les artistes, participants,
patrocinadors, organitzacions i persones que han contribuït al projecte
Iteracions i aquesta publicació.

<!-- __NOPUBLISH__ -->
<!-- DE -->

<!-- # header1 -->
<!-- ## header2 -->
<!-- *italics* -->
<!-- <https://iterations.space> -->
<!-- > the quote here -->

# Itera-<br />tionen

Das Internet ist zu einem erstaunlich komplexen Ort geworden und kann als Metapher für die ganze Welt interpretiert werden - kein Individuum ist in der Lage, seine Gesamtheit zu erfassen. An Technologie interessierten Kunstschaffenden war dies Anlass, zusammenzukommen, um die wichtigste Infrastruktur unserer Zeit gemeinsam zu verstehen, zu nutzen, zu entwickeln und zu hinterfragen. Wir wollten Netzwerke „auseinandernehmen“ und selbst unsere eigenen Netzwerke hacken, mehrere Internets erfinden und betreiben und eine Zukunft aufbauen, in der wir gerne leben würden.

Die Partnerorganisationen von *Iterationen* (Hangar, esc medien kunst labor, Constant und Dyne.org) sind zwar alle im gleichen Bereich tätig, hatten jedoch unterschiedliche Ausgangspunkte, Zielsetzungen und Arbeitsweisen. Unsere Absicht bestand darin, für dieses Projekt gemeinsame Methoden festzulegen, um dann von jeder Organisation unterschiedlich umgesetzt zu werden.

*Iterationen* ist ein Gemeinschaftsprojekt mehrerer Kunst- und Kulturorganisationen mit Sitz in Europa, das unter anderem im Rahmen des Programms Creative Europe der Europäischen Kommission finanziell gefördert und im Zeitraum 2017-2019 realisiert wurde. Das Projekt entstand aus der Überzeugung heraus, dass Kollektivität in der Kunst untersucht und gefördert werden muss. Bei der Gestaltung des Projekts wurden jedoch auch Kriterien des Creative-Europe-Rahmens berücksichtigt, wie beispielsweise das Programm zur Unterstützung von „transnationalen Kooperationsprojekten aus verschiedenen Ländern“, mit dem wir das Paradigma einer verstärkten Mobilität unterstützten. Ob wir mittels Online-Tools aus der Ferne zusammenarbeiten oder ob wir zu einem Treffen anreisen, unser Handeln basiert dabei immer auf Ressourcennutzung und Energieverbrauch und der Vergrößerung unseres ökologischen Fußabdrucks. Ein weiteres Beispiel dafür, wie unsere Arbeit durch einen solchen Rahmen beeinflusst wurde, besteht darin, dass die geografische Beschränkung auf europäische Grenzen für die Mitgliedsorganisationen nicht selbstverständlich ist, denn oftmals arbeiten Hangar, esc mkl, Constant und Dyne.org mit Partner\*innen aus Nicht-EU-Ländern zusammen. Als Angehörige der Generation des beginnenden 21. Jahrhunderts leben wir zwischen den postkapitalistischen Ruinen, die zu unserer kollektiven Heimat geworden sind. Wir übernehmen freiwillig Arbeiten zur Haushaltsführung, Instandhaltung und Reparatur. Dazu müssen wir lernen zusammenzuarbeiten, uns zu besprechen, zu verhandeln, zu kämpfen, zu handeln und manchmal auch zu scheitern.

Wenn wir als langfristigen Vektor gemeinsam haben, das Ende des Kapitalismus beschleunigen zu wollen, wie können Projekte wie *Iterationen* dazu beitragen? Anstatt dies als expliziten Teil der Mission zu erklären, beschlossen wir, Raum und Gelegenheit für die Entwicklung kritischer Kunstpraktiken zu schaffen, um Bewusstsein, Sensibilität und das Gespür für die Politik von Werkzeugen und Technologien durch das Herbeiführen von Zusammengehörigkeit und Austausch zu stimulieren.

Daher wurde *Iterationen* als eine Reihe von Residencies, Künstler\*innenaufenthalten an verschiedenen Orten in Europa konzipiert. Jeder Residency sollte als Iteration des vorhergehenden Aufenthalts betrachtet und seinerseits im nächsten Aufenthalt wieder aufgenommen werden. Ein sogenanntes Handover, ein „Treffen zur Übergabe“ war der Moment, in dem Kunstwerke, Prozesse, Materialien und Konzepte von einer Arbeitsgruppe an die nächste übergeben wurden, – als ein Ausgangspunkt, von dem aus man es erneut versuchen konnte.

Beim Auftakttreffen im Dezember 2017 in Hangar, Barcelona, wurden die ersten gemeinsamen Ansätze zur Zusammenarbeit entwickelt. Im Frühjahr 2018 machte sich dann eine aus mehr als 30 Künstler\*innen, Hacker\*innen, Theatermacher\*innen und Musiker\*innen bestehende Gruppe auf den Weg nach Giampilieri in Sizilien, einem kleinen, <br />zwischen Meer und Bergen versteckt gelegenen Dorf im Großraum Messina. Dieser Aufenthalt unter dem Titel Trasformatorio wurde von Dyne.org organisiert. Die während des Workshops produzierten Werke wurden im ganzen Dorf feierlich präsentiert und ein dabei entstandenes Lied wurde zum Übergabematerial für die nächste Gruppe. Die Präsentation dieses Liedes, die in sizilianischem Dialekt stattfand, machte die Herausforderungen der Mehrsprachigkeit deutlich. Während des anschließenden Aufenthaltes in Barcelona im November 2018 konzentrierten sich die Künstler*Innen darauf, eine kollektive Persona zu schaffen: eine künstlerische Identität mit dem Namen Rica Rickson, eine Entität/<br />Einheit, die von jedem Mitglied der Gruppe oder vielen von ihnen gleichzeitig „bewohnt“ werden konnte und kann.

Nach einer Reihe weiterer Präsentationen, Worklabs und Treffen arbeitete im Mai 2019 eine Gruppe von Künstler\*Innen unter dem Namen *common ground* zusammen, um die Ausstellung Collaboration Contamination im Ausstellungsraum des esc medien kunst labor als ortsspezifische Installation zu schaffen. Im Sommer 2019 organisierte <br />Constant zwei Arbeitsaufenthalte in Brüssel, aus denen sich im Oktober die Ausstellung „Operating / Exploitation“ im Bozar-Lab entwickelte, an die unmittelbar danach das Worklab „Collective Conditions“ anschloss.

> „Iterationen erforscht die Zukunft der künstlerischen Zusammenarbeit in einem digitalen Kontext“

Dieser Kernsatz stand im Zentrum des Gesamtprojektes. Das Grundkonzept bestand darin, eine Aktion unter mehreren sich ähnlichen, aber dennoch unterschiedlichen Umständen zu „iterieren“, also zu wiederholen und daraus entstehende kollaborative und kollektive Strategien in der jeweiligen künstlerischen Praxis zu stärken.

Was steckt hinter den Begriffen dieses Kernsatzes?

## „Erforschen“

Also: der Vorgang des Erforschens. Die entwickelten Methoden basierten auf der Praxis des gemeinsamen Handelns: gemeinsam Kunst zu schaffen, im Verständnis des Handelns als einem Akt der Freiheit, des Aktivseins, des Handelns als selbstbewusste Entität(en). Obwohl *Iterationen* einer festgelegten Methodik folgte, verliefen die Forschungen auf vielerlei Art und waren geleitet von Intuition, Affinitäten und Impulsen, – im Gegensatz zu „Re-search“, quasi einer Nach-Suche nach etwas (von dem man eigentlich weiß, dass es da ist) zu dem Zweck, eine These zu beweisen. Könnte man *Iterationen* also als ein „Experiment“ bezeichnen? Spielen ohne ein bestimmtes Ziel oder auch Verwendung lebendiger Körper für die Wissenschaft: die TeilnehmerInnen als „Laborratten“ in einem White-Cube-Reagenzglas? Das ist weit von dem entfernt, was wir anstrebten. Die besondere Situation, Künstler\*innen, die noch nie zuvor zusammengearbeitet hatten, in einem künstlichen Rahmen in Zeit und Raum zusammenzubringen, bestimmte dennoch viele Faktoren der Interaktion(en). In den 3 Jahren sind die Projektant\*innen mehrmals einen Schritt zurückgetreten, sind zurück zum Start und haben von vorne angefangen.

Die Ausgangsüberlegungen und die Methodik des Projekts beinhalteten die aktive Debatte und Diskussion (oder auch Action-in-Discussion) des kollektiven künstlerischen Prozesses. Dies strahlte auf die Struktur des Projekts selbst aus; konkrete Entwicklungen des Projekts wie auch die Stimmen der beteiligten Personen und Gruppen beeinflussten dessen Umsetzung.

Das bedeutete auch, dass es Reibungen gab. Kann irgendeine Zusammenarbeit überhaupt reibungslos verlaufen? Sind Konflikte nicht die unvermeidliche (Neben-)Wirkung eines künstlichen Settings? Gibt es kollektive Produktion ohne solche Herausforderungen? Es scheint schwierig zu sein, sich typischen Gruppendynamiken zu entziehen, vielleicht insbesondere bei Experimenten von so kurzer Dauer (durchschnittlich 2 Wochen). Ein Aspekt war jedoch von Anfang an klar: Ziel der Auseinandersetzung bei *Iterationen* ist nicht notwendigerweise oder in erster Linie das entstehende Kunstwerk, sondern die prozessualen Bedingungen: die Arbeit, der Austausch, die Erfahrung des Miteinanders.

## „Zukunft beginnt“

Die Aufnahme einer regelbasierten Iteration schafft neue Zukunftsvarianten. Und zwar mehrere. Sie haben Auswirkungen auf das Hier und Jetzt: Quantenpräsenz und Diffraktion. Kommt dies dem nahe, was den Künstler\*Innen für das Projekt vorschwebte? Die Perspektive zu wechseln und bewährte Konzepte um neue Orientierungen zu erweitern? 

Wir können vielleicht noch nicht gemeinsam in eine Richtung gehen, aber wir müssen wenigstens versuchen mit Materialien zu verhandeln, die von Bedeutung sind. „Die Zukunft ist entweder kollektiv, oder es wird keine geben.“ „Zukunft“ nicht als ein vielversprechendes und ausgewogenes utopisches Bild, für das wir etwas tun können, sondern als ein Bündel von Varianten, das aus dem Hier und Jetzt entsteht. Es gibt keine Pause vor „dieser“ Zukunft, keinen Raum für Hoffnung oder Verzweiflung, sondern vielmehr Formen der berührbaren Zukunft, die sich unseren Händen und unserer Kontrolle entziehen. 

## „Künstlerische Zusammenarbeit“

Inspiriert von rekursiven Formen der Zusammenarbeit, wie sie in der Open-Source-Softwareentwicklung (F/LOSS) existieren, wandte das Projekt *Iterationen* Wiederholung und Zirkularität auf künstlerische Methoden an: Prozesse, bei denen die Ergebnisse einer Aktivität als Inspiration und Material für die nächste verwendet oder auch beiseite gelassen werden können.

Als Referenz dien(t)en F/LOSS-Praktiken, die Strategien zur Unterstützung von Projekten beinhalten, die man für wichtig hält, unabhängig von deren Ausgangsüberlegungen. Wir sind Zeug\*innen einer riesigen Unterrepräsentanz von Praktiken kollektiver Produktion und Repräsentation in der zeitgenössischen Kunst. Gleichzeitig sehen wir aber, dass ein beachtlicher Teil künstlerischer Entwicklung von Konzepten, Werkzeugen, Methoden und Ansätzen gemeinsam oder in Zusammenarbeit mit anderen, also kooperativ und/oder kollektiv erfolgt.

Deswegen experimentierte das Projekt mit kollektivem Schaffen, gemeinsamer Urheber\*innenschaft und Gruppenarbeitsbedingungen. Jede Iteration war eine offene Einladung zur (Wieder-)Aneignung. Materialien, Software-Code, Bilder, Anleitungen und Dokumentation wurden und werden unter Open-Content-Lizenzen auf der Webseite des Projekts zur Verfügung gestellt.

In diesem Zusammenhang waren die „Übergabe-Treffen“ für den gemeinschaftlichen Kunstprozess von wesentlicher Bedeutung, denn sie boten die Gelegenheit, Kunstwerke, Denkprozesse, Rohmaterialien und Konzepte an die nächste Gruppe weiterzugeben, damit diese sie fortsetzen könnte. Die Übergabemomente bestanden aus dem Teilen eines laufenden Prozesses und einer Situation: eine Geste zur Überbrückung, um ein Element der gelebten Erfahrung der Iteration weiterzuschenken. Eine solche Übergabe war immer sowohl eine Metamorphose als auch ein Werden. Der Austausch unter den Künstler\*innen erfolgte daher nicht nur über Materialien, sondern auch über Ideen, Erfahrungen und manchmal über Daten.

Interessanterweise ergab sich mehrmals folgende Situation: die beteiligten Künstler\*innen stellten den Begriff „collaboration“, also Zusammenarbeit, zur Diskussion und ersetzten ihn in der Auseinandersetzung durch den Begriff des Kollektiven. Diese Verschiebung kann wie folgt beschrieben werden: in einem Umfeld der Zusammenarbeit, also der „collaboration“, arbeiten und handeln einzelne Menschen als Individuen in einer Gruppe und hinterlassen zuordenbare Spuren ihrer Arbeit, dh. es kann eine eindeutige Autor\*innenschaft geben. Im Gegensatz dazu sind in einer kollektiven Umgebung diese Spuren einer Autor\*innenschaft oder die Trennung zwischen den verschiedenen Beiträgen nicht mehr möglich. Die Notwendigkeit, einheitliche Kriterien zu schaffen, war (und ist generell) eine Herausforderung. Vielleicht wäre es effektiver und weniger schmerzhaft (gewesen), gemeinsame Handlungsansätze oder Medien zu generieren. Vielleicht aber ist eben genau dies die Frage, die auch im Zentrum des Projekts *Iterationen* stand: Wie (können wir) gemeinsam handeln?

Im Laufe der Zeit tauchten immer mehr Vorsilben auf: „co, col und con“ (mit, zusammen, gemeinsam). Ebenso kristallisierte sich Konflikt als konstruktives Element heraus. <br />Es zeigte sich, dass das Dranbleiben, das Weitermachen trotz Schwierigkeiten ein möglicher Weg war, nicht loszulassen und so Raum für schwierige Beziehungen zu schaffen, die für Praktiken notwendig sind, die sich auf Unterschiede einlassen wollen. Ja, das Konzept der Zugehörigkeit ist eine Hürde und schwierige Voraussetzung für die Erreichung eines Ziels, aber sie gibt Raum für Allianzen. Am Ende kann das Zusammenstehen, das Teilhaben auch ein kollektives Statement sein, eine Aktion von Einzelnen im Bündnis – Körperlichkeiten, die ihre Individualität behalten können.

In den konkreten Situationen, die *Iterationen* geschaffen hat, flossen die Unterschiede zwischen den einzelnen Personen in die Arbeit ein. In vielen Fällen trafen sich Künstler\*innen, Organisator\*innen und andere Agierende zum ersten Mal, ohne vorher zusammengearbeitet zu haben. Obwohl nicht immer einfach, ist es wichtig, dass aus diesen Situationen Spannungen und Reibungen entstehen und dass diese nicht ignoriert, sondern ernst genommen werden. Dadurch entstehen produktive Differenzen, die einen zentralen Platz in der gemeinsamen Arbeit verdienen.

<framebreak>

## „Digital“

In den vergangenen Jahrzehnten hat sich die Bedeutung des Begriffs des Digitalen verändert. Das frühe Internet führte zur Entstehung von Netzkunst, kritischen Cyborg-Identitäten, Gender-Tech-Experimenten und Raum für unabhängige Infrastrukturen. In der Gegenwart werden diese hoffnungsvollen Entwicklungen überschattet durch technik-koloniale, auf technische Lösungen fixierte und limitierte Ansätze im Bereich der Big-Data-Korrelationen, Energievergeudung, alles einnehmende Kommodifizierung und zerstörerischen Kommerz. „Digital“ darf daher nicht unkommentiert bleiben, sondern verlangt eine kritische Herangehensweise. Im Verlauf von *Iterationen* zeigte sich, dass Teilnehmer\*-<br />innen manchmal lieber ihre Laptops zuklappten und stattdessen gemeinsam Zeit verbrachten und Essen, Temperaturen, Geräusche und Stille, Energie und Müdigkeit teilten. Wenn dies geschah, war das Digitale vielleicht nichts mehr als eine starke Metapher.

## „Kontexte“

Was bedeutet Kontext? Welche Symmetrien / Asymmetrien sind an der Position des Subjekts beteiligt? Unsere Arbeit in und mit Kunst und Kultur ist mit vielen Bereichen der Gesellschaft verbunden, mit technischen Innovationsprogrammen genauso wie mit ökologischer / ökonomischer Politik, sie bezieht sich auf Zukunftsformen, die wir uns jetzt vielleicht nicht vorstellen können. Wir denken über den Kontext in ähnlicher Weise wie wir über die Zukunft denken. Es ist genau hier, sehr nahe, – aber es ist auch die diffraktive und pluralistische Offenheit der unmittelbaren Dinge. Wir arbeiten in so vielen verschiedenen Umgebungen: in der Stadt, auf dem Land, abgeschieden, im Zentrum, im Privaten, in der Öffentlichkeit.

Ökologie, die Zeit der Post-Wahrheit, Museen und Ausstellungen versus performative Aktionen: Wie beeinflussen die verschiedenen Kontexte die Iterationen, ihre Dynamik und ihre Ergebnisse? Ökologie als großes Thema, das Posthumane als Frage, als Methode. Und neben all dem eine Notwendigkeit, Verwandtschaften zu erkennen, zu verstehen, zu vervollständigen und auch zu kontaminieren. Kollektive bilden sich auch durch Eindringlinge, durch Veränderungen, Transpositionen, Unfälle und Zufälle. Kontext ist konkret und gleichzeitig disloziert. Wer ist hier? Wer ist in unserer Nähe? Wer wirkt auf uns ein? Wann geschieht das? Und wo? Die Dislokation, das intuitive Zusammenführen von getrennten Orten, Dingen und Menschen als Methode zur Herstellung eines Kontextes.

## Über diese <br />Publikation

Diese Publikation soll Zugänge öffnen und zu neuen Konzepten für noch zu schaffende Werke anregen. Sie ist quasi ein Gefäß mit hybridem Inhalt, der ausgepackt und wieder neu gemischt werden kann. Das Buch enthält sowohl Beiträge von Teilnehmer\*innen vergangener Residencies als auch von Iterator\*innen der Zukunft, sodass Ideen zu Personen und Kontexten transportiert werden, die wir noch nicht kennen. Als solche könnte diese Publikation als eine Iteration an sich betrachtet werden.

Die Beiträge beziehen sich auf Ansätze, Haltungen und Positionen, die aus dem *Iterationen*-Projekt hervorgegangen sind. Die Heraus-<br />geber\*innen erstellten eine Reihe von Beschreibungen von gemeinsamen Themen, die als Handles bezeichnet wurden und luden die einzelnen Teilnehmer\*innen ein, auf eine davon zu reagieren. Während des „redaktionellen Sprints“ zur Bearbeitung des Materials wurden <br />Scores erstellt, die eine transversale Lektüre der Beiträge gestatten. Diese Praxis wurde „X-Dexing“ genannt (die Herausgeberinnen Jara Rocha und Manetta Berends erläutern diese Methoden in der Broschüre innerhalb dieser Publikation).

Sprache verbindet und trennt gleichzeitig, sie schließt ein und schließt aus. Die Texte, die Sie hier finden, werden in die bzw. aus den Sprachen der Partner\*innen organisationen übersetzt. Englisch gehört nicht dazu, aber es ist hier präsent, weil es während des Projekts die Hauptkommunikationssprache war. Beim Sprechen und Schreiben verwandeln wir es in einen polyphonen Mischling. Da an diesem Projekt Teilnehmer\*innen aus mehr als 30 Nationen beteiligt waren, haben wir Spuren unserer jeweiligen Muttersprachen hinterlassen, die auch in dieser Publikation erkennbar werden.

Einige Beiträge bauen auf Dokumenten auf, die aus Aktivitäten von *Iterationen* resultieren. *common ground* skizziert basierend auf den hervorgehobenen Bereichen der Ausstellungskarte „Collaboration Contamination“ eine Partitur <br />(Score), welche akustische Elemente der Ausstellung markiert und als Score für zukünftige (Ton-)Werke verwendet werden kann.

Die Residency und die Ausstellung mit dem Namen „Operating / Exploitation“ bilden die Quelle für den Beitrag „Dear visitor,“ von <br />*Behuki*.

Die Einführung „Three Docu-<br />ments“ von *Collective Conditions* stellt mehrere gruppenspezifische Schreibpraktiken vor, die im Zusammenhang mit der Worksession „Collective Conditions“ bei Constant in Brüssel entstanden sind.

*Rica Rickson* reflektiert in bunten Farben über die Geburt während der Residency in Hangar. In einem inneren Dialog betrachtet Rica ihren kollektiven Körper sowie ihre vielfältigen Verstrickungen in Kunstwelt, Wünsche, Bedürfnisse und Genealogien.

Neben diesen Beiträgen wurden zwei Einladungen für Begleitartikel ausgesprochen: In „Holding Spell“ macht *Kym Ward* deutlich, dass Begegnungen zwischen Menschen Momente der Annäherung und Verzauberung sein können. In dem Beitrag „Por debajo y por los lados. Sostener infraestructuras feministas con ficción especculativa“ (dt. „Von unten von den Seiten. Feministische Infrastrukturen durch spekulative Literatur unterstützen“) trägt <br />*spideralex* Techniken und Formulierungen zu weltschaffenden und zukunftsorientierten Community-<br />Infrastrukturen zusammen.

Diese Publikation wurde nicht als Ausstellungskatalog konzipiert. Zur Verfügung gestellte Arbeit hat das Recht verbreitet zu werden, ein Eigenleben zu entwickeln, brillanten Einflüssen ausgesetzt zu werden, von den Ergänzungen anderer zu profitieren und sich zu verwandeln. 

Wir laden also alle dazu ein, Bil-<br />der, Texte, Kunstwerke, Tonaufnah-<br />men, Quellcodes und andere Mate-<br />rialien, die aus dem *Iterationen*-<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Projekt stammen, weiter zu erforschen, die Webseite des Projekts <https://iterations.space> zu besuchen und die dort enthaltenen Inhalte zu verwenden, zu kopieren, zu verändern und weiterzuverbreiten wie auch weiter zu bearbeiten. 

Ein großes Dankeschön geht an alle Künstler\*innen, Teilnehmer-<br />\*innen, Förderer\*innen, Organisationen und alle anderen, die am *Iterationen*-Projekt und an der Entstehung dieser Publikation beteiligt <br />waren.



<!-- __NOPUBLISH__ -->
<!-- EN -->

<!-- # header1 -->
<!-- ## header2 -->
<!-- *italics* -->
<!-- <https://iterations.space> -->
<!-- > the quote here -->


# Itera-<br />tions

The internet has become an amazingly complex place and can be
interpreted as a metaphor for the entire world - no individual is
ca-<br />pable of grasping its totality. For art practitioners interested in
technology, this gives reason to get together to collectively
understand, use, develop, or dismantle the main infrastructure of our
times. To take apart and hack together networks of our own, invent and
run multiple internets and build futures that we would like to inhabit.

Although Iterations’ partner organisations (Hangar, esc, Constant, and
Dyne.org) all operate in the same field, they have different points of
departure, missions and modes of operation. For this project, we
intended to establish common methodologies, which were implemented
differently by each organisation.

As a collaboration between several art and culture organisations based
in Europe, Iterations is co-funded by the Creative Europe framework of
the European Commission. Although the project springs from our shared
conviction that collectivity in the arts needs investigation and
attention, the project is also formatted around the criteria of the
Creative Europe framework.
 
An example of this is the fact that we enter into a programme that
supports “transnational cooperation projects from different countries”,
meaning that we endorse a paradigm of increased mobility. Whether we
choose to meet and work remotely through online tools, or we travel by
train or airplane, our actions are based on consumption of energy and
increasing carbon footprints. Another example of how our work is
influenced through such a framework is that the geographical
delimitation to European borders is not self evident for our
organisations, as in many other situations we work with partners from
outside the EU. This is not part of our usual set of working criteria.
 We, early-twentyfirst-centurists are living in the post-capitalist
ruins that have become our collective home. We volunteer to doing some
housekeeping, maintenance, restoration. This requires that we learn to
work together, to negotiate and speak, to struggle, deal and sometimes
fail.
 
If a shared long term vector between us is to speed up the end of
capitalism, then how are projects such as Iterations benificial to that?
We decided to rather than stating this as an explicit part of its
mission, make an effort by creating space and opportunity for critical
art practices to develop. Stimulating awareness, sensitivity, intuition
for the politics of tools and technologies, through bringing about
togetherness and exchange.

Iterations was hence conceived as a series of artistic residencies in
different locations around Europe. Every residency was thought of as an
iteration of the previous one, and would later be iterated in the next.
A handover meeting was the moment where art pieces, processes,
materials, concepts where handed from one group of resident artist to
the next, as a re-starting point from which to try again.

In December 2017 the starting seminar took place in Hangar, where some
common outsets were proposed and discussed. In spring 2018, a group of
30+ artists, hackers, the-<br />ater makers and musicians embarked for a shared
residency to Giampilieri, a small village hidden between the sea and the
mountain in the metropolitan area of Messina, Sicily. This
“Trasformatorio” residency was organised by Dyne.org. The works produced
during the resi-<br />dency were festively presented across the village. A song
was selected as the material to be remixed by the artists who joined for
the next residency; the performance, which was in Sicilian dialect,
introduced the challenges of multilingualism. During the residency that
followed in Barcelona in November 2018, the artists concentrated around
forming a collective persona: a groupal artistic identity with the name
of Rica Rickson, an identity/entity that can be inhabited by each of the
group or many of them at the same time.

In May 2019, several presentations, laboratories, and meetings later, a
group of artists worked together under the name *common ground* on
creating a site-specific installation for the exhibition "Collaboration
Contamination" in the space of esc mkl, Graz. Over the summer of 2019,
two residencies were organised by Constant in Brussels, resulting in the
exhibition “Operating / Exploitation” in Bozar-Lab in October,
immediately followed by the work-session “Collective Conditions”.

> “*Iterations investigates the future of artistic collaboration in a digital context*”

This tag line was used throughout the four years of the project's
duration. The basic concept was to “iterate” an action in several
similar, yet different circumstances and to strengthen collaborative and
collective strategies in artistic practice(s).

Let's take a closer look at the words in that core sentence “investi-<br />gate
the future of artistic collaboration in digital contexts”.

<framebreak>

## Investigate
 
That is: the process of investigation. The means of inquiry that were
developed were led by the practice of doing together: doing artistic
practice together, doing art together, doing in the understanding of
acting, of being active, of acting, of having agency as self-aware
entities. Although Iterations followed a described methodology, the way
we investigated was multiple: led by “intuition”, affinities, impulses
as opposed to “Re-search” (searching again for what we know is already
there, to prove a thesis). Is this then what we could call “to
experiment”? Playing without a specified goal, or exploiting living
bodies for science: the participants as lab rats in a white cube test
tube? That is far from what we were after. Nevertheless, the particular
situation to put together artists that had not collaborated
previously,in an artificial setting in time and space, determined many
factors of the interaction. Several times during the last 3 years we
stepped back, re-set the conditions back to the starting point, and
tried again. We reiterated.

The set of initial conditions and the methodologies of the project
involved the discussion-in-action (or action-in-discussion) of the
collective artistic process, and this affected the development of the
project itself. Concrete progress of the project, plus all the voices
and bodies involved, change its implementation.

That also meant friction. Is any collaboration free of friction, at all?
Are conflicts an unavoidable (side)effect of an artificial setting such
as this one? Does collective production exist without these challenges?
It seems hard to go out of recurrent group dynamics, maybe especially in
such a short experimental time (usually 2 weeks). Still, one aspect was
clear from the beginning: in Iterations, the goal of the investigation
is not necessarily or mainly the resulting art piece, but rather the
processual conditions: the labor, the exchanges, the *experience of
togetherness*.

## The Future of

Starting a rule-based Iteration creates new futures. And they are
plural. They have repercussions on the here and now:quantum presences and
*diffractions*. Is this close to what the project envisioned artists could do? To change
perspectives and bring new orientations to trusted concepts?

We might not know how to go in one direction together, but we at least
have to try to learn to negotiate with materials that matter. The future
is collective, or it is not likely to be. Future not as a promising and
well-balanced utopic picture that we can provide for, but as a plural
 forthcoming in the here and now. There is no waiting period before this
future, no open gap available for hope or despair. Instead, a form of
touchable future departing from our hands and out of our control. 

## Artistic <br />Collaboration

Inspired by recursive forms of collaboration as they exist in open
source software development (F/LOSS), the project Iterations applies
repetition and circularity to artistic methodologies: processes in which
the output from one activity can be used as inspiration and input to the
next, and can also be left aside without consideration.

We were and are inspired by F/LOSS practices that include strategies to
contribute to projects one finds important, no matter their starting
point; we witness a rigid underexposure of collective practice in
production and representation in contemporary art; and we also witness
that much artistic development of concepts, tools, methods, and
approaches are done collectively, or in collaboration.

As such, the series experimented with collective creation, joint
authorship, and group conditions. Each iteration  was an open invitation
for re-appropriation. Materials, code, images, instructions, and
documentation were and are shared under open content licenses on the
website of the project.

In relation to that, the the “handover meetings” were essential to the
collaborative art process as occasions when art pieces, thought
processes, raw materials and concepts were given to the next group of
people to continue. The handover moments were, then, the sharing of an
ongoing process and a situation: a gesture to bridge, to give away some
element of the lived experience of the iteration. The 'handover' was in
every case a metamorphosis, and a becoming action. Therefore, the
exchange among artists was not neccesarily of materials, but also of
ideas, of experiences and sometimes of data. 

In some key moments, the group involved in Iterations decided to
challenge the term *collaboration*, and to try to go deeper into the
concept of the *collective*. The shift can be described as follows: in a
collaborative setting different people work, labor, act together and
there are possible traces in an authorship, whose concept is to an
extent based on  a collective setting, where the separation between the
different contributions is not clear anymore. This means that, in some
moments, the development of the project challenged the decision made at
the beginnig of using the word collaboration instead of collective
action. The necessity of creating unified criteria is a hard demand.
Maybe, generating common strings or media would have been more effective
and less painful. Yet, the question remains if not precisely in the
struggles encountered in each implementation of the project,  in thecore
of what Iterations is about: how to (make it possible to) act together.

During the different phases, we witnessed an increasing abundance of
“co”, “col” and “con” prefixes (with, together, joint). In the course of
the project, conflict became to be considered a constructive element.
Staying 'with' the trouble as a way to not let go, to make space for
difficult relations, that are necessary for practices that want to
embrace difference. Yes, *belonging* is a difficult precondition to
fulfill a goal, but it gives space for alliances. At the end to stand with, to be part,
can also be a collective statement, an action of bodies in alliance.
Corporalities that keep their individuality.

Iterations has created situations in which differences were encountered,
discussed and transfered into work. In many cases the artists,
organisers, and other parties met for the first time and had not
previously worked together. Although not always easy, it is important
that tensions and frictions can arise from these situations and that
they are not ignored but taken seriously, creating productive
differences that earn a central place in the common practice.

## Digital

Over the last decades, the “digi-<br />tal” has shifted meaning. The early
internet induced the emergence of net art, critical cyborg identity and
gender tech experiments, and space for independent infrastructures. In
our present, this hopeful potential-<br />ity is overshadowed by techno
colonial, techno solutionist, big data correlating, energy slurping, all
invasive commodification and commerce. “Digital”, therefore, can not
remain un-annotated, but asks for critical positioning. AlongIterations
it was present that sometimes participants preferred to close their
laptops and share the time, the food, the temperatures, the sounds and
silences, the energy and the fatigue. When this happened, the digital
was perhaps no more than a strong metaphor.

## Contexts

What is context? What (a)symmetries are involved in the position of the
subject? Our work in art and culture is associated with many sectors of
society, with tech innovation schemes, with eco(-logical -nomical)
politics, it relates to futures we might not be able to imagine. We
think of the context in a way similar as how we think of the future. It
is right here, very close, but it is also the diffractive and plural
openness of the immediate things. We work in so many different
surroundings: the city, the rural, the remote, the center, the private,
the public.
 
Ecology, post truth times, musea and exhibitions versus performative
actions: how are the different contexts influencing the iterations,
their dynamics and their outcomes? Ecology as a big topic, the
post-human as question, as method. And along all that, a necessity of
making kin, of understanding, of completing and of contaminating.
Collectives are also formed from intrusions  and alterations,
transpositions or accidents. The context is concrete and dislocated at
the same time. Who is here? Who is near us? Who is doing this with us?
When is this happening? And where? The dislocation, the intuitive
bringing together of separated places, things and people, as a method of
establishing a context.

## This publication

This publication aims to create openings and inspire new concepts for
work yet to be developed. As a vessel it is filled with hybrid content
that can be unpacked and remixed. Inside this book, contributions of
some participants from the past and Iterators from the future coincide
in a joint effort to transport ideas to persons and contexts that are
yet unknown to us. As such, this publication could be read as an
Iteration in itself.
 
The contributions are related to approaches, attitudes, positions that
emerged from the Iterations project. The editors created a set of
descriptions of common threads that were called “handles”, and invited
each contributor to respond to one of them. During the 'editorial
sprint' to process the material, *scores* were produced that allowed for a
transversal reading of the contributions. This practice was named
“X-dexing” (editors Jara Rocha and Manetta Berends explain more about
these methodologies in the booklet within this publication).
 
Language simultaneously connects and divides, it includes and excludes.
The languages to- or from which the texts that you will find here are
translated, are the ones that are proper to the partner organisations.
English is not one of them, but it is present here because it was the
main communication language during the project. While speaking and
writing it, we transform it to a polyphonic, mongrolleded device. Being
with participants of more than 30 nationalities in this project, we
colored it with spores of our various mothertongues, leaving traces that
are also tangible throughout this publication.

Some contributions are based on documents that result from Iterations
activities. *common ground* sketched a musical score, based on highlighted areas in the map
of the exhibition “Collaboration Contamination”, and that mark acoustic
elements from the exhibition and can be used as score for future (sound)
works.

The residency and exhibition named “Operating / Exploitation” is the
source for the contribution “Dear visitor,” by *Behuki*.

The introduction by *Collective Conditions*, “Three Documents”, situates
several groupal writing practices that have emerged in conjunction with
the Constant worksession “Collective Conditions”.

*Rica Rickson* reflects in multicolor on their fresh birth that came about
during their Hangar residency. In an interior dialogue Rica considers
their collective body, as well as their multiplicity entanglements to
the art world, desires, needs and genealogies.

Next to these contributions, two invitations were extended for
companion-articles: “holding spell” by *Kym Ward* notes that meetings
between people can be moments of convergence and enchantment. In “Por
debajo y por los lados. Sostener infraestructuras feministas con ficción
especculativa” (“From Below and From the Sides. Supporting Feminist
Infrastructures with Speculative Fiction”), *spideralex* brings together
techniques and formulations around world-making and future-thinking
community infrastructures.

This publication is not conceived as a catalogue. Shared work has the
right to be disseminated, to have a life of its own, to be exposed to
brilliant influences, benefit from the additions of others and
transform. So if you are interested in exploring further the images,
texts, art, soundrecordings, source codes, and other materials that
originated from the Iterations project, we warmly invite you to explore
the website of the project: <https://iterations.space> and to use, copy,
change and re-distribute its contents. The site contains also some
extra tranlations of contributions to this publication.

A huge thank you goes out to all artists, participants, funding bodies,
organisations, and all who have contributed to the Iterations project
and to this publication.

<!-- __NOPUBLISH__ -->
<!-- ES -->

<!-- # header1 -->
<!-- ## header2 -->
<!-- *italics* -->
<!-- <https://iterations.space> -->
<!-- > the quote here -->

# Itera-<br />ciones

Internet se ha convertido en un espacio increíblemente complejo que se
puede interpretar como una metáfora del mundo: ninguna persona es capaz
de abarcarlo entero. Para las personas hacedoras de  arte e interesadas
en la tecnología, es un buen motivo para agruparse y de manera colectiva
comprender, usar, desarrollar o desmantelar la principal infraestructura
de nuestra época. Para desmontar o *hackear* conjuntamente redes que sean
nuestras, inventar y gestionar múltiples internets, construir futuros en
los que nos gustaría vivir.

Las organizaciones socias de Iteraciones (Hangar, esc, Constant y
Dyne.org) trabajan en el mismo campo, pero tienen distintos puntos de
partida, misiones y modos de operar. Para este proyecto, hemos intentado
establecer metodologías comunes, que cada organización ha aplicado a su
manera.

Iteraciones es una colaboración entre varias organizaciones dedicadas al
arte y la cultura en Europa, y ha sido cofinanciada por el programa
Europa Creativa de la Comisión Europea. Aunque el proyecto surge de
nuestra convicción compartida de que la colectividad en el arte requiere
investigación y atención, también se ha configurado en base a los
criterios del marco Europa Creativa.

Ejemplo de ello es el hecho de que entramos en un programa que apoya
“proyectos transnacionales de cooperación desde diferentes países”, es
decir que contribuímos al paradigma de la creciente movilidad. Tanto si
decidimos reunirnos y trabajar a distancia por medio de herramientas
*online*, como si viajamos en tren o en avión, nuestras acciones se basan
en el consumo de energía y en una huella de carbono cada vez mayor. Otro
ejemplo de la influencia de dicho marco en nuestro trabajo es que
nuestras organizaciones no dan por sentada la delimitación geográfica de
las fronteras europeas: en muchas situaciones colaboramos con socios
externos a la UE. No es parte de nuestros criterios habituales de
trabajo. Nosotras, las personas de principios del siglo XXI, estamos
viviendo en las ruinas poscapitalistas que se han convertido en nuestro
hogar colectivo. Nos ofrecemos voluntariamente a hacer un poco de
limpieza, mantenimiento, restauración. Para ello es necesario que
aprendamos juntes, que negociemos y hablemos, que luchemos, lidiemos con
ello y que, en algunas ocasiones, fracasemos.

Si uno de los vectores que a largo plazo  nos unen es acelerar el fin
del capitalismo, ¿en qué ayudan proyectos como Iteraciones? En lugar de
comenzar explícitamente esta acción como parte de ello, hemos decidido
hacer el esfuerzo de crear un espacio y una oportunidad para que se
desarrollen prácticas artísticas críticas. Estimular la conciencia, la
sensibilidad, la intuición para las políticas de herramientas y
tecnologías, por medio de la generación de unidad e intercambio.

Por consiguiente, Iteraciones ha concebido una serie de residencias
artísticas en diferentes lugares de toda Europa. Cada residencia se ha
pensado como una reiteración de la anterior, y será reiterada en la
siguiente. En cada reunión de relevo, las obras de arte, procesos,
materiales y conceptos se pasaban de un grupo de artistas residentes al
siguiente, como un nuevo punto de partida desde el que intentarlo de
nuevo.

En diciembre de 2017 se celebró el seminario inicial en Hangar; allí se
propusieron y debatieron algunos principios comunes de partida. En la
primavera de 2018, un grupo de más de 30 artistas, *hackers*, dramaturgues
y músiques se apuntaron a una residencia en Giampilieri, un pueblito
escondido entre el mar y la montaña en el término municipal de Messina,
Sicilia. Esta residencia “Transformatorio” fue organizada por Dyne.org.
Las obras producidas durante la residencia se presentaron de manera
festiva en el pueblo. Se escogió una canción como material a remezclar
por les artistas que se unían a la siguiente residencia; la performance,
en dialecto siciliano, presentaba los desafíos del multilingüismo.
Durante la residencia siguiente, realizada en Barcelona en noviembre de
2018, les artistas se concentraron formando una persona colectiva: una
identidad artística grupal con el nombre de Rica Rickson y cuya
identidad/<br />entidad podía ser ocupada por cada una de los conformantes del
grupo o por varias a la vez.

En mayo de 2019, un grupo de artistas colaboraron en varias
presentaciones, laboratorios y reuniones posteriores, bajo el nombre de
*common ground*, en la creación de una instalación *ad hoc* para la
exposición "Collaboration Contamination" en el espacio de esc mkl, en
Graz. Durante el verano de 2019, Constant organizó dos residencias en
Bruselas, de las que resultó la exposición "Operating / Exploitation" en
octubre en Bozar-Lab, e inmediatamente después la sesión de trabajo
“Collective Conditions”.

> "Iteraciones investiga el futuro de la colaboración artística en
contextos digitales".

Este lema se ha venido usando durante los cuatro años que ha durado el
proyecto. El concepto básico es "iterar" la acción en varias
circunstancias similares pero diferentes y fortalecer las estrategias
colaborativas y colectivas en la(s) práctica(s) artística(s).

Vamos a analizar más detenidamente cada una de las palabras de esa frase
nuclear: "investiga el futuro de la colaboración artística en contextos
digitales".

## Investiga

Es decir: el proceso de investigación. Los medios de indagación que se
desarrollaron estaban guiados por la práctica de hacer las cosas juntes:
hacer juntes la práctica artística, hacer juntes el arte, hacer juntes
la comprensión de la interpretación, de estar activa, de actuar, de
tener capacidad de acción siendo entidades auto-conscientes. Aunque
Iteraciones seguía una metodología determinada, se pusieron en práctica
diversas formas de investigar: investigación guiada por la “intuición”,
afinidades, impulsos, por oposición a la “re-<br />search” (buscar-de-nuevo lo
que ya sabemos que está ahí, para demostrar una tesis). ¿Es esto lo que
llamaríamos “experimentar”? ¿Jugar sin objeto particular, o explotar
cuerpos vivos en aras de la ciencia: les parti-<br />cipantes como ratas de
laboratorio en un cubo blanco convertido en probeta? Esto dista mucho de
lo que buscábamos. No obstante, la situación particular de reunir
artistas que no habían colaborado antes, en un entorno artificial en
cuanto al tiempo y al espacio, determinó muchos factores de la
interacción. Durante los últimos tres años hemos dado un paso atrás
muchas veces, hemos vuelto a establecer las condiciones del punto de
partida y lo hemos intentado de nuevo. Hemos reiterado.

El conjunto de condiciones iniciales y de metodologías del proyecto
implicaba el debate en acción (o la acción a debate) sobre el proceso
artístico colectivo, y esto afectaba al desarrollo del propio proyecto.
El progreso concreto del proyecto, sumado a las voces y organismos
implicados, cambia su implementación.

Eso también supone fricciones. ¿Acaso existe alguna colaboración sin
fricciones de algún tipo? ¿Los conflictos son un efecto (secundario)
inevitable de un escenario artificial como este? ¿Es posible la
producción colectiva sin estos desafíos? Parece complicado librarse de
estas dinámicas grupales recurrentes, tal vez especialmente en un plazo
de experimentación tan corto (normalmente, dos semanas). Sin embargo,
desde el principio estaba claro que en Iteraciones, el objetivo de la
investigación no era necesaria o principalmente la obra de arte
resultante, sino las condiciones del proceso: el trabajo, los
intercambios, la *experiencia de estar juntas*.

<framebreak> 

## El futuro de

Iniciar una Iteración basada en ciertas reglas genera nuevos futuros. Y
son plurales. Tienen repercusiones en el aquí y el ahora: presencias cuánticas y
*difracciones*. ¿Se aproxima esto a lo que el proyecto preveía que podían hacer les
artistas? ¿Cambiar las perspectivas y aportar nuevas orientaciones a
conceptos afianzados?

Es posible que no sepamos ir juntes en una misma dirección, pero al
menos tenemos que aprender a negociar con materiales importantes. El
futuro es coletivo, o probablemente no será. El futuro, no como esa
imagen utópica, prometedora y equilibrada que podemos concebir, sino
como un porvenir plural en el aquí y el ahora. Ante ese futuro no hay
periodo de espera, no hay un lapso disponible para la esperanza o la
desesperación. En lugar de eso, es una forma de futuro tangible que
empieza donde acaba la punta de nuestros dedos pero está fuera de
nuestro control.

## Colaboración artística

Inspirado en las formas recurrentes de colaboración que se dan en el
desarrollo de software de código abierto (F/LOSS), el proyecto
Iteraciones aplica la repetición y la circularidad a las metodologías
artísticas: procesos en los que el resultado de una actividad puede ser
usado como inspiración y punto de partida para el siguiente, pero
también se puede descartar sin miramientos.

Nos inspiraron —y nos inspiran— las prácticas de F/LOSS que incluyen
estrategias para contribuir a los proyectos que consideremos
importantes, indiferentemente del punto de partida; estamos viviendo una
falta total de familiaridad con las prácticas colectivas en la
producción y representación del arte contemporáneo; pero también está
ocurriendo que en muchos casos el desarrollo artístico de conceptos,
herramientas, métodos y enfoques se realiza de manera colectiva o en
colaboración.

Como tal, la serie experimentó con la creación colectiva, la autoría
conjunta y las condiciones de grupo. Cada iteración era una invitación
abierta a la reapropiación. Se compartieron —y se comparten— materiales,
código, imágenes, instrucciones y documentación con licencias de
contenido abierto en la página web del proyecto.

A ese respecto, las “reuniones de relevo” son esenciales para el proceso
artístico colaborativo, pues constituyen el momento en el que las obras
de arte, los procesos ideados, los materiales en bruto y los conceptos
se traspasan al siguiente grupo de personas para que lo continúen. Los
momentos de relevo consistían, por tanto, en la puesta en común de una
situación y un proceso en curso: un gesto para enlazar, para regalar
elementos de la experiencia de iteración vivida. En todos los casos, el
“relevo” era una metamorfosis, una acción favorecedora. Por
consiguiente, el intercambio entre artistas no era necesariamente de
materiales, sino también de ideas, experiencias y en ocasiones datos.

En algunos momentos clave, el grupo implicado en Iteraciones decidía
cuestionar el término *colaboración* e intentar ir más allá en el concepto
de *colectivo*. El cambio se puede describir de la siguiente manera: en un
escenario colaborativo, diferentes personas trabajan, producen, actúan
juntas y hay posibles trazas de autoría, cuyo concepto se basa en cierta
medida en el escenario colectivo, donde la separación entre las
diferentes aportaciones deja de estar clara. Eso significa que, en
algunos momentos, el desarrollo del proyecto pone en cuestión la
decisión tomada al principio de usar la palabra “colaboración” en lugar
de “acción colectiva”. La necesidad de crear criterios unificados es una
demanda difícil de cumplir. Quizás hubiera sido más eficaz y menos
doloroso generar unas condiciones o medios comunes. Y sin embargo, la
cuestión sigue candente, si no en las dificultades que se han encontrado
en la implementación de cada proyecto, sí en el meollo de Iteraciones:
cómo (hacer posible) actuar juntes.

Durante las distintas fases, vimos un incremento del uso de los prefijos
“co-”, “con-” (con, conjunto). A lo largo del proyecto, el conflicto
llegó a ser considerado un elemento constructivo. *Quedarse “con” el
problema* como una forma de no dejarlo pasar, de hacer espacio para las
relaciones difíciles, que son necesarias para las prácticas que
pretenden abrazar la diferencia. Sí, la *pertenencia* es una condición
previa complicada para cumplir una meta, pero deja espacio para las
alianzas. A fin de cuentas, quedarse, ser parte, puede ser también una
afirmación colec-<br />tiva, una acción de cuerpos en alian-<br />za. Las
corporalidades pueden mantener su individualidad.

Iteraciones ha creado situaciones en las que las diferencias se
encontraban, se afirmaban y se transferían a las obras. En muchos casos
las artistas, las organizadoras y otras partes se encontraban por
primera vez y no habían trabajado juntas antes. Aunque no es sencillo,
es importante que las tensiones y fricciones que pueden surgir de estas
situaciones no se ignoren, sino que se tomen en serio, generando
diferencias productivas que se merecen un lugar central en la práctica
común.

<framebreak>

## Digital

A lo largo de las últimas décadas, ha cambiado el significado de
“digital”. La primera época de internet suscitó la aparición del Net
Art, de experimentos tecnológicos cruciales sobre la identidad y el
género virtuales, así como un espacio para infraestructuras
independientes. A día de hoy, esa esperanzadora potencialidad está
eclipsada por una mercantilización y comercio invasivos,
tecnocoloniales, tecnosolucionistas, correlacionados con el Big Data y
que absorben toda la energía. El término “digital”, por consiguiente, no
puede usarse sin explicación, requiere un posicionamiento crítico.
Durante Ite-<br />raciones, teníamos presente que a veces les participantes
preferían cerrar los ordenadores y compartir el tiempo, la comida, la
temperatura, los sonidos y silencios, las energías y las fatigas. Cuando
esto ocurría, quizás lo digital no era más que una metáfora muy potente.

## Contextos

¿Qué es el contexto? ¿Qué (a)simetrías participan en la posición del
sujeto? Nuestro trabajo en arte y cultura se asocia con muchos sectores
de la sociedad, con planteamientos de innovación tecnológica, con
políticas eco(-lógicas, -nómicas); se refiere a futuros que tal vez no
seamos capaces de imaginar. Concebimos el contexto de igual manera que
el futuro. Está aquí mismo, muy cerca, pero es también una apertura
difractiva y plural a la inmediatez. Trabajamos en múltiples entornos
diferentes: la ciudad, el medio rural, lo remoto, el centro, lo privado,
lo público.

La ecología, la era de la posverdad, los museos y exposiciones frente a
las *performances*... ¿De qué manera influyen los diferentes contextos en
las iteraciones, sus dinámicas y resultados? La ecología como gran tema; y
lo post-humano como cuestión, como método. Y junto a todo ello, la
necesidad de generar parentesco, de comprender, de completar y
contaminar. Los colectivos se forman también a base de intrusiones y
alteraciones, transposiciones o accidentes. El contexto es al mismo
tiempo concreto y dislocado. ¿Quién está aquí? ¿Quién está a nuestro
lado? ¿Quién está haciendo esto con nosotras? ¿En qué momento está
ocurriendo? ¿En qué lugar? La dislocación, la reunión intuitiva de
lugares, objetos y personas separadas, como método para establecer el
contexto.

## Esta publicación

El objetivo de esta publicación es crear apertura de miras e inspirar
nuevos conceptos de trabajo aún por desarrollar. Como un navío, está
llena de contenidos híbridos que se pueden desplegar y remezclar. En
este libro, las aportaciones de algunes participantes en ediciones
pasadas y futuras de iteraciones coinciden en un esfuerzo conjunto para
trasladar ideas a personas y contextos que todavía nos son desconocidos.
En ese sentido, esta publicación se puede considerar una iteración en sí
misma.

Las contribuciones están relacionadas con enfoques, actitudes, posturas
que surgieron del proyecto Iteraciones. Las editoras han creado un
conjunto de descripciones de los hilos comunes a las que hemos llamado
“asideros” (*handles*), y hemos invitado a cada participante a responder a
uno de ellos. Durante el *book sprint* para procesar el material, se han
elaborado unas partituras (*scores*) que permiten la lectura transversal
de las aportaciones. Esta práctica se ha denominado “exdexación”, por
oposición a "indexación" (las editoras Jara Rocha y Maneta Berends
explican más estas metodologías en el cuadernillo incluido en esta
publicación).

Las lenguas conectan y dividen simultáneamente, incluyen y excluyen. Las
lenguas a las que se han traducido —o desde las que se han traducido—
los textos que encontrarás aquí, son las propias de las organizaciones
socias. El inglés no lo es, sin embargo está presente aquí por haber
sido la principal lengua de comunicación durante el proyecto. Al
hablarlo y escribirlo, lo transformamos en un artefacto polifónico y
mestizo. Siendo les participantes en este proyecto de más de 30
nacionalidades, hemos coloreado la lengua con esporas de nuestras
distintas lenguas maternas, dejando rastros tangibles de ello también en
esta publicación.

Algunas contribuciones se basan en documentos resultado de las
actividades de Iteraciones. *common ground* esbozaron una partitura musical partiendo de las
zonas destacadas en el mapa de la exposición “Collaboration
Contamination”, que marca los elementos acústicos de la exposición y
puede ser usada como partitura para futuras obras (sonoras).

La residencia y exposición titulada “Operating / Exploitation” es la
fuente de la contribución “Dear visitor,” por *Behuki*.

La introducción redactada por *Collective Conditions*, “Three Documents”,
explica varias prácticas grupales que han surgido en conjunción con los
talleres “Collective Conditions” de Constant.

*Rica Rickson* refleja en multicolor lo que acababa de nacer durante su
residencia en Hangar. En un diálogo interior, Rica considera su cuerpo
colectivo, así como sus múltiples enredos con el mundo del arte, los
deseos, las necesidades y las genealogías.

Junto a estas contribuciones, se hicieron dos invitaciones para
artículos complementarios: “holding spell” por *Kym Ward* apunta que los
encuentros entre personas pueden ser momentos de convergencia y magia.
En “Por debajo y por los lados. Sostener infraestructuras feministas con
ficción especulativa” , *spideralex* conjuga
técnicas y formulaciones sobre las infraestructuras comunitarias de
reflexión sobre el futuro y la construcción del mundo.

Esta publicación no está concebida como un catálogo. El trabajo
compartido puede ser difundido, tener vida propia, enfrentarse a
influencias brillantes, beneficiarse de los añadidos de otras personas y
ser transformado. Si tienes interés en explorar más a fondo las
imágenes, textos, arte, grabaciones sonoras, códigos fuente y otros
materiales originados en el marco del proyecto Iteraciones, te damos una
afectuosa bienvenida a la página web del proyecto:
<https://iterations.space>; puedes usar, copiar, modificar y redistribuir
sus contenidos. Se incluyen también en la web algunas versiones
traducidas de contribuciones a esta publicación.

Un agradecimiento enorme a todes les artistas, participantes,
financiadoras, organizaciones y todas las personas que han contribuido
al proyecto Iteraciones y a esta publicación.


<!-- __NOPUBLISH__ -->
<!-- FR -->

<!-- # header1 -->
<!-- ## header2 -->
<!-- *italics* -->
<!-- <https://iterations.space> -->
<!-- > the quote here -->

# Itéra-<br />tions

Internet est devenu un endroit extrêmement complexe, inter-<br />prétable comme
une métaphore du monde entier : il est impossible d’en saisir la
totalité. Pour les professionnel·le·s de l’art qui s'intéressent à la
technologie, c’est une raison de se retrouver pour comprendre, utiliser,
développer ou démonter de façon collective la principale infrastructure
de notre époque. Démonter et pirater ensemble nos propres réseaux,
inventer et administrer des Internets multiples et bâtir des futurs que
nous souhaiterions habiter.

Même si les organisations partenaires d’Itérations (Hangar, esc,
Constant et Dyne.org) travaillent toutes dans le même domaine, leurs
points de départ, missions et modes de fonctionnement diffèrent les uns
des autres. Pour ce projet, nous entendons établir des méthodes
communes, mises en œuvre de différentes façons par chaque organisation.

Fruit de la collaboration de plusieurs organisations artistiques et
culturelles en Europe, Itérations est co-financée par le programme
Creative Europe de la Commission européenne. Bien que le projet surgisse
de la conviction partagée selon laquelle le collectif dans les arts
nécessite des recherches et une attention particulières, il s'agit
également d’un projet structuré selon les critères du programme Creative
Europe.

Exemple en est le fait que nous entrions dans un cadre soutenant <br />« les
projets de coopération transnationale entre différents pays », autrement
dit : nous appuyons le paradigme d’une mobilité toujours plus grande.
Que nous choisissions de nous retrouver et travailler ensemble à
distance par le biais d’outils en ligne ou que nous voyagions en train
ou en avion, nos actions se fondent sur la consommation d’énergie et une
empreinte carbone accrue. Un autre exemple de l’influence de ce cadre
sur notre travail est la délimitation géographique aux frontières
européennes, un aspect qui ne va pas de soi pour nos organisations,
étant donné que nous travaillons en de nombreuses occasions avec des
partenaires hors de l’UE. Cette restriction spatiale ne fait pas partie
de nos critères habituels de travail. Nous, génération du début du 21e
siècle, vivons sur les ruines post-capitalistes devenues notre maison à
tou·te·s. Nous nous portons volontaires pour en faire le ménage,
l’entretien, la rénovation, des tâches qui nécessitent que nous
apprenions à travailler ensemble, à négocier et à parler, à lutter, à
faire face et parfois à échouer.

Si nous sommes toutes et tous animé·es de longue date par l’accélération
de la fin du capitalisme, en quoi des projets tels qu’Itérations
peuvent-ils être bénéfiques ? Au lieu de déclarer ceci comme composante
explicite de sa mission, nous avons décidé de déployer des efforts pour
créer des espaces et des opportunités en vue de développer des pratiques
d’art critiques. Stimuler la prise de conscience, la sensibilité,
l’intuition des politiques d’outils et de technologies, tout en
suscitant de l’échange et de la solidarité.

Itérations a donc été conçu comme une série de résidences artistiques à
différents endroits en Europe. Chaque résidence a été pensée comme une
itération de la précédente, qui serait ensuite itérée dans la suivante.
Une réunion de passage de témoin est le moment où les œuvres d’art, les
processus, les matériaux, les concepts, sont transmis par un groupe
d’artistes résident·e·s au groupe suivant, comme point de ré-départ à
partir duquel réessayer.

En décembre 2017, un séminaire de démarrage s’est tenu au Hangar, au
cours duquel ont été proposés et discutés des débuts communs. Au
printemps 2018, un groupe d’une trentaine d’artistes, hackeur·se·s,
créateur·rice·s de théâtre et muscien·ne·s se sont embarqué·e·s dans une
résidence partagée à Giampilieri, un petit village niché entre mer et
montagne dans l’agglomération de Messine, en Sicile. Cette résidence
« Trasformatorio » a été organisée par Dyne.org. Les travaux produits
lors de cette résidence ont été présentés de façon festive dans tout le
village. Une chanson a été sélectionnée comme matériel à remixer par les
artistes participant à la prochaine résidence ; la performance, réalisée
dans l’un des dialectes du sicilien, a été l’occasion de présenter les
défis du multilinguisme. Lors de la résidence qui s’en est suivie à
Barcelone en novembre 2018, les artistes se sont focalisé·es sur la
formation d’un personnage collectif : une identité artistique de groupe
du nom de Rica Rickson, une identité/entité pouvant être habitée par
chaque personne du groupe ou par plusieurs simultanément.

En mai 2019, après plusieurs présentations, laboratoires et réunions, un
groupe d’artistes a travaillé ensemble sous le nom de <br />« common ground »
(terrain commun) pour créer une installation _in situ_ pour l'exposition
« Collaboration Contamination » dans l’espace d’esc mkl à Graz. Au cours
de l’été 2019, deux résidences ont été organisées par Constant à
Bruxelles, avec pour résultat l’exposition « Operating / Exploitation »
au Bozar-Lab en octobre, aussitôt suivie par la session de travail
<br />« Conditions Collectives ».

> « Itérations étudie le futur de la collaboration artistique dans un
contexte numérique. »

Ce slogan a été utilisé tout au long de la durée du projet, pendant
quatre ans. Le concept de base était « d’itérer » une action dans des
circonstances très similaires, mais cependant différentes, et de
renforcer les stratégies collaboratives et collectives des pratiques
artistiques.

Examinons plus en détail les mots de la phrase clé « étudier le futur de
la collaboration artistique dans des contextes numériques ».

## ÉTUDIER

Autrement dit : le processus d’investigation. Les moyens d’enquête
déployés ont été pilotés par la pratique du faire ensemble : réaliser la
pratique artistique ensemble, fabriquer de l'art ensemble, faire dans la
compréhension de l’agir, d’être actif·ve, d’interpréter, d'avoir du
pouvoir en tant qu’entités auto-conscientes. Bien qu’Itérations ait
suivi une <br />méthodologie décrite, notre mode d’investigation était
multiple : animé par « l'intuition », les affinités, les impulsions, par
opposition à la « re-cherche » (chercher encore ce que nous savons déjà
là, pour prouver une thèse). Est-ce à ce moment-là que nous pouvons
parler « d’expériment-<br />ation » ? Jouer sans objectif spécifique, ou
exploiter des organismes vivants pour la science : les participant·e·s
comme rats de laboratoires dans une éprouvette au milieu d’une salle
d’exposition ? C’est bien loin de ce que nous poursuivons. Néanmoins, la
particularité d’une situation où se réunissent des artistes n’ayant
jamais collaboré auparavant, dans un cadre artificiel du point de vue du
temps comme de l’espace, a permis de déterminer nombre de facteurs de
l’interaction. À plusieurs reprises ces trois dernières années nous
avons reculé, ré-défini les conditions en revenant au point de départ,
pour essayer, encore et toujours. Nous avons réitéré.

L’ensemble des conditions et méthodologies initiales du projet
impliquait la discussion-en-action (ou action-en-discussion) du
processus artistique collectif et ceci a eu un effet sur le
développement du projet lui-même. Les progrès concrets du projet,
ajoutés à toutes les voix et corps impliqué·e·s, ont modifié sa mise en
œuvre.

Cela veut aussi dire friction. Une collaboration peut-elle vraiment
exister sans aucun type de friction ? Les conflits sont-ils des effets
(secondaires) inévitables d’un contexte artificiel comme celui-ci ? La
production collective existe-t-elle sans ces défis ? Il apparaît
difficile de sortir des dynamiques de groupe récurrentes, et peut-être
tout particulièrement dans un temps d’expérimentation court (2 semaines,
habituellement). Pourtant, un aspect était clair dès le départ : dans
Itérations, le but de l'investigation n’était pas nécessairement ou
principalement l'œuvre d’art qui verrait le jour, mais plutôt les
conditions du processus : le travail, les échanges, _l’expérience de
l’union et de la solidarité_.

## LE FUTUR DE

Démarrer une Itération fondée sur des règles crée de nouveaux futurs.
Des futurs pluriels.

Qui ont des répercussions sur l'ici et l'immédiat : des présences et
diffractions quantum.

Est-ce proche de ce que le projet imaginait que pouvaient faire les
artistes ? Modifier les perspectives et apporter de nouvelles
orientations à des concepts acquis/ayant fait leurs preuves ? 

Nous ne savons peut-être pas comment aller ensemble dans une même
direction, mais au moins, nous nous devons d'essayer d’apprendre à
négocier avec les matériaux qui importent. Le futur sera collectif ou ne
sera pas. Le futur non comme une image utopique prometteuse et
équilibrée que nous pouvons alimenter, mais comme un avenir pluriel, ici
et maintenant. Il n’y a pas de période d’attente avant ce futur, aucun
fossé ouvert à l’espoir ou au désespoir, mais plutôt une forme de futur
palpable qui tire son origine de nos mains et qui échappe à notre
contrôle. 

## COLLABORATION ARTISTIQUE

Inspiré par les formes récursives de collaboration à l'instar de celles
qui existent dans le développement de logiciel libre et open source
(F/LOSS), le projet Itérations applique la répétition et la
circularité aux méthodes artistiques : des processus au cours desquels
le résultat d’une activité peut être utilisé comme inspiration et
contribution pour la suivante, mais aussi être laissé de côté sans être
pris en considération.

Nous nous sommes inspiré·e·s et continuons de nous inspirer des pratiques
F/LOSS, qui comprennent des stratégies pour contribuer aux projets que
l’on considère comme importants, quel que soit leur point de départ ;
nous constatons une sous-exposition flagrante de la pratique collective
dans la production et la représentation dans l’art contemporain ; et
nous observons également qu’un grand nombre de développements
artistiques de concepts, outils, méthodes et approches sont effectués de
façon collective ou en collaboration.

En tant que telles, les séries ont expérimenté la création collective,
la titularité collective de l'œuvre et les conditions de groupe. Chaque
itération a été une invitation ouverte à la ré-appropriation. Les
matériaux, les codes, les images, les instructions et la documentation
ont été et sont partagés sous licence libre sur le site web du projet.

En la matière, les réunions de <br />« passage de témoin » ont été
essentielles au processus d’art collaboratif, devenant des occasions
pour les œuvres d'art, les mécanismes de pensée, les matières brutes et
les concepts d’être transmis au groupe suivant pour être poursuivis. Les
moments de passage de témoin étaient alors un partage de processus
continu et d’une situation : un geste pour jeter des ponts, céder
certains éléments de l’expérience vécue de l’itération. Le « passage de
témoin » a été dans chaque cas une métamorphose, une action de devenir.
Les échanges entre artistes ne concernaient pas nécessairement les
matériaux, mais également les idées, les expériences, et parfois les
données. 

À certains moments clés, le groupe impliqué dans Itérations a décidé de
remettre en cause le terme de collaboration, en essayant d’aller plus
en profondeur dans le concept de collectif. La transition peut être
décrite ainsi : dans un contexte collaboratif, différentes personnes
travaillent, œuvrent, agissent ensemble et il en existe des traces
possibles dans la titularité d'une œuvre, dont le concept se fonde dans
une certaine mesure sur un environnement collectif, où la séparation
entre différentes contributions n’est plus si claire. Autrement dit, à
certains moments, le développement du projet a remis en cause la
décision prise au départ d’utiliser le mot « collaboration » plutôt que
celui « d’action collective ». La nécessité de créer des critères
unifiés est une exigence épineuse. Générer des chaînes ou médias communs
aurait peut-être été plus efficace et moins douloureux. Toutefois, la
question demeure, sinon précisément dans les luttes rencontrées dans
chaque mise en œuvre du projet, du moins au cœur de la finalité
d’Itérations : comment (rendre possible l’)agir ensemble.

Au cours des différentes phases, nous avons constaté une abondance
toujours plus grandissante des préfixes « co- », « coll- » et « con- »
(avec, ensemble, conjoint). Tout au long du projet, les conflits ont
commencé à être considérés comme des éléments constructifs. Rester <br />
« avec » le problème, comme façon de ne pas lâcher prise, pour faire de
la place à des relations difficiles, nécessaires pour les pratiques
entendant célébrer la différence. <br />Oui, _l’appartenance_ est une
condition _sine qua non_ compliquée pour remplir un objectif, mais elle
laisse de la place aux alliances. Au final, être aux côtés de, faire
partie, peut également être une déclaration collective, une action de
corps en alliance. Des corporalités qui maintiennent leur individualité.

Itérations a créé des situations au cours desquelles des différences ont
été rencontrées, discutées et transférées dans le travail. Dans nombre
de cas, les artistes, organisateur·rice·s et autres parties se
réunissaient pour la première fois et n’avaient encore jamais travaillé
ensemble. Même si ce n’est jamais facile, il est important que ces
situations donnent lieu à des tensions et des frictions et que celles-ci
ne soient pas ignorées, mais bien prises au sérieux, créant alors des
différences productives qui méritent une place centrale dans la pratique
commune.

## NUMÉRIQUE

Ces dernières décennies, le sens du « numérique » a opéré une certaine
transition. Les débuts d’Internet ont suscité l’apparition de net art, de
l’identité critique cyborg, des expérimentations tech et genre, et un
espace pour les infrastructures indépendantes. Dans notre présent, cette
potentialité encourageante est éclipsée par le technocolonialisme, le
technosolutionnisme, la corrélation du big data, l’engloutissement des
énergies, la marchandisation et le commerce envahissants. Le <br />
« numérique » ne peut donc être laissé sans commentaire et appelle à un
positionnement critique. Au sein d’Itérations, il s’est avéré que les
participant·e·s préféraient parfois éteindre leurs PC portables et
partager du temps, un repas, les températures, les bruits et les
silences, l’énergie et la fatigue. Quand ceci se produisait, le
numérique n’était alors plus qu’une métaphore forte.

## CONTEXTES

Qu’est-ce que le contexte ? Quelles (a)symétries sont impliquées dans la
position du sujet ? Notre travail dans l’art et la culture est associé à
nombre de secteurs de la société, avec des modèles d'innovation
technologique, des politiques éco(-logique, -nomique), notre labeur
établit un lien avec des futurs que nous ne serons peut-être pas en
mesure d’imaginer. Nous pensons au contexte comme nous pensons au futur.
Il est là, tout proche, mais c’est aussi l’ouverture diffractée et
plurielle des choses immédiates. Nous travaillons au cœur
d’environnements si divers : la ville, le rural, le distant, le centre,
le privé, le public.

L’écologie, l’ère de la post-vérité, les musées et les expositions par
rapport aux actions performatives : de quelle(s) façon(s) les différents
contextes influencent-ils les itérations, leurs dynamiques et leurs
résultats ? L’écologie comme sujet majeur, le post-humain comme
question, méthode. Et en parallèle, la nécessité de créer de la parenté,
de comprendre, de compléter et de contaminer. Les collectifs sont
également composés d’intrusions et d’altérations, de transpositions et
d’accidents. Le contexte est simultanément concret et disloqué. Qui est
ici ? Qui est proche de nous ? Qui fait ça avec nous ? Quand ceci se
produit-il ? Et où ? La dislocation, la réunion intuitive de lieux,
choses et personnes chacun·e de leur côté, comme méthode d’établissement
de contexte.

## CETTE PUBLICATION

Cette publication entend créer des ouvertures et inspirer de nouveaux
concepts pour le travail qu’il reste encore à développer. Tel un
vaisseau, elle est emplie de contenu hybride qui peut être déballé et
remixé. Dans ce livre, les contributions de certain·e·s des
participant·e·s du passé et des Itérateur·rice·s du futur coïncident dans
un effort commun de transport des idées vers les personnes et les
contextes qui nous sont encore inconnu·e·s. Cette publication peut donc
être elle-même lue comme une itération.

Les contributions sont liées aux approches, attitudes, positions,
émanant du projet Itérations. Les éditeur·rice·s ont créé un ensemble de
descriptions de fils conducteurs qui ont été baptisés « poignées », et
invitaient chaque contributeur·rice à répondre à l’un de ces fils. Lors
du <br />« sprint éditorial » de traitement du matériel, des _partitions_ ont
été produites pour permettre une lecture transversale des contributions.

Cette démarche a été nommée <br />« x-dexage » (x-dexing, méthodologie
expliquée plus en détail par les éditrices Jara Rocha et Manetta Berends
dans le livret inclus dans la publication).

Le langage connecte et divise à la fois, il inclut et il exclut. Les
langues vers ou depuis lesquelles les textes que vous retrouverez ici
ont été traduits sont propres aux organisations partenaires. L’anglais
n’en fait pas partie, mais il est ici présent car il a servi de langue
de communication tout au long du projet. En le parlant et en l’écrivant,
nous le transformons en dispositif polyphonique et hybride. Grâce à la
présence de participant·e·s de plus de 30 nationalités différentes, nous
avons coloré ce projet de spores de nos différentes langues maternelles,
en y laissant des traces qui sont également tangibles et parcourent
toute cette publication.

Certaines contributions sont basées sur des documents résultant
d’activités d’Itérations. 

*common ground* ont esquissé une partition de musique, en
fonction des zones surlignées de la carte de l'exposition
« Collaboration Contamination » dont les éléments acoustiques peuvent
être utilisés comme partition pour de futurs travaux (sonores).
La résidence et l’exposition appelées « Operating / Exploitation » sont
la source de la contribution « Dear visitor, » (cher visiteur,) de
*Behuki*.
La présentation par *Conditions Collectives*, « Three Documents », situe
plusieurs pratiques d’écriture de groupe ayant émergé dans le sillage de
la session de travail « Conditions Collectives » de Constant.
*Rica Rickson* réfléchit en multicolore sur leur toute nouvelle
apparition, née lors de leur résidence au Hangar. Dans un dialogue
intérieur, Rica considère leur corps collectif ainsi que leur
multiplicité d’enchevêtrements avec le monde de l’art, les désirs, les
nécessités et les généalogies.

Aux côtés de ces contributions, deux invitations ont été lancées à des
articles-compagnons : « holding spell » de *Kym Ward* fait remarquer que
les rencontres entre les personnes peuvent être des moments de
convergence et d’enchantement. Dans « Por debajo y por los lados. Sostener
infraestructuras feministas con ficción especulativa » (Du dessous et
des côtés. Soutenir des infrastructures féministes avec la fiction
spéculative), *spideralex* réunit des techniques et formulations autour
des infrastructures communautaires de fabrication du monde et de
réflexion sur le futur.

Cette publication n'est pas conçue comme un catalogue. Le travail
partagé a le droit d’être diffusé, d'avoir une vie propre, d’être exposé
aux influences brillantes, de bénéficier d’autres ajouts et de se
transformer. Si vous êtes intéressé·e et souhaitez explorer davantage
les images, textes, œuvres, enregistrements sonores, codes source et
autres matériels émanant du projet Itérations, nous vous invitons
chaleureusement à explorer le site Web du projet :
<https://iterations.space> et à utiliser, copier, modifier et redistribuer
ses contenus. Le site contient également des traductions supplémentaires
des contributions à cette publication.

Un énorme merci à tou·te·s les artistes, participant·e·s, organismes de
financement, organisations et tou·te·s celleux qui ont contribué <br />au
projet Itérations et à cette <br />publication.

<!-- __NOPUBLISH__ -->
<!-- NL -->

<!-- # header1 -->
<!-- ## header2 -->
<!-- *italics* -->
<!-- <https://iterations.space> -->
<!-- > the quote here -->


# Itera-<br />ties

Het internet is een zeer complexe ruimte die geïnterpreteerd kan worden
als een metafoor voor de wereld. Geen individu is in staat haar
totaliteit te bevatten. Voor veel kunstbeoefenaars die zich in
technologie interesseren is dit een reden om elkaar op te zoeken, om de
belangrijkste infrastructuur van onze tijd samen te leren begrijpen, te
gebruiken, verder te ontwikkelen of juist te demonteren, om eigen
netwerken uit elkaar te halen en in elkaar te hacken, om meervoudige
internetten uit te vinden en toekomsten te bouwen die we graag willen
bewonen.
 
Hoewel de partnerorganisaties van Iteraties (Hangar, esc, Constant en
Dyne.org) allemaal in hetzelfde vakgebied opereren, hebben ze
verschillende uitgangspunten, missies en werkwijzen. Voor dit project
was het de bedoeling om gemeenschap-<br />pelijke methodes vast te stellen die
door elke organisatie anders zijn <br />geïmplementeerd.
 
Iteraties is een samenwerking tussen verschillende kunst en
cultuurorganisaties en wordt mede-<br />gefinancierd door het Creative Europa
programma van de Europese Commissie. Het project is ontstaan uit onze
gezamenlijke overtuiging dat artistieke collectiviteit aandacht en
onderzoek nodig heeft, maar het is ook opgezet aan de hand van de
criteria die gesteld werden binnen <br />Creative Europe.
 
We doen mee aan een programma dat transnationale coöperatie-projecten
uit verschillende landen ondersteunt, en daarmee ondersteunen we een
raamwerk dat de toename van mobiliteit ondersteunt. Of we nu kiezen om
op afstand te werken via online middelen, of dat we per trein of
vliegtuig reizen, onze <br />acties gebruiken energie en doen onze
CO2-voetafdruk toenemen. <br />Een voorbeeld van hoe ons werk wordt beïnvloed
door dit kader is dat het voor onze organisaties niet vanzelfsprekend is
om ons werk te beperken tot Europese landsgrenzen. Dit is geen onderdeel
van onze gebruikelijke werkcriteria. In veel andere situaties werken we
samen met partners van buiten de EU. Wij, vroeg-éénentwintigste
eeuwelingen, leven in de post-kapitalistische <br />ruïnes die ons
gemeenschappelijke huis zijn geworden. We bieden ons vrijwillig aan om
het huishouden, onderhoud en restauratie te doen, maar dit vraagt dat we
moeten samenwerken, onderhandelen, worstelen. We moeten het samen 
kunnen vinden en ook samen leren falen.
 
Een gezamenlijke langetermijnvector is dat we graag het einde van het
kapitalisme dichterbij zouden brengen. Maar hoe kunnen projecten zoals
Iteraties daarbij helpen? In plaats van dit tot een expliciet onderdeel
van de missie van het project te maken, besloten we moeite te doen om
ruimte en kansen te creëren voor de ontwikkeling van kritische
kunstpraktijken. We doen dit door het bewustzijn, de gevoeligheid en de
intuïtie voor de politiek van technologie te stimuleren en door
saamhorigheid en uitwisseling te bevor-<br />deren.
 
Iteraties werd opgezet als een <br />serie artistieke residenties die op
meerdere locaties in Europa plaats-<br />vonden. Iedere residentie bouwde
verder op een vorige residentie en voedde een volgende. Bij de
'hand-<br />over meetings' werden alle kunstwerken, artistieke processen,
materialen en concepten van de ene groep naar de andere groep
overgedragen, om zo een nieuw begin te vormen.
 
In december 2017 vond een eerste seminar plaats in Hangar. Daar werden
een aantal algemene doelstellingen voorgesteld en bediscussieerd. In het
voorjaar van 2018 startte een groep van ruim 30 kunstenaars, <br />hackers,
theatermakers en muzikanten een gezamenlijk verblijf in Giampilieri, een
klein dorp verscholen tussen de zee en de bergen in het stadsgebied van
Messina, Sicilië. Deze 'Trasformatorio' residentie was georganiseerd
door Dyne.org. Het werk dat geproduceerd werd tijdens het verblijf werd
feestelijk tentoongesteld op meerdere plekken in het dorp. Als materiaal om over te
dragen aan de volgende residentie werd gekozen voor een lied dat werd
gezongen in een Siciliaans dialect. Dit introduceerde de uitdagingen van
meertaligheid, een centraal thema in een internationale samenwerking
zoals deze. Tijdens de volgende residentie in Barcelona in 2018
concentreerden de kunstenaars zich op het creëren van een collectief personage. 
Rica Rickson is een gemeenschappelijke artistieke
identiteit/entiteit die door iedereen in de groep kan worden aangenomen.
Alleen, een aantal of door de hele groep.
 
Na verschillende presentaties, <br />laboratoria en bijeenkomsten, werkte een
groep artiesten in mei 2019 onder de naam  *common ground* aan een
site-specifieke installatie voor de tentoonstelling "Collaboration
Contamination", in de ruimte van esc medien kunst labor in Graz. Iets later, in de zomer
van 2019 organiseerde Constant twee residenties in Brussel, die
resulteerden in de expositie "Operating / Exploitation" die paatsvond in 
oktober in Bozar-Lab, gevolgd door de werksessie "Collec-<br />tive
Conditions".
 
> “Iteraties onderzoekt de toe-<br />komst van artistieke samenwerking in een digitale context”.
 
Gedurende de vier jaar dat het project duurde gebruikten we deze slogan
regelmatig. Het basisconcept van het project bestond uit het “itereren”
van een vergelijkbare actie in verschillende omstandigheden om
samenwerkings- en collectieve strategieën in artistiek werk te
versterken. Laten we de woorden uit de zin “het onderzoeken van de toekomst van
artistieke samenwerking in een digitale context” eens van dichterbij
bekijken.

## Onderzoek
 
Onderzoek vatten we op als een proces. De methodes die we ontwikkelden
worden getypeerd door de prakijk van samen “doen”: samen <br />artistieke
praktijken maken, samen kunst “doen”, doen opgevat als uitvoeren, actief
zijn, doen als in: invloed hebben als zelfbewuste entiteiten. Alhoewel
Iteraties een beschreven methodologie volgde was de <br />manier waarop we
onderzoek deden meervoudig, geleid door intuïties, <br />affiniteiten en
impulsen. Dit in tegenstelling tot “re-search” (iets onderzoeken waarvan
we het bestaan al kenden, om een theorie te bewijzen). Kunnen we dit dan
“experimenteren” noemen? Is dit spelen zonder specifiek doel?
Exploiteert dit levende lichamen, zijn de deelnemers als artistieke
labaratoriumratten in een witte kubus testbuis? Dat stond ver af van
onze bedoeling. Maar het bijeenbrengen van kunstenaars die nog niet
eerder hadden samengewerkt in een specifieke, artificiële situatie, 
bepaalde veel factoren van de samenwerking. Meerdere keren tijdens deze
laatste 3 jaar deden we een aantal stappen terug om ons te bezinnen op
de condities van het project, ze te herformuleren en opnieuw te proberen
en te re-itereren.
 
De set van initiële voorwaarden en methodologieën van het project
vormden een onderdeel van de discussie-in-actie (of actie-in-discussie)
van het collectieve artistieke proces en dit beïnvloedde de ontwikkeling
van het project zelf. De concrete voortgang van het project plus alle
stemmen die erbij betrokken waren veranderden haar
implementatie.
 
Dit veroorzaakte ook frictie. Is iedere samenwerking ooit überhaupt vrij
van frictie? Zijn conflicten een onvermijdelijk (bij)effect in een
artificiële setting zoals deze? Zou er collectieve ontwikkeling bestaan
zonder deze uitdagingen? Het lijkt moeilijk om de terugkerende
groepsdynamiek te vermijden zeker in zo'n korte, experimentele periode
(normaal gesproken was die ongeveer twee weken). Ondanks alles was één
aspect altijd duidelijk vanaf het begin; bij Iteraties is het doel van
het werk niet zozeer het bereiken van een artistiek eindresultaat, maar
eerder het onderzoeken van de condities van het proces: het werk, de
uitwisselingen, de ervaring van het samenzijn.

<framebreak>

## De toekomst van
 
Het opzetten van een op regels gebaseerde Iteratie creëert nieuwe
toekomsten. En deze zijn meervoudig.
Ze hebben repercussies op het hier en nu: kwantum-presenties en
diffracties.
Is dat hoe het project zich voorstelde wat kunstenaars zouden kunnen
doen? Het veranderen van perspectieven en het brengen van nieuwe
oriëntaties voor vertrouwde concepten? 
 
Misschien weten we niet altijd hoe we samen één richting uit moeten gaan
maar we moeten op zijn minst leren onderhandelen met de materialen die
ertoe doen. De toekomst is collectief of het is niet erg waarschijnlijk
dat er een toekomst is. De toekomst niet als een veelbelovend en
uitgebalanceerd utopisch beeld waar wij voor kunnen zorgen, maar een
meervoudige voortgang in het hier en nu. Er is geen wacht-<br />periode voor
deze toekomst, geen open ruimte beschikbaar voor hoop of wanhoop. In
plaats daarvan rolt een tastbare toekomst uit onze handen en buiten onze
controle.
 
## Artistieke <br />Samenwerking
 
Geïnspireerd door recursieve vormen van samenwerking zoals die bestaan
in het ontwikkelen van open source software (F/LOSS) past het project
Iteraties repetitie en circulariteit toe op artistieke methodologieën:
processen waarbij de output van een activiteit kan worden gebruikt als
inspiratie en input voor de volgende maar die ook terzijde kan worden
geschoven.
 
Wij waren en zijn geïnspireerd door F/LOSS praktijken waarbij <br />iemand kan
bijdragen aan projecten die hij/zij belangrijk vindt, ongeacht hun
beginpunt of einddoel. We zien dat veel artistieke ontwikkelingen van
concepten, gereedschappen, <br />methodes en benaderingen in 
samenwerkingsverband en collectief gebeuren. Maar tegelijkertijd zien we ook dat in
de productie en representatie van hedendaagse kunst die samenwerkingen
compleet worden onderbelicht.
 
De serie experimenteerde met collectieve creaties, met gedeeld
<br />auteurschap en collectieve condities. Iedere Iteratie was een open
uitnodiging voor re-annexatie. Materialen, codes, beelden, instructies
en documentatie worden gedeeld onder open content licenties via de
website van het project.
 
In verband daarmee waren de “handover meetings” ook essentieel voor het
collectieve kunstproces. Het waren gelegenheden waar kunstwerken,
denkprocessen, ruw materiaal en concepten werden doorgegeven van de ene
groep aan de andere. Deze momenten deelden een voortgaand proces: ze
waren een overbruggingsgebaar, om iets van de doorleefde ervaring van de
iteratie door te geven. De “handover” was bij iedere casus een
metamorfose en een handeling in wording. De kunstenaars wisselden niet
enkel materialen uit, maar ook ideeën, ervaringen en data.
 
Op enkele sleutelmomenten besloot de groep die betrokken was bij
Iteraties de term *samenwerking* uit te dagen en dieper in te gaan op het
concept van het *collectief*. De verschuiving kan als volgt worden
omgeschreven: in een collaboratieve omgeving werken, handelen en doen
verschillende mensen samen. Op <br />basis van een collectieve omgeving
scheppen ze een auteurschap, waarin de verschillende bijdragen niet
meer duidelijk te onderscheiden zijn. Dat betekent dat op een gegeven
moment tijdens de ontwikkeling van het project de beslissing voor het
woord samenwerking werd uitgedaagd ten opzichte van collectieve actie.
De noodzaak voor de creatie van uniforme criteria is een harde eis.
Misschien was de creatie van gemeenschappelijke regels of media meer
effectief en minder pijnlijk geweest. Toch blijft de vraag bestaan of
deze worstelingen dan wel überhaupt waren ontstaan bij iedere
implementatie van het project en dit is de kern waar Iteraties voor
staat: hoe kunnen we (het mogelijk maken om) samen (te) werken.
 
Tijdens de verschillende fases zagen we een toename van de voorvoegsels
co-, col- en con. (met, samen, gezamenlijk). In de loop van het project
werden conflicten als een constructief element beschouwd. Aan
moeilijkheden vasthouden als een manier om door te zetten, om ruimte te
maken voor gecompliceerde relaties die nodig zijn voor praktijken die
verschil willen omarmen. Ja, ergens toe behoren is een lastige
randvoorwaarde om een doelstelling te bereiken, maar het geeft ook ruimte voor allianties. Om uiteindelijk samen te staan en deel te zijn van, dat kan ook een collectief statement zijn, een actie van lichamen die een verband aangaan. Lichamelijkheden die hun eigen individualiteit behouden.
 
Iteraties creëerde situaties waar kunstenaars verschillen konden
tegenkomen, ze konden bespreken en transformeren tot werk. In veel
gevallen ontmoetten de kunstenaars, organisatoren en andere partijen
elkaar voor de eerste keer en werkten zo ook voor het eerst samen.
Alhoewel niet altijd makkelijk, is het belangrijk toe te laten dat in
zulke situaties spanningen en fricties kunnen ontstaan. Dat deze niet
genegeerd worden maar serieus worden genomen, zodat ze een productief
centraal punt kunnen worden in de gemeenschappelijke werkwijze.
 
## Digitaal
 
In de laatste decennia heeft “digitaal” een andere betekenis gekregen.
Het vroege internet bracht ons netwerk-kunst, kritische cyborg
identiteit, gender-tech experimenten en ruimte voor onafhankelijke
infrastructuren. Dit hoopvol potentieel wordt in onze tijd overschaduwd
door techno-koloniaal oplossings-<br />fetishisme, door energieslurpende,
<br />totaal invasieve big-data correlaties, commodificatie en commercie.
“Digitaal” kan daarom niet onaangetekend blijven, maar vraagt om
kritische benadering. Tijdens Iteraties gebeurde het dat sommige
deelnemers liever hun laptops dicht hielden om beter te kunnen delen in
de gezamenlijke tijd, het eten, de temperaturen, de geluiden en de
stiltes, de energie en de vermoeidheden. <br />Op dergelijke momenten was
*digitaal* wellicht niet meer dan een sterke metafoor.

## Context
 
Wat is context? Welke (a)sym-<br />metrieën zijn er bij de positie van het
onderwerp betrokken? Ons werk in kunst en cultuur is verbonden met vele
sectoren in de samenleving, met technologische innovatie-<br />schema's, met
eco(-logische of -nomische) politiek, het verbindt zich aan toekomsten
die wij ons misschien zelfs niet kunnen voorstellen. We denken op een
gelijkaardige manier over context als over toekomst. Het is nu en hier,
zeer dichtbij maar het is ook de diffractieve en meervoudige openheid
van de onmiddellijke dingen. We werken in zoveel verschillende
omgevingen: de stad, het platteland, afgelegen gebieden, het centrum,
privé of publiek.
 
Ecologie, de tijd van post-waarheden, musea en tentoonstellingen versus
performatieve acties: hoe beïnvloeden de verschillende contexten de
Iteraties, hun dynamiek en hun resultaten? Ecologie als hoofdonderwerp,
de post-human als vraag, als methode. En daarbij de noodzaak om
verwanten te maken, om te begrijpen, om te voltooien en te besmetten.
Collectieven worden ook gevormd door intrusies en wijzigingen,
transposities of ongelukken. De context is tegelijk concreet en
ontwricht. Wie is hier? Wie is dichtbij ons? Wie doet dit met ons?
Wanneer gebeurt dit? En waar? De ontwrichting, het intuïtief
samenbrengen van gescheiden plaatsen, dingen en mensen als methode om
een context te creëren.
 
## Deze publicatie

Deze publicatie heeft tot doel openingen te creëren en nieuwe concepten
voor nog te ontwikkelen werk te inspireren. Als een vat dat gevuld is
met een hybride inhoud die eruit kan worden gelaten om opnieuw gemengd
te worden. In dit boek komen bijdragen van participanten uit het
verleden en Iterators uit de toekomst samen om ideeën te transporteren
naar ons nog onbekende personen en contexten. Als zodanig kan deze
publicatie op zichzelf ook worden gezien als een nieuwe
Iteratie.
 
De bijdragen hebben betrekking op manieren van aanpak, houdingen en
standpunten die uit het Iteraties-project naar voren zijn gekomen. <br />De
redactrices creëerden een set beschrijvingen van gemeenschappelijke
thema's die “handvatten” werden genoemd, en zij nodigden elke bijdrager
uit om te reageren op één van deze thema's. Tijdens de redactie sprint
waar het materiaal werd verwerkt, werden *scores* geproduceerd die een
transversale lezing van de bijdragen mogelijk maakten. Deze manier van
werken werd X-dexing genoemd (redactrices Jara Rocha en Manetta Berends
lichten deze methodologieën verder toe in het boekje binnenin deze
publicatie).
 
Taal verbindt en verdeelt tege-<br />lijkertijd, het omvat en het sluit uit. De
talen waarin of waaruit de teksten die je hier vindt zijn vertaald zijn
eigen aan de partnerorganisaties. Engels hoort daar niet bij, maar het
is hier wel aanwezig omdat het de belangrijkste communicatietaal was
tijdens het project. Terwijl we het spreken en schrijven, transformeren
we Engels tot een polyfoon, verbasterd apparaat. Omdat we met meer dan
30 nationaliteiten aan dit project werkten, hebben we het ingekleurd met
de sporen van onze verschillende moedertalen, resten achterlatend die
ook in deze publicatie tastbaar zijn.
 
Sommige bijdragen zijn gebaseerd op documenten die voort-<br />vloeien uit de
activiteiten van Iteraties. De groep *common ground* ontwierp een muzikale 
score of partituur die gebaseerd is op akoestische elementen uit de plattegrond van de
tentoonstelling “Collaboration Contamination”. Deze kunnen worden
gebruikt als score of partituur voor toekomstige (geluids)werken.
 
De bron voor de bijdrage <br />“Dear Visitor,” van *Behuki* is de <br />residentie en
de tentoonstelling <br />“Operating / Exploitation”.
 
De inleiding “Three Documents” door *Collective Conditions* situeert
meerdere groepsschrijfpraktijken die in samenhang met de Constant
werksessie “Collective Conditions” zijn ontstaan.
 
*Rica Rickson* reflecteert veelkleurig op haar nieuwe geboorte tijdens de
Hangar residentie. In een <br />innerlijke dialoog overdenkt Rica haar
collectieve lichaam, haar verstrengelingen met de kunstwereld, haar
verlangens, behoeftes en geneologieën.
 
Deze bijdragen worden vergezeld door twee toegevoegde artikelen:
“holding spell” van *Kym Ward* merkt op dat ontmoetingen tussen mensen
momenten van convergentie en betovering kunnen zijn. In “Por debajo y
por los lados. Sostener infraestructuras feministas con ficción
especculativa” (“Van beneden en van de zijkanten. Ondersteuning van
feministische infrastructuren met speculatieve fictie”), brengt
*spideralex* technieken en theorieën samen over de wereldmakende en
toekomstdenkende gemeenschapsinfrastructuren.
 
Deze publicatie is niet bedacht als een catalogus. Gedeeld werk heeft
het recht om verspreid te worden, een eigen leven te leiden,
blootgesteld te worden aan briljante invloeden, te profiteren van de
toevoegingen van anderen en te transformeren. Dus als je geïnteresseerd
bent in het verder verkennen van de beelden, teksten, kunst,
geluidsopnames, broncodes en andere materialen die afkomstig zijn van
het Iteraties-project, nodigen wij je van harte uit om de website van
het project te verkennen: <https://iterations.space> en de inhoud ervan te
gebruiken, <br />te kopiëren, te wijzigen en te her-<br />distribueren. De site
bevat ook enkele extra vertalingen van bijdragen aan deze publicatie.

Een groot dankwoord gaat uit naar alle kunstenaars, deelnemers,
<br />financiers, organisaties en personen die hebben bijgedragen aan het
<br />Iteraties project en aan deze <br />publicatie.

 
<!-- __NOPUBLISH__ -->

# Colophon

COLOPHON

*Equip editorial / Das Redaktionsteam / Editorial team / Equipo editorial \
/ Équipe éditoriale / Redactieteam*: \
Jara Rocha, Manetta Berends, \
Lluís Nacenta, Nayarí Castillo, \
Peter Westenberg, Femke Snelting, \
Reni Hofmüller, Ludovica Michelin, \
Donatella Portoghese

*Desenvolupament i disseny d'eines edi- \
torials / Entwicklung und Design von Redaktionswerkzeugen / Editorial tool development and design / Desarrollo y diseño de herramientas editoriales \
/ Développement d'outils éditoriaux et graphisme / Ontwikkeling redactioneel gereedschap en ontwerp*: \
Jara Rocha, Manetta Berends

*Traduccions / Übersetzungen \
/ Translations / Traducciones \
/ Traductions / Vertalingen*: \
Cristina Ridruejo (ES), Rita Soler (CAT), Camille Rieunier (FR), \
Monique Fuller (EN), Berber Ormeling (NL), Anette Hilgendag (DE) <http://www.aeiou-traductores.com/>

*Arxius origen / Quelldateien \
/ Source-files / Archivos fuente \
/ Code source / Bronbestanden*: \
<https://gitlab.constantvzw.org/manetta/iterations-publication>

*Descàrrega en pdf / Herunterladen des Pdf / Download pdf / Descarga en pdf / Téléchargez le pdf / Download pdf*: <https://iterations.space/publication>

*Eines / Werkzeuge / Tools / Herra- \
mientas / Outils / Gereedschappen*: Reportlab, Etherpad, Libre Writer

*Font tipogràfica / Schriftarten \
/ Fonts / Tipografía / Polices \
/ Lettertypes*: CMU Concrete \
(*<http://canopus.iacp.dvo.ru/%7Epanov/cm-unicode/index.html>*), Brazil (*<https://colophon.info/Images/bRAZIL.zip>*) and GNU Unifont (*<http://unifoundry.com/unifont/index.html>*)

<!-- *Printed at / druk / imprimée par / Druck / Impreso en / imprès a*: xxxx -->

<framebreak>

*Publicat per / Herausgegeben von / Published by / Publicado por / Publiée par / Een publicatie van*: Constant, Vereniging voor Kunst en Media, Brussels (2020)

*Web del projecte / Projektwebseite \
/ Project's website / Web del proyecto \
/ Site web du projet / Project website*: <https://iterations.space>

*Llicència / Lizenz / License \
/ Licencia / Licence / Licentie*: \
Texts and images are available under a Free Art License 1.3 (C) Copyleft Attitude, 2007. You may copy, distribute and modify them according to the terms of the Free Art License: *<http://artlibre.org>*; Teksten en afbeeldingen zijn beschikbaar onder een Free Art License 1.3 (C) Copyleft Attitude, 2007. U kunt ze dus kopiëren, verspreiden en wijzigen volgens de voorwaarden van de Free Art License: *<http://artlibre.org>*; Les textes et les images sont disponibles sous licence Art Libre 1.3 (C) Copyleft Attitude 2007. Vous pouvez les copier, distribuer et modifier selon les termes de la Licence Art Libre: *<http://artlibre.org>*; Texte und Bilder sind unter einer Free Art License 1.3 (C) Copyleft Attitude, 2007, erhältlich. Sie dürfen gemäß den Bedingungen der Free Art License kopiert, verbreitet und verändert werden: *<http://artlibre.org>*; Los textos e imágenes están disponibles bajo una Licencia de Arte Libre 1.3 (C) Copyleft Attitude, 2007. Puede copiarlos, distribuirlos y modificarlos según los términos de la Licencia de Arte Libre: *<http://artlibre.org>* ; Els textos i imatges estan disponibles sota una Llicència d'Art Lliure 1.3 (C) Copyleft Attitude, 2007. És permès copiar-los, distribuir-los i modificar-los segons els termes de la Llicència d'Art Lliure: *<http://artlibre.org>*

<!--*ISBN*: xxxxxxxxxxxx -->

*NUR (Nederlandstalige Uniforme \
Rubrieksindeling) code*: 647 - Beeldende kunsttechnieken

<framebreak>

*Gràcies / Danke / Thank you \
/ Gracias / Merci / Bedankt*: \
Cristina Cochior, Anne Laforet, \
Ona Bros, Nayarí Castillo, Lucía Egaña, Lídia Pereira, Gijs de Heij

*Iteracions va ser iniciat per / Iterationen wurde initiiert von / Iterations was initiated by / Iteraciones fue iniciado por / Itérations est initié par / Iteraties werd geïnitieerd door*: Hangar (Barcelona), esc medien kunst labor (Graz), Dyne (Amsterdam) \
+ Constant (Bruxelles/Brussel)

![](images/logo/logos-partners.png)

*Aquesta publicació està cofinançada pel Programa “Europa Creativa” de la Unió Europea i recolzada per / Diese Publikation wird vom  „Creative Europe" Programm der Europäischen Union kofinanziert und unterstützt von / This publication is co-funded by the "Creative Europe" Programme of the European Union and supported by / Esta publicación está cofinanciada por el Programa “Europa Creativa” de la Unión Europea y apoyada por \
/ Cette publication est cofinancée par le programme « Europe Créative » de l'Union Européenne et soutenue par \
/ Deze publicatie wordt medegefinancierd door het programma “Creative Europe” van de Europese Unie en ondersteund door*:

![](images/logo/logos.png)
