% Becoming Rica Rickson
% Rica Rickson

# Becoming <br />Rica Rickson

[I am here.]{.green}[\ I am also here.]{.red}[\ or there, or somewhere
else. it depends where I find myself at the moment.]{.green}[\ I am a
little bit nervous]{.red}[(tense?)]{.green}[.Shy?]{.red}[\ Melancholic?]{.green}[\ In a
continuo]{.yellow}[u]{.green}[s hurry?]{.yellow}[\ asking myself, when do
we really have time for collective digestion of the events happening?
How can we really understand processes if we cannot stop?]{.pink}

[I was cleaning my cradle this morning and I found this box full of
memories. On the top of the box there was a bingo board full of english
words:]{.green}

[questioning -- magic -- empowerment -- (un)selfishness --
transformation -- authorship -- staying with the problem --
misunderstanding -- improvisation -- singularities -- tension -- taking
care -- agency -- trust -- porosity -- open mindset -- collective --
collaborative -- effort -- joy -- dis/agreement -- respons-ability --
needs -- power-relations -- desires]{.green}[]{.green}

[In the box with the words I also found part of a message\ ]{.yellow}[I
had sent to Brussels:]{.red}[\ ]{.yellow}

[As I faced a lot of conflicts, uncertainties and questions after the
residency at Hangar, I would like to further continue our vivid
discussion about\ ]{.yellow}[collectiveness]{.yellow}[
and to further reflect upon and share our experiences. I therefore
regard the worklab a constructive and interesting environment to
integrate with and a constructive opportunity to investigate more deeply
the potentials and\ ]{.yellow}[needs]{.yellow}[\ of my continuously developing
organism.]{.yellow}

[I was freshly born, when I felt like being thrown back in a mechanism,
where I emerged from.]{.violet}[]{.red}

[The individual parts grew together within an intensive reflection about
]{.red}[authorship]{.red}[,
]{.red}[questioning]{.red}[\ the difference
between\ ]{.red}[collaboration]{.red}[\ and
]{.red}[collectiveness]{.red}[,
]{.red}[questioning]{.red}[\ the market logics of
art production,\ ]{.red}[celebrating female\ ]{.yellow}[magic]{.yellow}[.]{.yellow}[\ In an organic process of
mutual]{.red}[\ act of\ ]{.green}[taking care]{.red}[, listening,]{.red}[\ ]{.blue}[trust]{.blue}[,\ ]{.blue}[(]{.pink}[un]{.blue}[)]{.pink}[selfishness]{.blue}[\ etc. I wanted to create a body that protects
myself from the artworld outside.]{.red}[\ I thought, then it is my
decision, whether I care about\ ]{.violet}[authorship]{.violet}[\ or not,]{.violet}[\ I thought, I am a we, a she,
a him]{.red}[\ (maybe less)]{.orange}[\ and finally an I, but not a
label.]{.red}[\ I thought, I can emancipate myself from
]{.blue}[power-relations]{.blue}[\ given by
institutions,]{.blue}[\ I am the body I live in.\ ]{.red}[(whereas an
institution is an uncorporeal body.)]{.orange}[\ I thought I could escape
from market forces, productivity strategies, neoliberalism.]{.red}[\ I
]{.yellow}[trusted]{.yellow}[, that I am my own
matrix]{.yellow}[\ but I am just a parasite, some uninvited organism that
]{.green}[needs]{.green}[\ its host to exist and
grow although my presence necessarily disturb its normal functioning. I
am grateful to this host which is necessary for my own survival and at
the same time I feel stuck in an environment whose forces go against my
]{.green}[desires]{.green}[\ and structural
dreams.]{.green}[\ I was freshly born, when I realized that I cannot
escape and that it does\ ]{.red}[and will]{.green}[\ affect me. That was a
slightly painful, disappointing conclusion, it destabilized my organism.
]{.red}[Who am I? May I present your work and what is its meaning? What
is your name?]{.orange}[\ How much is it? I would like to invite you, who
will come?]{.yellow}

[I can resist,]{.red}[\ and I have to,]{.blue}[\ if I want to survive the
surrounding fragmenting forces.]{.red}[\ If I don’t, I might be disolved
in the host, phagocited by the bigger organisms, end up being part of
the different cells forming this host.]{.green}

[I have to stay with my often controversial\ ]{.red}[needs]{.red}[, different temporalities,
]{.red}[misunderstanding]{.red}[s.
]{.red}[]{.yellow}

[Being myself in is a constant state of\ ]{.yellow}[tension]{.yellow}[, that costs energy,\ ]{.yellow}[effort]{.yellow}[,\ ]{.yellow}[agency]{.yellow}[\ and commitment.]{.yellow}[\ I am my own
]{.green}[desire]{.green}[\ and my own
parasite]{.green}[. I am Frankenstein and Frankensteins Monster]{.blue}[
Mother.]{.green}

[In the box with the words and the message I had sent to Brussels, I
found part of an email I had sent to Sicily:\ ]{.blue}

[Within the last days I have been reflecting upon different ways of
collaboration, starting from different concepts I was introduced to, for
example misunderstanding (between languages, different cultures) or the
"parasite-concept” asking who hosts whom etc.]{.blue}

[\...]{.blue}

[I would be happy, if you would like to participate, which implicates,
that your name will not appear, as an individual author. Of course,
everything I am working on now, will also be open and collectively
shared.]{.blue}[]{.pink}

[In the morning, I was reading some articles about “identity in art
production"- very often they\ ]{.red}[- but who are they? -]{.green}[
authors :D - referred to “self-care", which means transforming personal
issues into artworks.]{.red}[\ And I thought about “the death of the
author” by Roland Barthes that I had just read again yesterday:
]{.green}

[Here is a quote about it that I found in a very well-know fabric-like
content website:\ ]{.orange}[In a well-known quotation, Barthes draws an
analogy between text and textiles, declaring that a “text is a tissue
\[or fabric\] of quotations,” drawn from “innumerable centers of
culture,” rather than from one, individual experience. The essential
meaning of a work depends on the impressions of the reader, rather than
the “passions” or “tastes” of the writer; “a text’s unity lies not in
its origins,” or its creator, “but in its destination,” or its
audience.]{.orange}[]{.green}

[How to\ ]{.pink}[connect,]{.green}[\ reconnect with myself?]{.pink}[
Myself are you hearing me?]{.yellow}

[Every collaborative / collective project is a switch that turns
on.]{.pink}[\ My daily life is made of many switches, who am I when they
are all off, but above all who am I when, for some reason, they are all
on?]{.yellow}

[Economic survival and also a certain character of mine, lead me to work
on different fronts, to create, a verb that problematically collides
with “produce” many things / projects / situations / groups.]{.pink}[
What was the question again? cultural creation or cultural
production?]{.green}

[But where am I when I’m here and I have to be there tomorrow? Who am I
with you today if I have to be with them tomorrow?]{.violet}[]{.pink}

[Okay.]{.yellow}

[Now the switches are all on.]{.blue}[]{.pink}

[I’m getting anxiety. Maybe I should just do one project? Make my
curriculum vitae coherent, functional, be one thing, a monad.]{.pink}[\ A
nomad?]{.green}[\ Is this an aspiration of my deep self or the market
demand? How and when did I embody it? Who am I when I reconnect with
myself and all the different parts I’m connected to?\ ]{.pink}[How would
my body react if one of these parts remained passive or atrophied, or
simply got carry by others? Or on the contrary, if it became more
active, or even the most active\... like the motor of the whole
body?]{.blue}[\ Am I still connected to myself if I am not connected with
anyone else?]{.green}

[Do I need a body? But what if I don’t need the calm of the
Unity.]{.blue}[\ I want to live WITH and to work IN the contradiction.
How to develop contradictions as a new form of knowledge? How to embrace
them as a way to learn that the contrary is in me?]{.yellow}[\ How to
inhabit the contradictions in myself? The space in between is always an
interesting textile made of different colors and knots to create a whole
full of memories.]{.pink}[\ The space in between has an enormous creative
power.]{.orange}[\ I have the intuition that the idea of Unity it’s what
made Western culture so wrong. The subjective intensity in this “in
between” erase the ego, the authority, and I recognize my own ignorance.
]{.violet}[There is a creative explosion IN my contradictions.
]{.green}[How to take
de]{.yellow}[c]{.green}[i]{.yellow}[s]{.green}[ions in the
contradiction? Can they adapt on the in-between]{.yellow}[? Are
decisions univocal?]{.blue}[\ Maybe the question is not how to overcome
contradictions but how to live with them, how to inhabit them to avoid
schizofrenia. To transform schizofrenia into a process of
knowledge.]{.red}[\ Can I transform my stigma into my emblem?
]{.blue}[Can I transform my monstrosity into my beauty?]{.violet}[\ How
to be an alternate body?]{.orange}

[Author/authority\... Power Relations]{.green}

[\ Where is the reader, who is\ ]{.orange}[taking care]{.orange}[\ of what and whom?]{.orange}

[Why am I an artist?]{.red}[\ Am I an artist?]{.green}[\ How do I solve my
problems? Are they problems, traumas or obsessions? Are my obsessions
contradictory within myself. Are my obsessions individual or
collective?]{.pink}[\ I am a she\...]{.red}[\ Am]{.yellow}

[I? Is my identity my choice?]{.yellow}[\ Is it what emerges through the
interconnection of my different parts?]{.violet}[\ Is it the choice of
the reader? I chose it some time ago but I have the freedom to go
against my choice.]{.green}[\ Will I forever adolescence?]{.red}[\ Freedom
can be troublematic\...]{.green}[\ But making choices means
]{.red}[questioning]{.red}[, and a question is
always a good start for creation.]{.red}[\ What was the question
again?]{.blue}[\ Is the question still important when having a question
mark going on?]{.yellow}

[There is something about starting that is always making me nervous,
]{.green}[tense]{.green}[, shy.]{.green}[
(Respons-ability)]{.green}[]{.green}

[There is no need to start from the beginning.]{.blue}[\ I understand the
world around me, using the dimensions of my body are my reference. The
world around me, starts from my body.]{.red}[\ My body is troubled, it is
human, but multiple.]{.green}[\ And I love,\ ]{.pink}[fight for]{.green}[
and will always defend my\ ]{.pink}[porosity]{.pink}[. My body is in the world, connected to the
world but not necessarily placed in one specific space at once\... I am
now in my bed]{.green}[, I am on my sofa]{.violet}[, I am
outside.]{.blue}[\ ]{.yellow}[There is a small breakfast table holding
the computer on which I am writing, so it let my belly breath.]{.green}[
There is a big window in front of me, showing the grey in grey of the
sky.]{.red}[\ Where else am I present?]{.green}[\ The sky is grey here, it
is winter and cold and I wished to be back on a sunny November day in
Barcelona, having beer - no, what was is? on the streets- on the balcony
of the casita, having coffee, walking around, jumping in the
sea.]{.orange}[\ Having fun,]{.yellow}[\ with\ ]{.pink}[joy]{.pink}[\ ]{.pink}[and\ ]{.blue}[dis/agreement]{.blue}[]{.blue}

[Do I need a body?]{.pink}[\ One body that fills a house, and fills all
its entities with passion.]{.blue}[\ I walk by the house almost every
week. I see the balcony and remember the view from there, but I am not
allowed to enter the]{.green}

[place anymore. The house is now just a shell for other bodies to enter,
inhabit, cohabit, make the place alive.]{.green}[\ Do I need a house?
]{.orange}[I enter another body of this kind some month ago in Brussels,
there was an elephant bone in the middle of the room There was a garden
too.]{.green}[\ What does it mean, What does it mean, if I can not go
back to where I started?\ ]{.blue}[Start is not necessarily the
beginning.]{.red}[\ Does this make me fragmented?]{.pink}[\ Is this
fragmentation subject of my art?]{.blue}[\ Can I cure this fragmentation
through my art?]{.yellow}[\ Can I recreate]{.violet}[/reeneact]{.pink}[
the start? Do I need hypnosis to do a regression?]{.violet}[\ Do I need
hypnosis to do a regression?]{.yellow}

[I am a constellation of affinities. A community of sense. A body
politics.]{.yellow}[\ I think this is a privilege.]{.blue}[\ I feel now
more solid!]{.red}

[Can I add visual memories here?
https://cloud.hangar.org/apps/files/?dir=/Documentation/Photos/05.11.18-Action1&fileid=12866]{.red}[
I just erased something that I found irrelevant for the reader, is it
self censorship?]{.violet}[\ Censorship? It destabilizes my memory, I
cannot remember what was there.]{.red}[\ What had opened before the
sky?]{.yellow}[\ The recording platform.]{.violet}[\ I don’t remember
talking about a recording platform today.]{.red}[\ Anyways, The sky also
just opened. I’ll go out quickly to look at the sky without glass
inbetween There is a discoball in front of the window that fills my room
with light dots.]{.pink}[\ Once I was at an exhibition, where the artist
put round colored stickers all over the room (on everythig), so it
lookied like there was a discolight.The title was “I am here, but
nothing”.]{.orange}[\ Yesterday I was watching the instagram stories two
friends. They both had a picture of light reflections of there windows
on their wall. There was a light comunication between their places, it
often happens when there is a rainbow that is observed by several people
at the same time but from different places.\ ]{.green}[My body has the
ability to see the world from different places, angles and timezones. I
can do that with all my senses, actually. and I can even appear and act
in different places, without being virtual or a spirit.]{.yellow}[\ My
flatmate just brought me a crosswordpuzzle.]{.blue}[\ The nursery sent me
a message, they need diapers.]{.green}[\ I remember the first time I
misunderstood myself. In front of a bar. Sometimes I understand a
reference better, than a word.]{.blue}[\ My language is
allegorical.]{.orange}[\ My language is circunstancial.]{.green}[
Allegory is self-conscious poiesis that respect the conditions of
particularities without reduce the power of the common. Allegorical
language is aesthetically open.\ ]{.pink}[Even my silence is allegorical,
]{.blue}[circunstancial.\ ]{.green}[My children also learned to speak it.
We learned it together. They taught me. Once, they create a very
beautiful one at Hangar. It was an allegory of gestures and objects, a
common ground, a garden. We create this center that shaped
everything.]{.orange}

[///\ ]{.yellow}[improvisation]{.yellow}[
///]{.yellow}[\ What was the question again?]{.red}[\ how to make of
trouble a generative force?]{.violet}[\ Wait I’ll be back.\ ]{.green}

[// this option would imply to both provisionally define/understand what
trouble is (or not) in cultural creations, and to list and unfold its
generative forces/potential.]{.yellow}[]{.violet}

[Troubles in cultural creations:]{.violet}[\ disturbing the
comfort]{.red}[,]{.green}[authorship]{.green}[/authority]{.green}[, censorship,
]{.blue}[money,\ ]{.red}[set of values,]{.pink}[
]{.red}[misunderstanding]{.red}[,freedom?]{.orange}[]{.red}

[What is a cultural creation?]{.orange}[\ I tried to avoid this
question\... Maybe I/you can find references.\ ]{.violet}[I feel lonely,
uncomplete today.]{.yellow}[\ Melancholy is a well known motif in
cultural creation.]{.blue}[]{.yellow}

[Maybe after that it was a long time I wasn’t seeing myself. Sometimes,
you feel your entire body as being so many different parts, not even
knowing which kind of fluctuation causes them to stay together and
merge.]{.yellow}

[Sometimes I have no sleep, but my eyes are closing.]{.blue}[\ Sometimes
my legs want to run, but I am still siting here.\ ]{.orange}[In the end,
the symbiosis that are established between all the parts shows that a
type of connection]{.pink}

[makes any of of them be here, touching us in some
way.]{.pink}[]{.yellow}

[I need to change spot, my butt is hurting!]{.green}

[Hello there reader, back again in a new chair, a new location, a bit
cold due to the weather conditions of this place.]{.violet}[\ Rica means
rich, a rich woman in Spanish.\ ]{.blue}[Rica also means sexy, hot,
tasty, ggood for latinos. My language is circunstacial.]{.green}[\ When I
was born some of the members of this collective organism were
]{.yellow}[questioning]{.yellow}[\ its own
precarious situations and while half joking, we thought that being
called Rica was a]{.yellow}

[good option to get out this precarity, a total contradiction as being
all artists and working in art institutions]{.yellow}[, we know how
individual names are the prefered option for most of the places we work
and try to collaborate with.]{.red}

[I like]{.blue}[\ to cook with others, do some gardening, write texts
together]{.violet}[\ and get a bit confused with the limits of
]{.orange}[my]{.blue}[\ own self]{.orange}[, dance, make sounds
]{.red}[and try to travel with all its members to the different cities
they get invited to contribute with their works.]{.blue}[\ I
]{.green}[need]{.green}[\ money and time to get
mysef together.\ ]{.green}[Am I really\ ]{.orange}[always]{.pink}[\ ok to
]{.orange}[stay with the trouble?]{.orange}[]{.green}

[//Endnote: This text was created by Rica Rickson in a process of
creative writing, It is a reflection that shall reveal the generative
forces of troubles. //]{.blue}[]{.green}

[Ps. I am also fine with the sharing of the Cocktail]{.orange}
