% Three documents
% Collective Conditions

# Three documents

This contribution consists of three documents which emerged from the
worksession *Collective Conditions*, an experiment with the generative
potential of socio-technical protocols such as codes of conduct,
complaint procedures, bug reports and copyleft licenses. Constant’s
worksessions are temporary research labs, intensive collective
environments where different types of expertise can come into contact
with each other. To these otherwise-disciplined situations, artists,
software developers, theorists, activists and others are invited to
contribute.<sup>1</sup>

*Collective Conditions* was activated by the work of trans\*feminist
collectives on ally-ship, non-violent communication, score-making,
anti-colonial and intersectional activism, but also by ways of doing
developed within Free Culture and Free, Libre and Open Source software.
Out of commitment to the socio-technical protocols that these
collectives propose to intervene in cultures of harassment, we wanted 
to take serious the role that self-invented protocols might play in the 
(different) imagination of complex collectivities.

The need to formulate protocols is felt urgently within on-line
collectives, that operate on the intersection of social practice and
technological infrastructures and run up against the limits of
“openness” and “freedom”. Protocols have a tendency to focus energy on
discursive processes, rather than building concrete skills and practices
within groups and across participants. This worksession therefore
introduced different modes of “writing” in order to challenge, make stumble and
collide; this posed questions and problems. We experimented with
feminist tango, self-determined objects, translanguaging, *Tango
Thermique*, fragile community scores, micro-lectures and an acapella
proto-anarcho post-punk operetta featuring *Queering Damage* songs.

Participants shared various degrees of concern with the possibility that
we could be re-instating a “new normal’. The figure of “complex
collectivity” helped articulating these concerns in a way that we could
imagine ally-ship as a non-equalizing form of togetherness. What
protocols would for example work for non-normative human constellations,
or collectives where participants with radically different needs,
backgrounds and agencies come together? What different needs do “complex
collectives” have when they are the result of structural forces such as
laws, racism, technology, wars, austerity, queerphobia and ecological
conditions?

We also wondered about how our self-invented protocols might too easily
repeat law-like ways of doing. We felt we needed to pay attention to how
they are implemented, to not eventually reiterate the carceral logic of
victimization and punishment which we felt perpetuates and multiplies
harm. We made a start with working through non-divisive ways of dealing
with codes of conduct, complaints procedures, bug reports and copyleft
licenses, trying to go beyond ostracization and exclusion.

The three documents that follow respond each in their own way to the
complex challenge of *Collective Conditions*. 

he first document is the
***Manifesto of Cares*** which emerged as a response to a proposition by
Elodie Mugrefya and Olave Nduwanje, *Writing manifesto of rest/care*.
The text was partially written during the worksession, and finished in
the months after. This first version of the manifesto aims to reclaim
care as a critical practice, and “reaches out to people who haven\’t
thought about the social economical racial gendered implications of
cares, and is also for the people who wish to reinvigorate their cares
patterns”. 

The second document is a splash page for ***Bibliostrike***,
a situated version of the *Bibliotecha* local digital library set-up.
During the worksession, a version of Bibliothecha was installed together
with etherbox<sup>2</sup>. The shared infrastructure was adjusted,
re-articulated and weaponized to join the picketline of the University
and college strikes in the UK. 

The third and last documents are related
to the ongoing discussions around the ***Constant Collaboration
Guidelines***<sup>3</sup>. The guidelines were tested for the first time during *Collective Conditions* and discussed before and after with several
groups. Constant committed to keep adjusting these guidelines; the
included document traces the different problems and questions that have
come up.

<!-- -------------- -->

## Manifesto of cares

**Commitment to Cares,** \
**Why to commit to care?** \
We are a group of intersectional trans\*feminists coming together in the
context of *Collective Conditions*, a worksession organised by Constant,
in Brussels, in the winter of 2019. We gathered around the notion of cares
to further understand our relationship to cares in our shifting
contexts. We are concerned with the urgency of this moment because we
have noticed that many in positions of power are doing little to enact
cares. As for us, we are in positions where we can enact contextualized
cares and we want to revive our commitment towards cares as critical
practices. We need to talk about cares, not because they don’t exist,
but because cares are not distributed equally, and because to “take
care” can have multiple meanings depending on positions of privilege,
context and who and what we’re dealing with. This manifesto is a demand
towards those subjects within institutional frameworks to reflect on
their relationship towards the giving, taking, receiving cares, to
question well worn patterns of cares, and how they can be re-inscribed.
This manifesto reaches out to people who haven\’t thought about the
social economical racial gendered implications of cares, and is also for
the people who wish to reinvigorate their cares”.

**Caring requires** 

-   active concern, mindfulness and commitment towards all living and
    non living entities.

-   attention to entangled environments: physical, digital,
    interpersonal, ecological, and those we share them with.

-   attention towards individuals and communities.

-   not presuming and asking for different needs.

-   time and the understanding of time\’s multidimensional nature.

-   actions, not expected services.

-   deep listening.<sup>4</sup>

-   patience.

-   courage and responsibility.

-   multiple interpretations and possibilities.

-   \... not knowing what caring might mean.

-   to be taken seriously and to take seriously.

-   Acknowledgment.

**The state of cares** \
Caring is:

-   Caring is deeply entwined in power relations and not equally
    distributed.

-   Caring is currently hegemonic: some have the agency to impose a
    constricting meaning and practice of cares, on behalf of others.

-   Caring is subsumed within capitalist frameworks that create the
    conditions for care workers to have less agency and not be
    represented in what they are employed to do.

-   Caring is in a false dichotomy of productive/reproductive labour.

-   Caring is chronically undervalued.

-   Caring is deeply entwined in institutional engagement or lack
    thereof.

-   Our society produces vacuums of cares in places where they are
    needed.

-   Caring is underdeveloped in institutional contexts.

-   Caring is something that cannot be outsourced.

Caring is not a tool.\
Cares cannot be instrumentalised.\
Cares cannot be generalized.\
Cares cannot be constricted by normativity.\
Caring cannot be conditional.\
Cares are antimanifesto.

**Outlook of cares** \
If things stay this way ...

-   the tyranny of self-help models of survival will undermine the
    formation of solidarity and collective power.

-   we\’ll be suspended in precarious modes of surviving which
    capitalizes on our failures.

-   caring will continue to be performed by those who are exploited by
    contemporary slavery.

-   slavery will continue to be reproduced as the insidious institution
    that it has always been.

-   the continued burning out of those who do the caring will exhaust
    the possibilities of cares that still currently exist.

-   there will be a proliferation of ego-centrist hedonists and hoarders
    of goods, wealth, status and power.

-   we risk to all become facsimiles of whiteness: greedy, extractive
    and exploitative.

-   the logics of extractivism will continue to intensify, spurring
    man-made ecological disasters disproportionately impacting certain
    geographies, geosystems and ecosystems.

-   persons, groups, ecosystems, identities will continue to be
    subjected to the violence of categorisation, victimization,
    commodification and marginalisation.

-   the end of humanity and the destruction of the ecosystems is
    inevitable.

**Envisioning Cares** \
Challenging the current trajectory of cares, we now demand a future in
which ...

-   all living and non living entities are acknowledged as valuable and
    worthy of care.

-   the emotional, physical, philosophical and social realities of all
    living beings have abundant expressions.

-   living matters will have space to discover their own agencies.

-   trees, plants, lands, oceans, river, water, rocks, mountains are
    acknowledged in reciprocal caring relationships

-   there is commitment to the work of making ecologically conscious
    decisions

-   nation states, borders and advertisement will be abolished in the
    service of equal opportunity for all living and non living entities.

-   value is shaped in do-it-together (DIT) collective bottom-up modes.

-   institutional, governmental, corporate agents of power will be
    transformed into self-determined agencies.

-   desire, love, sex, relationships and cares are liberated with modes
    of enthusiastic consent and freedom to abstain.

-   consent is not presumed and crucial to all relationships

-   touch implies addressing consent

-   cares are mutual, shared across groups, individuals, beings and
    entities according to varying needs, skills, capacities and
    possibilities.

-   caring roles exist outside normalised power dynamics

-   notions of cares continue to be discussed, to change and to be adapted.

**Crafts of Cares** \
Possible practices:

-   Cultivate curiosity; to care is to be curious, curious enough to
    ask.

-   Be humble, humble enough to abandon assumptions.

-   Cultivate gentleness, gentle enough to be asked

-   Move towards disassembled and loosely structured, interchangeable
    meshwork of enacted communities.

-   Acknowledge and deconstruct privileges.

-   Be open about failures.

-   Cultivating practices of abundance rather than hoarding.

-   Value those who do the caring.

-   Pay salaries to those who do the caring.

-   Value / pay for restorative care to heal deficits of cares.

-   Value/pay those who do domestic work.

-   Value/pay those who do the child rearing.

-   Value/pay those who do the teaching.

-   Value/pay those who are the caretakers of spaces.

-   Illegalise shareholder models and corporations.

-   Denormalise existing structures and power relationships.

-   Root power directly within communities to counter historic hoardings
    of power.

-   Multiply agencies for new interpretations of cares.

-   Commit to activist, sensitive lives; nurturing and facilitating each
    other\’s talents and dreams.

--------------

## B I B L I O S T R I K E

Welcome to the picket line and *bibliostrike*!

(╯°□°)╯︵ ┻━┻

A *bibliostrike* is a place to make acts of solidarity, dreaming,
counter-optimisation, futuring, past-ing and present-ing at the
picket-line.

It is a potential place for meeting on the picket line through
liberating texts from behind paywalls, walled gardens and isolated
libraries.

Why not, it is also a place to create your own documentation of your
greatest chats, hottest book recommendations and most subversive lines
of thought?

It is an invitation and the result of *Collective Conditions* with the
intention of (in)forming strategies of resistance, including with
respect to the (academic) publication industries.

As academics and their allies, we benefit from these neoliberal sites of
publication, as well as the open or liberated sites which are
illegalized, with great costs and personal risks to those who maintain
those infrastructures.

*Bibliostrike* is a re-orientation towards different infrastructures of
distribution that resist the many ways in which current
biblio-technological practices come to optimise knowledge resource
allocation to benefit a few.

For instance, the new business model of academic publishers is pivoting
towards “knowledge products” and “information analytics” based on the
capture of trends on publication “sharing” sites like Researchgate as
well as Mendeley and SSRN (the latter owned by Elsevier which now
defines itself as a tech company).

*Bibliostrike* allows you to access and distribute texts without the nasty
analytics, free of the bubbly promises of narrow minded predictions, and
exploitative business models. To do so you can use the link above. To
read texts, you can just use your browser, if you wish you can download
the resources, and if you want you can upload your own to share with
others on the picket-line. You can also curate your special shelves for
tech-outs, protests, and campaigns.

There is a legal regime which casts a long shadow over the practices we
promote here. “To be clear”, the upload and download of materials that
fall under the copyright regime is not legal. For those of you for whom
this risk is concerning, we encourage you to inform yourself about the
different liberating struggles like copyfight, open access, free
licenses of distribution, the public domain and orphan books, among
others. These struggles come with many ways in which you can come to
resist neoliberal publication practices and industries within legal
boundaries.

*Bibliotecha* is a framework built by queer feminist activist groups which
see technical justice a inseparable part of social justice. It relies on
a microcomputer running open-source software to serve books over a local
wifi hotspot. Using the browser to connect to the library one can
retrieve or donate texts.

<!-- -------------- -->

## Constant <br />Collaboration Guidelines: <br />Questions and problems

### 26 September → 25 November 2019 <br />*Constant\_V*: Collaboration Guidelines

-   Various comments, graffiti, corrections, appreciations and suggestions on the window of Constant\’s office.<sup>5</sup>

`Look at the images of the comments on the window, read the texts and see if anything is important to implement in the guidelines.`

### 08 → 16 November 2019 <br />worksession: *Collective Conditions*

*Discussions on consent:*<sup>6</sup>

-   Repeating issues with/discussions on the need for making time for consent, assumptions of agency/control/empowerment and the ability to re-commit, continued consent or changing your mind. Different practices/genealogies of consent: as a notion used by activists and legislators. Implicit, explicit and associated consent.

`Insert a line about the variation of consent, a reminder that consent is not a given, but that it needs (re)affirmation, thinking about variation and evolution of consent (to be worked on).`

*From discussions on transformative justice + anti-carceral activism + Queering Damage:*<sup>7</sup>

-   The last part of the guidelines (’What if these Guidelines are not met?’) does not convince anyone. Several discussions throughout the week on how to deal with this. If there are Guidelines, they need to address responsibility and consequences in some way? At the same time, we feel that we might unwillingly reproduce carceral politics (rules → punishment). But how to approach this? We understand such guidelines as performative documents, so what you write about consequences becomes performative in itself?

`Something in the last section on accountability.` \
`- Rename the last bit “What if the guidelines are not met” to: “This is what we do”` \
`- Turn away from “rules and punishment” logic. This also might deal with the concern that people are instrumentalised to functionaries ...` \
`- Insert the possibility to have listeners as a method.`

-   Experiments with having every day two other “listeners” (otherwise known as whisper-bots) in the room; volunteers (not: organisers) that amplify, deflect, gather or document signals of transgression. Interesting experience of de-centralisation and collectivised responsibility.

`See above!`

-   Assuming that crime/harassment is not just other people\’s problems. Attempt to work with practices from Transformative Justice.<sup>8</sup>

-   Another discussion thread on the differences of “calling out” vs. “calling in”: How to take care of someone who doesn’t care?<sup>9</sup>

-   Concern about proceduralisation of guidelines, whereby people become functionaries and not part of the situation: who we are is entangled with how we see problems. Timing matters: processes for immediate action and safety, and ones for long term response. In general, understanding that guidelines cannot be (left) alone: it needs a lot more than precise formulations. Need for developing practices, shared experiences, continuous work.<sup>10</sup>

*A Critical COC discussion:*

-   Most COC we dealt with did not deal with class/economic differences (aside for maybe mentioning class as one of possible discrimination axis)

`Under : “Exchange information, experiences and knowledge”, inequalities created by class: absence / presence: We could insert an example, highlighting the fact that absence can be the result of economic/class differences.`

-   Very few COCs are simple enough in text-density / language level / terminology used (at least to be widely accessible or offer way to catch-up and enter as an outsider)

-   What is the space/time for checking-in/confirmations of commitments

`Mention the method of coming together and address everyone’s involvement and commitment, practically like the moments we have each morning during worksessions.`

*Discussions on audio-visual gymnastics:*<sup>11</sup>

-   Relates to this part of the guideline: “Do not share photographs or recordings on proprietary social networks unless explicit consent by all involved.”

-   How to not respond to surveillance capitalism by stopping to make and share pictures? How to keep abundance, pleasure, possibilities in documentation; what ways to become visible as collectives, especially when we refuse to be visible as identifiable individuals. Or is there no way out of the dominant regime of exploited visibility?

-   Experiments with audio-visual gymnastics. Other imaging practices that re-orient cameras away from faces and ways to sollicit on-the-spot consent of participants, rather than as a general rule or consent beforehand. Protocols for sorting through images afterwards (**no immediate publishing).**

`Thinking about publishing (pictures, videos, ...) that has to be discussed, negotiated and debated, it’s not immediate. For the short version somethig sloganesque can work: “We endorse discursive publishing “This paragraph can be inserted with some discussion into the long version of the guidelines after: “No immediate publishing”.`

-   Also: watch out for discrimination/exclusion based on use of specific technologies: explain what is the problem with these structures (fb, pomme, \...) and how to or not relate this to the use of apps, un.st-a(ble)gram etc.

*Notes from the Modes d\’emploi tables / on arrival:*

-   Conflating \“maternalise\” and \“paternalise\” is a problematic linguistic move because it is conceptually putting them at the same level in tems of patterns of oppression and violence; even though they have historically never been equal. So not to say that maternalising doesn\’t exist, but whether that it doesn\’t have the same weight than paternalising.

`Change the line on matronise and patronise with smth like this: “We pay attention to the way informations, experiences and knowledges are shared between us with “possible asymmetries”; need better sentence. Maternalising and paternalisation could be examples, such as now there is teacher /student?`

-   These guidelines are very good at creating a sense of community, common sense and will but we have to be careful that if someone doesn\’t comply to it, this person actually falls out of the “community”.

-   It is good to know that for instance here Constant people are mindful towards the compliance of these guidelines but a good way to involve participants so to share the responsibility towards each other would be to decide on a rotating team of participants who are not only very mindful during exchanges but are also the persons to go to if something went wrong in regards to the guidelines.

`Let’s find a wording that doesn’t fit within constraints or “not to do” vocabulary, so not using transgression for instance. Example of using extra-legal in the context of Authors of the future so not to go into il-legality but instead referring to another frame of legality.`

### 8 November 2019 <br />HP

-   I would suggest using “informed by” or “found useful” rather than “inspired by” because of the content of the text, somehow “inspired by” for me feels a bit uncomfortable and I think its because of the affective qualities of the word inspire (which include excite) and maybe because so often (and so wrongly) harassment complaints become fetishised as \"exciting\” in complaint processes.

`Yes, let’s change it!`

### 14 October 2019 <br />General Assembly Constant

-   It might help people understand what Constant is and does; a kind of manual for those who do not already know.

`But we’re not trying to make Constant more “intelligible” with the guidelines, this is not the point...`

-   To “come closer” to Constant what takes time. This is a social process, and guidelines might nog help or be in the way even.

### 26 September 2019 <br />vernissage *Constant\_V*: Collaboration Guidelines

-   When writing dossiers, we try to avoid negative statements and negative language. Is there a way to approach the guidelines with a similar attitude?

`The principle is interesting, we can do a scan of the guidelines and see what turns up. Let’s discuss the two examples that are annotated. Just want to be careful with sugar coating language avoiding negativity (joy and freedom for instance); we want to take a stance.`

This would give things like:

Constant is committed to environments where possible futures, complex collectivities and desired technologies can be experimented without fear. → *Constant is committed to environments where possible futures, complex collectivities and desired technologies can be experimented with <u>joy and freedom</u>.*

The spaces that we initiate are therefore explicitly opposed to sexism, racism, queerphobia, ableism and other kinds of hatefulness. → *The spaces that we initiate are therefore explicitly <u>inviting everything but</u> sexism, racism, queerphobia, ableism and other kinds of hatefulness.*

Because of the intensity of exchanges and interactions during worksessions, there are moments of disagreement and discomfort which are not to be avoided at any cost, but instead need to be acknowledged and discussed within the limits of your own safety and sanity. → *Because of the intensity of exchanges and interactions during worksessions, there <u>can be</u> moments of disagreement and discomfort which ~~are not to be avoided at any cost, but instead~~ need to be acknowledged and
discussed within the limits of your own safety and sanity.*

Even if some of the below guidelines sound obvious, we have experienced that being together is not always as self-evident as it might seem. → *Even if some of the below guidelines sound obvious, we have experienced that being together <u>can be more complicated</u> as it might seem.*

Don’t speak for others. Do not intrude or impose yourself. → *Speak for yourself only. Give/leave* (to be discussed: imply that someone has the space - yes but it is the case, some ppl will occupy more space than others for various reasons) *other people space.* (leave space for others?)

Exchange information, experiences and knowledge. Don’t patronise nor matronise. → *Exchange information, experiences and knowledge. Others
always know as much as you, it might take more time to discover it.*

etc.

### 20 September 2019 <br />*Constant aan Zee*: Discussion with <br />members + Constant team

-   The point of guidelines is maybe to **de-naturalize behaviors.** Does this interfere with making space for precarity @ Constant?

-   Proposal to **reorganise document**; for example: bring short version to the front before the \’commitment\” which contains some complicated language. In general: document is long and structure complex? Things get easily overlooked.

`For every occasion we have to look at the formatting. Clear and short if necessary, and long and informative when needed ...`

-   **Some of the examples** (for example: \’dead names\” in the \’no harassment\’-section) are **hard to understand** if you are not already aware, so misses the point

-   This might be a courageous step; to make such issues/guidelines explicit, given the current times.

-   “Support and foreground the use of Free Libre and Open Source software” sounds like a prerequisite. It should not be that someone who does not know about FLOSS already, cannot discover it with Constant.

-   The “we” in this document is ambiguous, on purpose. But how does that really work? Many questions about how to take **collective responsibility** for this, and where Constant is or should be (end)responsible.

-   Are these some kind of house-rules? But where does the “house” begin and end? **How to not make this territorial**? (maybe it is more a “field” - ref. Kate Rich)

-   List of hateful -isms: **add ageism**?

-   Questions about tone: **imperative or not**? In general, multiple but different allergies surface to wordings/tone. Might need multiple versions for catholics, calvinists, anarchists, puritans, anti-imperialists, \...

-   How to not forget that the **auto-restrictions** that this document might produce, also **generate possibilities for others** (ref. Jara Rocha)?

`Add in the intentions that the guidelines, with their “restrictions”, create possibilities for others (After “we have written these guidelines to think of ways ...”). hmmm. “the restrictions that these guidelines will produce for some of us, might create possibilities for others of us.”`

-   “Do not share photographs or recordings on proprietary social networks unless explicit consent.” (and following text in long version) is causing confusion. Why not consent forms? It is an attempt to make space for media/visual production on other terms (agendas not set by GAFAM<sup>12</sup>), instead of minimizing situations where images can be made and shared. But: might be in conflict with GDPR<sup>13</sup>? And might even be in conflict with FAL<sup>14</sup>\... Does this mean Constant gallery needs a \’non-commercial\” license? Need to think this through more.

`Maybe one of us can find a bit of time to attack the regulations and try to understand what they would uimply for us .. and from there we could think of ways to collectively re- imagine “data”, “protection”, “regulation” for example in a GDPR sprint?`

-   Proposal to add to short guidelines: “**accept the limits of your understanding**” \-- not everything can always be processed at the same time/level/speed by everyone (ref. *Algolit*)

`Why not write a specific license for the Gallery? Is this possible? Fun? Is it compatible with the GDPR ? “You are free to use these images but we don’t want you to share them on FB” :-) so more as a technopolitical statement then an enforceable regulation?`

-   One should be able to print this document from the website (print CSS needs attention)!

`There is no possibility to print the document at the moment, we’re reporting it on the Gitlab issue tracker (DONE, look for “print css”– ticket opened it already 11 months ago :-/)`

-------

1. Constant worksessions
    [http://media.constantvzw.org/wefts/103/](http://media.constantvzw.org/wefts/103/)
2. *Bibliotecha* documentation and installation manual:
    [[https://networksofonesown.varia.zone/Bibliotecha/index.html]{.underline}](https://networksofonesown.varia.zone/Bibliotecha/index.html)
    etherbox documentation and installation manual:
    [https://networksofonesown.constantvzw.org/etherbox/manual.html](https://networksofonesown.constantvzw.org/etherbox/manual.html)
3. Constant Collaboration Guidelines
    [http://media.constantvzw.org/wefts/123/](http://constantvzw.org/w/?u=http://media.constantvzw.org/wefts/123/)
4. About deep listening: Earl E. Bakken, Center for Spirituality &
    Healing. *Deep Listening*
    [https://www.csh.umn.edu/education/focus-areas/whole-systems-healing/leadership/deep-listening](https://www.csh.umn.edu/education/focus-areas/whole-systems-healing/leadership/deep-listening)
5. Images from the comments chalked on the Constant window
    [https://gallery.constantvzw.org/index.php/Collaboration\_Guidelines](https://gallery.constantvzw.org/index.php/Collaboration_Guidelines)
6. [http://constantvzw.org/collectiveconditions/no\_u\_in\_dump/etherdump/thepadis.diff.html](http://constantvzw.org/collectiveconditions/no_u_in_dump/etherdump/thepadis.diff.html)
7. *Queering Damage*
    [https://queeringdamage.hangar.org](https://queeringdamage.hangar.org/)
8. Transformative Justice is a set of D.I.Y and anti-authoritave
    methods and tools for engaging someone who has perpetuated harm in
    an accountability process.
    [https://supportny.org/transformative-justice/](https://supportny.org/transformative-justice/)
9. Notes from *Collective Conditions*, on Calling Out and Calling In:
    [http://constantvzw.org/collectiveconditions/no\_u\_in\_dump/etherdump/keepinout.diff.htm](http://constantvzw.org/collectiveconditions/no_u_in_dump/etherdump/keepinout.diff.html)
10. Notes from *Collective Conditions*, on implementation:
    [http://constantvzw.org/collectiveconditions/no\_u\_in\_dump/etherdump/consequences.diff.html](http://constantvzw.org/collectiveconditions/no_u_in_dump/etherdump/consequences.diff.html)
11. Notes from *Collective Conditions*, on Audiovisual Gymnastics:
    [http://constantvzw.org/collectiveconditions/no\_u\_in\_dump/etherdump/pictures\_recordings.diff.html](http://constantvzw.org/collectiveconditions/no_u_in_dump/etherdump/pictures_recordings.diff.html)
    and
    [http://constantvzw.org/collectiveconditions/no\_u\_in\_dump/etherdump/audio-visual-gymnastics.diff.html](http://constantvzw.org/collectiveconditions/no_u_in_dump/etherdump/audio-visual-gymnastics.diff.html)
12. GAFAM: Google Amazon Facebook Microsoft
13. GDPR: General Data ProtectionRegulation
14. FAL: Free Art License
    [http://artlibre.org/licence/lal/en/](http://artlibre.org/licence/lal/en/)

<small>All documentation, including the documents in this publication, can be found here: <https://iterations.space></small>
