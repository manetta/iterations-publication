% Por debajo y por los lados. Sostener infraestructuras feministas con ficción especculativa
% spideralex

# Por debajo y por los lados: <br />Sostener infraestructuras feministas con ficción especulativa

Los talleres de “***Futurotopías feminis-<br />tas***”<sup>&nbsp;1</sup> han
demostrado ser capaces de aportar lo que un grupo necesita en ese
momento para poder sostenerse. En ellos compartimos acerca de cómo la
ficción especulativa puede sostener nuestros esfuerzos para la creación
de infraestructuras, comunitarias y feministas. A veces han permitido
que una comunidad identifique sus malestares y ponga el foco en lo que
necesita sanar, otras veces han servido para contrapesar violencias
estructurales causadas por el ***patriarcalismo*** (esa alianza criminal
del patriarcado y el capitalismo), en otras nos ha permitido un respiro
juntas para seguir cuidando de los posibles; y en otras ocasiones ha
activado ecosistemas habitados por narrativas, personajes de ficción y
llamadas a la acción colectiva transformadora.

En los párrafos que siguen partimos de algunas fricciones que se dan
entre la infraestructura comunitaria/libertaria y la feminista. Pensamos
en cómo las ficciones especulativas ayudan a sostener la infraestructura
feminista y lo ilustramos con algunas técnicas para hacer talleres de
ficción especulativa juntas.

------

Infraestructuras rugosas
------------------------

> **Infraestructura:** Construir una casa o algo que te sostiene desde
> algún lugar (diferente al mercado). Hacer comunidades situadas en una
> multitud de lugares.

Soy parte de una comunidad (Calafou) que “construyo especulativamente”
con muchas otras personas que no siempre he elegido, acostumbrándome a
sentir con ellas a quererlas. Un ser polimorfo monstruoso llamado
comunidad que siempre enraíza en un territorio y un paisaje. Para
construir comunidad hay que volver a un territorio donde se hace y
deshace la comunidad, compuesta por seres sentipensantes, pandillas,
voluntades, experiencias, trayectorias, subjetividades y disonancias
varias.

Calafou consiste en recorrer uno de los posibles caminos que separa lo
distópico cyberpunk de una futurotopía. ¿Cómo se hace cuando siempre nos
dijeron que la infraestructura o nos matará (*kill you*) o nos poseerá
(*enslave*)? En Calafou, a veces, nos pasan ambas cosas.

Multi-gestionamos juntxs una ***infraestructura viva***. Eso genera a
veces estrés, frustraciones, miedos y sentimientos de culpa. Uno de los
valores de la comunidad reside en cuántas equivocaciones se permite por
el camino. También genera técnicas, herramientas, conocimientos e
infraestructura. Otro de sus valores radica en cuánto se permite soñar y
especular juntas.

Hacemos especulativamente infraestructura con otrxs, sin saber siempre
cómo hacerlo. ¿Qué hay que fijar, sistematizar, documentar? ¿Qué hay que
dejar escapar y no tocar? ¿Qué hay que regular y gobernar? ¿Qué es lo
que no tiene nada que ver con todo esto? ¿Cómo superar las tensiones
causadas por los procesos de fijación y seguir permitiendo que la
infraestructura también sea fluida, ligera, adaptable?

A veces me gustaría ser *Binti*<sup>&nbsp;2</sup> *la armonizadora*,
colaborar en que las energías de las entidades que conforman la
comunidad puedan confluir, resolver, transformar juntas. Saber hacerlo
con todas las especies presentes en el territorio, activar esa interfaz.
Las siento cerca y lejos, la puesta a distancia de lo sensible sigue
allí e interfiere en nuestras proyecciones de las infraestructuras
comunitarias posibles.

Infraestructuras que tienen tanto de técnicas como de tecnologías. Si
algo las delimita entre ellas, ese algo es relativo, borroso, cambiante.
Redes comunitarias de comunicación, comunidades que mantienen y
fortalecen saberes, infraestructuras que sostienen comunidades diversas.
Hablamos de infraestructuras que se enfocan en lo que consideramos como
básicamente necesario: luz, agua, caca, pis, conectividad, compost,
aguas residuales, residuos orgánicos/electróni-<br />cos/industriales,
alojamiento, producción de cosas materiales e inmateriales; y a veces se
procesa por ejemplo haciendo comidas y bebidas.

Se trata de la infraestructura básica que te sostiene y te consume,
porque ese tipo de infraestructura no siempre te libera, ni a tu tiempo,
ni a tu energía. No estoy segura de cómo se construyen infraestructuras
en comunidad que nos sostengan y sean a la vez sostenibles
*energéticamente* y emocionalmente. Y puede que la infraestructura
feminista nos ofrezca pistas adicionales por explorar.

Infraestructuras feministas
---------------------------

Entendemos por ***infraestructuras feministas*** todo aquello que que
sostiene y apuntala con recursos, más o menos estables, las luchas
feministas para su desarrollo y avance. Por ***recursos*** nos referimos
a técnicas, tecnologías y procesos (analógicos, digitales, sociales)
incluyendo espacios seguros, refugios, bibliotecas, redes de sororidad y
de confianza, servidores, páginas amarillas, repositorios, bots,
herramientas de documentación y memoria, enciclopedias, HerStories,
técnicas para la vida, conjuros, rituales y exorcismos. Se incluye aquí
también lo móvil, efímero y transicional que pueda residir en la
infraestructura temporal de encuentros, talleres y fiestas que nutren la
confianza, el cariño y el bienestar de las compañeras feministas.

Creemos que la infraestructura feminista se encuentra ***por debajo y
por los lados***, es a menudo precaria y a veces difícil de ver. Pero es
extensa, distribuida y pone en su centro el valor y afecto que se
ofrecen entre sí las personas, máquinas y ecosistemas que la componen.

Nuestras perspectivas y condiciones de acceso, uso y desarrollo de las
tecnologías están profundamente influenciadas por cómo el patriarcado,
el capitalismo y el colonialismo están arraigados en nuestro cotidiano y
las sociedades en las que vivimos. Por ello necesitamos desarrollar
metodologías nuevas para identificar los procesos que están creando
infraestructuras feministas y cómo éstas nos señalan técnicas y
tecnologías liberadoras pensadas desde y para la vida.

La creación de infraestructura feminista trata de darnos respuestas y
valor. Pensar en la diversidad de nuestras contribuciones y acciones nos
permite abrir nuevos horizontes de acción política, así como procesos
restaurativos, y modelar otras posibilidades para todas nosotras.

Puede que la infraestructura feminista sea aquella que ofrece además
capacidad de sostenerse mediante la acción de trabajar juntxs los miedos
y vergüenzas impuestos por el sistema ***patriarcalista***. La
infraestructura feminista se parece a la infraestructuras comunitarias
anteriormente detalladas, pero se hace desde otros lugares y
motivaciones.

Las mujeres y las compañeras feministas siempre han estado allí, por
debajo y por los lados, compartiendo ***técnicas para la vida*** y
haciendo ***tecnologías apropiadas*** (desde una idiosincrasia que no
contamina ni resta), tecnologías “lentas”, tecnologías ancestrales,
“tecnologías menores”, y tecnologías libres en pos de la soberanía y
autonomía de las comunidades que las desarrollan. Hemos sido portadoras
y garantes de que el conocimiento acerca de esta diversidad de técnicas
se compartiera y evolucionara dentro de comunidades ya que, como nos
recuerda Margarita Padilla: “*todas las tecnologías se desarrollan en
comunidades, que pueden ser, más o menos, autónomas o pueden estar, más
o menos, controladas por las corporaciones. En la lucha por la
soberanía, la cosa va de comunidades. Nadie inventa, construye o
programa en solitario, sencillamente porque la complejidad de la tarea
es tal que eso resultaría imposible”*<sup>&nbsp;3</sup>.

Técnicas para la vida <br /><><br />Tecnologías apropiadas
-----------------------------------------------------

Pero, ¿qué es lo que delimita la línea sensible entre técnicas y
tecnologías y por qué eso importa para la infraestructura feminista?
Según Biagini y Carnino, la técnica es “*a la vez un saber hacer y unas
herramientas, es decir, un conjunto de procesos informales y su
sedimentación instrumental en los objetos producidos por lx
artesanx*<sup>&nbsp;4</sup> \[...\] La tecnología es un conjunto de
procesos macrotécnicos (es decir, procesos que son más grandes que el
ser humano y la comunidad de una aldea) que son posibles gracias a la
alianza de la ciencia y la tecnología”<sup>&nbsp;5</sup>.

La técnica puede ser no tecnológica, pero la tecnología se hace
absorbiendo técnicas. En la sistematización inducida por la producción
de tecnologías, se pueden obviar y destruir por completo técnicas para
la vida. Las que nos ofrecían otras maneras de pensar nuestra relación
con el entorno y los valores que proyectamos en esa relación.

Biagini y Carnino nos aportan otro elemento central de reflexión al
desatacar que “*Según Wigney, el mundo antiguo seguía produciendo hechos
relativos en lugar de hechos absolutos porque se basaba en conocimientos
esencialmente situados y difíciles de transferir a otros lugares. La
industrialización y su corolario, la proletarización definida como
despojo artesanal, sólo fue posible a escala masiva con la ayuda de la
ciencia en desarrollo. Esta ciencia, lejos de ser especulativa, está
profundamente arraigada en la realidad: es un hecho”*<sup>&nbsp;6</sup>.

En ese deslizamiento sentimos de nuevo el potencial de la práctica de
ficciones especulativas para recuperar esas técnicas para la vida y ver
cómo nos llevan hacia tecnologías apropiadas para nuestras comunidades.
En lo situado y lo especulativo encontramos manera de deshacernos del
mito de la ciencia y el progreso tecnológico. Esa tecnología se define
ante todo como una política de hechos consumados (*move fast and break
things*). Muy pocos participan en soñarla, diseñarla, decidirla; pero
todas nosotras estamos expuestas a los efectos de su implementación. No
deja lugar a un diseño especulativo colectivo de las técnicas y
tecnologías que necesitamos y nos merecemos. Las ciencias modernas y las
“nuevas” tecnologías se basan en poner a distancia, anular o absorber
las técnicas necesarias para la vida y nos impiden encontrar los pasos,
atajos y caminos hacia nuestras tecnologías apropiadas.

Según Elleflane, una “*tecnología adecuada y también apropiada, copiada,
obtenida. \[…\] describe aquella tecnología que mejor se adecua a
situaciones medioambientales, culturales y económicas, requiere pocos
recursos, implica menos costos, tiene un bajo impacto ambiental, no
requiere altos niveles de mantenimiento, se genera con destrezas,
herramientas y materiales de la zona y puede ser localmente reparada,
modificada y transformada. Al fin y al cabo, ¿qué comunidad no necesita
que una tecnología sea eficiente, se comprenda y se adapte a su contexto
medioambiental, cultural y económico propio*?”<sup>&nbsp;7</sup>. En esta
comprensión y consentimiento mutuo entre comunidades y sus tecnologías
apropiadas se encuentran las claves para una infraestructura feminista
que sostiene ecosistemas regeneradores. Como si estuvieran basados en
procesos de autopoiesis, se alimentan de nuestras ideas, memorias,
narrativas, historias, fabulaciones y deseos.

Vocabularios nuevos <br />para crear mundos
-------------------------------------

Lo que no se nombra no puede existir, por ello invitamos a crear nuevos
vocabularios y técnicas para explorar estas futurotopías e
infraestructuras feministas. Por ejemplo, podríamos hablarnos desde
estas nociones para darles forma:

<df>Eco-tecnología (*Toward an ecological society*, Murray Bookchin): “*Una
eco-comunidad podría ser sostenida por una nueva clase de tecnología
compuesta de maquinaria flexible y versátil cuyas aplicaciones
productivas garanticen la durabilidad y la calidad, no siendo
programadas para la obsolescencia ni para producir una cantidad absurda
de bara-<br />tijas y entrar en la rápida circulación de mercancías básicas*”.</df>

<df>Eco-economía (*Trilogía de Marte*, Stanley Robinson):  ¿Cómo creamos en este y los otros planetas una economía que pone en su centro una ecología basada en la solidaridad geológica e inter-especies?</df>

<df>Cacátedra (*Ecosec*):  Volverse experta en gestionar nuestra mierda, en el sentido literal y figurado, toda la cadena que implica expulsarla, recogerla, sacarla a paseo y compostarla. Devolverle su dignidad como indicio de buena suerte y abono para la tierra.</df>

<df>Para-paraíso (*Tatiana, THF Editorial*):  Se trata de un paraíso cuidado por antiguos paramilitares reconvertidos en guías turísticos y guardianes del bosque y los ríos, o de un paraíso situado en una dimensión paralela/alternativa que está muy cerca pero a la cual no sabes aun cómo llegar.</df>

<df>Narcofeminismo (*Metzineres*): Teoría práctica basada en lo desarrollado y hablado en los espacios autogestionados por mujeres que consumen drogas, construyendo una relación diferente con un hábito de consumo a través del feminismo y la solidaridad, empoderándose juntas para sobrepasar situaciones de violencia de genero sistémicas. Apropiarse de tecnologías blandas, químicas y relacionales.</df>

<df>Futurotopía (*B01*):  Contraer los futuros y nuestras utopías, resaltar la posibilidad de pensar juntas en los futuros hacia los cuales queremos caminar juntas, volviéndolos posibles al permitir pensarlos y contárselos a otras personas. Para trabajar juntas en el eje del tiempo, también proponemos a continuación algunas técnicas de Black Quantum Futurism<sup>&nbsp;8.</sup></df>

<df>*Visión de futuro: a través de este modo de práctica se aumenta la capacidad de conocer el futuro al ser capaz de verlo con más claridad visual de lo normal. Este modo implica poca o ninguna desviación del futuro, sólo una mayor precisión al visualizarlo.*</df>

<df>*Alteración futura: este modo de práctica implica una estrecha desviación de la realidad presente, utilizando lo que ya está disponible y lo que es estadísticamente probable para elegir el futuro de un pequeño subconjunto de futuros probables.*</df>

<df>*Manifestación futura: este modo de práctica implica el mayor grado de creatividad, permitiendo al practicante construir el futuro paso a paso, pieza a pieza.*</df>

Y añadimos generar nuestras propias profecías autorrealizadas “*una predicción que, una vez hecha, es en sí misma la causa de que se haga realidad*”.

Finalmente, listamos otras técnicas posibles<sup>&nbsp;9</sup> que hemos ido
detectando en nuestras ficciones especulativas: visionar, relatar
nuestros sueños y pesadillas, contar nuestras memorias, meditar, dormir
despiertas, ramificar, crear recuerdos del futuro, rescatar y exorcizar
pasados no escritos, apuntar a los vacíos, suturar las grietas, viajar
en nuestras cuerpas, crear procesos restaurativos, sanar los traumas,
limitar las narrativas impuestas, escritura automática.

-----

Pensar nuestras infraestructuras desde los puntos de tensión
------------------------------------------------------------

La infraestructura comunitaria y la feminista comparten puntos en
común y algunas tensiones que las atraviesan. \
\
Creemos que ambas se basan en procesos de hacer juntas de manera
especulativa. Ambas pueden explotar, reventar, desaparecer rápidamente
también. \
\
Implican técnicas para la vida, tecnologías que permiten
sistematizar/fijar ciertos procesos y que existen en ecosistemas que
necesitan ser documentados, comunicados, circulados para así existir. \
\
Tienden a sistematizar/sedimentar ciertos procesos, absorbiendo técnicas
para la vida y haciendo a veces con ellas tecnologías apropiadas. \
\
Orientan ciertas necesidades/acciones hacia algunos recursos pensados
para cubrirlos/darles una respuesta, generando efectos varios que no
sabemos aún cómo rastrear/leer. \
\
Las infraestructuras tienden a (re)generar y (a)cumular, y hay que
revisar con periodicidad <br /><br />la alquimia que se deriva de esa tensión, para
drenar o regar/alimentar a tiempo. \
\
Las infraestructuras estables, o las que están en beta, nos adentran en
una tensión paradójica entre “ganar” en autonomía y no “perder” en
independencia. \
\
Así que en sí mismas son preguntas abiertas acerca de cómo podemos
seguir con ellas o vivir sin ellas, y en qué grado (o de qué modos) lo
hacemos posible juntas.

\
\

1. https://zoiahorn.anarchaserver.org/specfic/
2. https://en.wikipedia.org/wiki/Nnedi\_Okorafor
3. Margarita Padilla, “Soberanía Tecnológica, Volumen 2”, 2018, Disponible: https://www.ritimo.org/IMG/pdf/sobtech2-es-with-covers-web-150dpi-2018-01-13-v2.pdf
4. El lenguaje es también una tecnología, a menudo binaria, y en esta ocasión he decidido des-binarizar alx artesanx.
5. “On arrête parfois le progrès”, Cedric Biacini y Guillaume Carnino, “Les luddites en France: Résistances a l’industrialisation et a l’informatisation", Ed. L’échapée, 2010
6. *ibid*
7. De las tecnologías apropiadas a las Tecnologías Re-Apropiadas, por Elleflâne, Soberanía Tecnológica, Volumen 2”, 2018, Disponible: https://www.ritimo.org/IMG/pdf/sobtech2-es-with-covers-web-150dpi-2018-01-13-v2.pdf
8. https://www.blackquantumfuturism.com/
9. https://zoiahorn.anarchaserver.org/specfic/2020/02/11/iterations/