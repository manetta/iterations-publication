#! /bin/bash

for file in $(ls *.pdf); do

	pdffile=$file;
	dpi=200

	gs \
	  -o "./small/${pdffile%.pdf}.pdf" \
	  -sDEVICE=pdfwrite \
	  -dDownsampleColorImages=true \
	  -dDownsampleGrayImages=true \
	  -dDownsampleMonoImages=true \
	  -dColorImageResolution=$dpi \
	  -dGrayImageResolution=$dpi \
	  -dMonoImageResolution=$dpi \
	  -dColorImageDownsampleThreshold=1.0 \
	  -dGrayImageDownsampleThreshold=1.0 \
	  -dMonoImageDownsampleThreshold=1.0 \
	   "${pdffile}"

done

