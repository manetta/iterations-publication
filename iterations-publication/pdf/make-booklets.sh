for pdf in $(ls iterations-*.pdf); do
	echo
	echo $pdf
	pdfbook --papersize '{170mm,220mm}' --no-twoside $pdf -o booklets/$pdf
done
