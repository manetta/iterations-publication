from reportlab.lib.units import mm

bleed = 0*mm
# bleed = 3*mm

pdffilename = 'iterations'
# pdffilename = 'iterations-iterationen-iteraties-itérations-iterationes-iterations'
pagesizes = [110*mm+(2*bleed), 170*mm+(2*bleed)]
# title = 'iterations-iterationen-iteraties-itérations-iterationes-iterations'

contributions = {
	0 : {
		'title' : 'cover - traces',
		'pdftitle' : 'cover - traces',
		'author' : '---',
		'filename' : 'cover'
	},
	1 : {
		'title' : 'holding spell',
		'pdftitle' : 'holding spell',
		'author' : 'Kym Ward',
		'filename' : 'holding-spell'
	},
	2 : {
		'title' : 'Becoming Rica Rickson',
		'pdftitle' : 'Becoming Rica Rickson',
		'author' : 'Rica Rickson',
		'filename' : 'rica-rickson'
	},
	3 : {
		'title' : 'Por debajo y <br />por los lados. <br /><br />Sostener <br />infraestructuras feministas <br />con ficción <br />especculativa',
		'pdftitle' : 'Por debajo y por los lados. Sostener infraestructuras feministas con ficción especculativa',
		'author' : 'spideralex',
		'filename' : 'spideralex'
	},
	4 : {
		'title' : 'Dear visitor,',
		'pdftitle' : 'Dear visitor,',
		'author' : 'Behuki',
		'filename' : 'behuki'
	},
	5 : {
		'title' : 'Three <br />documents',
		'pdftitle' : 'Three documents',
		'author' : 'Collective Conditions',
		'filename' : 'collective-conditions'
	},
	6 : {
		'title' : 'scores',
		'pdftitle' : 'scores',
		'author' : 'common ground',
		'filename' : 'scores'
	},
	7 : {
		'title' : 'The Future <br />of Artistic <br />Collaboration<br />in Digital Contexts',
		'pdftitle' : 'The Future of Artistic Collaboration in Digital Contexts',
		'author' : 'Nayari Castillo (esc) <br />Reni Hofmüller (esc) <br />Lluís Nacenta (Hangar) <br />Peter Westenberg (Constant)',
		'filename' : 'introduction'
	},
	8 : {
		'title' : 'backcover - traces',
		'pdftitle' : 'backcover - traces',
		'author' : '---',
		'filename' : 'backcover'
	},
	9 : {
		'title' : 'x-dex',
		'pdftitle' : 'x-dex',
		'author' : 'Jara Rocha<br />Manetta Berends',
		'filename' : 'x-dexing'
	}
}