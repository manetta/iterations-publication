from reportlab.lib.units import mm

# blue
xcurves = (1, 0.15, 0, 0)
xcurvesback = (0.5, 0, 0, 0)

# pink
xcolors = (0, 0.7, 0, 0)
xcolorsback = (0, 0.4, 0, 0)

# green
xtext = (0.75, 0, 1, 0)
xtextback = (0.5, 0, 0.5, 0)

# orange
xabsences = (0, 0.45, 1, 0)
xabsencesback = (0, 0.25, 0.5, 0)


xdexcolor1 = xtext # inside+back cover background
xdexcolor2 = (0, 0, 0, 1) # textcolor
xdexcolor3 = xcolors  # x-dex cover letter
xdexcolor4 = (0, 0, 0, 0.1) # backcover title
