# -*- coding: UTF-8 -*-
# https://github.com/youtubetotaltechnology/source/blob/master/reportlab_tutorial_50.py

import os, sys
from functools import partial

from reportlab.platypus.doctemplate import SimpleDocTemplate, PageTemplate, PageBreak, BaseDocTemplate
from reportlab.graphics.shapes import Drawing, Line, PolyLine
from reportlab.pdfgen import canvas
from reportlab.platypus import Paragraph, NextPageTemplate, Frame, Spacer, FrameBreak
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import mm

from functions import *
from settings import *
# from colors import * 
from stylesheet import stylesheet

# Reportlab API Reference 
# https://kite.com/python/docs/reportlab 

# PRE-STEP: 
# saving a function as a variable, in order to insert it to the PageTemplate
# https://gist.github.com/squarepegsys/e9b7426af65cf5f1b94312beaca7d56d
# addHeader = partial(createHeader, title='Iterations', author='ALL OF US')

for x, contribution in contributions.items():
	
	title = contributions[x]['title']
	author = contributions[x]['author']
	filename = contributions[x]['filename']
	pdftitle = contributions[x]['pdftitle']
	pdffilename = 'iterations-{}.pdf'.format(filename)

	pdf = BaseDocTemplate('./pdf/{}'.format(pdffilename),
		pagesize=pagesizes,
		pageTemplates=[],
		showBoundary=0,
		leftMargin=mm,
		rightMargin=mm,
		topMargin=mm,
		bottomMargin=mm,
		allowSplitting=1,
		title=pdftitle,
		author=author,
		_pageBreakQuick=1,
		_debug=1,
		encrypt=None,
		# cropMarks=True,
		# cropBox=(0*mm,0*mm,116*mm,176*mm),
		# artBox=(0*mm,0*mm,116*mm,176*mm),
		# trimBox=(15*mm,15*mm,113*mm,173*mm),
		# bleedBox=(0*mm,0*mm,116*mm,176*mm),
		# enforceColorSpace='CMYK',
		)

	story = []
	
	# ---
	# set defaults
	custom_style = stylesheet['main']
	custom_template = 'main'

	# ---
	# prepare custom section cover
	sectionCover = partial(makeSectionCover, contribution=filename, bookletnr=x)
	addSectionCover = None

	prepareTracesPages = partial(drawTracesPages, filename=filename)
	# prepareXDEXLines = partial(addXDEXLines, filename=filename)

	# ---
	# set specific templates and typographic styles
	if 'cover' in filename:
		custom_template = 'cover'
		story.append(NextPageTemplate('cover'))
	elif 'spell' in filename:
		custom_template = 'spell'
		custom_style = stylesheet['spell']
		addSectionCover = True
	elif 'rica' in filename:
		custom_template = 'rica'
		custom_style = stylesheet['rica']
		addSectionCover = True
	elif 'spideralex' in filename:
		custom_style = stylesheet['spideralex']
		addSectionCover = True
	elif 'behuki' in filename:
		custom_template = 'behuki'
		custom_style = stylesheet['behuki']
		addSectionCover = True
	elif 'collective-conditions' in filename:
		custom_style = stylesheet['collectiveconditions']
		addSectionCover = True
	elif 'scores' in filename:
		custom_style = stylesheet['scores']
		addSectionCover = True
	elif 'introduction' in filename:
		custom_template = 'introcolumns'
		addSectionCover = True
	elif 'x-dexing' in filename:
		custom_template = 'xdexcolumns'
		addSectionCover = True

	# ---
	# start of a new section (booklet)
	print('... inserting {}'.format(title))

	# ---
	# add sectioncover
	if addSectionCover == True:
		if 'introduction' in filename:
			para = Paragraph(title, style=stylesheet['h1white'])
			author = Paragraph(author, style=stylesheet['h2white'])
		elif 'x-dex' in filename:
			para = Paragraph(title, style=stylesheet['h1xdexcover'])
			author = Paragraph(author, style=stylesheet['backcoverxdex'])
		else:
			para = Paragraph(title, style=stylesheet['h1'])
			author = Paragraph(author, style=stylesheet['backcover'])
		space = Spacer(110*mm, 0*mm)
		story.append(space)
		story.append(para)
		story.append(PageBreak())
		story.append(author)

	# ---
	# open section content
	lines = open('./rml/{}.html'.format(filename), 'r').readlines()
	
	# ---
	# switch to custom page template
	if not any(string in filename for string in ['cover', 'timeline', 'colophon']):
		story.append(NextPageTemplate(custom_template))
		story.append(PageBreak())
	
	# work in progres...
	prepareBehukiSection = partial(addBehukiSection, section='')

	# start processing content, line by line
	img_count = 0
	scorecount = 0
	firstintroh1 = False
	xdexhandlecounter = 0
	for line in lines:
		if '<img' in line:
			search = re.search('src=.*? ', line)
			match = search.group(0)
			src = match.replace('src=', '').strip()
			print('::: image src:', src)
			if 'logos-partners' in line:
				# colophon partner organisation logo's
				img = Image(src, width=30*mm, height=15*mm, hAlign='CENTER')
			elif 'introduction' in filename:
				# colophon funding logo's
				img = Image(src, width=30*mm, height=35*mm, hAlign='CENTER')
			elif 'behuki' in filename: 
				img = Image(src, width=90*mm, height=150*mm, hAlign='CENTER')
				story.append(NextPageTemplate("behukiIMG"))
				if img_count == 0:
					story.append(PageBreak())
			elif 'scores' in filename: 
				img = Image(src, width=110*mm, height=170*mm, hAlign='CENTER')
				story.append(NextPageTemplate("scoresIMG"))
				if img_count == 0:
					story.append(PageBreak())
			else:
				print('<<< this IMG is not inserted >>>')
			story.append(img)
			img_count += 1
			if 'logos-partners' in line:
				space = Spacer(45*mm, 2.5*mm)
				story.append(space)
			story.append(NextPageTemplate(custom_template))
		elif '<h1' in line:
			# applies to introduction + xdex introduction
			if any(string in filename for string in ['introduction', 'x-dexing']):
				if 'introduction' in filename:
					if firstintroh1 == True:
						if not 'colophon' in line.lower():
							story.append(PageBreak())
					else:
						firstintroh1 = True
				elif 'beta' in line.lower():
					story.append(NextPageTemplate(['xdexScores']))
					story.append(PageBreak())	
				else:
					if firstintroh1 == True:
						story.append(FrameBreak())
					else:
						firstintroh1 = True
				# set hyphenation language for each language in introduction
				if 'introduction' in filename:
					if 'itera-<br />cions' in line.lower():
						custom_style = stylesheet['introcolumnsCAT']
						para = Paragraph(line, style=stylesheet['h1white'])
						story.append(para)
					elif 'itera-<br />tionen' in line.lower():
						custom_style = stylesheet['introcolumnsDE']
						para = Paragraph(line, style=stylesheet['h1white'])
						story.append(para)
					elif 'itera-<br />tions' in line.lower():
						custom_style = stylesheet['introcolumnsEN']
						para = Paragraph(line, style=stylesheet['h1white'])
						story.append(para)
					elif 'itera-<br />ciones' in line.lower():
						custom_style = stylesheet['introcolumnsES']
						para = Paragraph(line, style=stylesheet['h1white'])
						story.append(para)
					elif 'itéra-<br />tions' in line.lower():
						custom_style = stylesheet['introcolumnsFR']
						para = Paragraph(line, style=stylesheet['h1white'])
						story.append(para)
					elif 'itera-<br />ties' in line.lower():
						custom_style = stylesheet['introcolumnsNL']
						para = Paragraph(line, style=stylesheet['h1white'])
						story.append(para)
					elif 'colophon' in line.lower():
						custom_style = stylesheet['colophon']
						story.append(NextPageTemplate(['colophon']))
						story.append(PageBreak())
				# for x-dexing booklet h1's, color them
				elif 'x-dexing' in filename:
					if 'beta-version' in line.lower():
						story.append(NextPageTemplate(['xdexScores']))
						custom_style = stylesheet['xdexscores']
						para = Paragraph(line, style=stylesheet['h1'])
						story.append(para)							
						story = insertXdexScores(story) # insert scores !!! 
					# x-dex introduction
					else:
						if 'anindexa-<br />ció' in line.lower():
							custom_style = stylesheet['xdexcolumnsCAT']
						elif 'x-dizierung' in line.lower():
							custom_style = stylesheet['xdexcolumnsDE']
						elif 'x-dexing' in line.lower():
							custom_style = stylesheet['xdexcolumnsEN']
						elif 'exdexa-<br />ción' in line.lower():
							custom_style = stylesheet['xdexcolumnsES']
						elif 'x-dexage' in line.lower():
							custom_style = stylesheet['xdexcolumnsFR']
						elif 'x-dexeren' in line.lower():
							custom_style = stylesheet['xdexcolumnsNL']
						space = Spacer(50*mm, 11)
						para = Paragraph(line, style=stylesheet['h1xdex'])
						story.append(space)
						story.append(para)	
						story.append(space)
		elif '<h2' in line:
			if 'behuki' in custom_template:
				if 'opt' in line:
					behukiHeader = '/opt'
				elif 'var' in line:
					behukiHeader = '/var'
				elif 'home' in line:
					behukiHeader = '/home /boot /lost & found'
				prepareBehukiSection = partial(addBehukiSection, section=behukiHeader)
				story.append(NextPageTemplate(custom_template))
			elif 'introduction' in filename:
				para = Paragraph(line, style=stylesheet['h2white'])
				story.append(para)
			elif 'x-dexing' in filename:
				if '<h2>score</h2>' in line.lower():
					para = Paragraph(line, style=stylesheet['h2xdex'])
					story.append(para)
					scorecount += 1
				else:
					space = Spacer(50*mm, 11)
					story.append(space)
					para = Paragraph(line, style=stylesheet['h2xdex'])
					story.append(para)
			else:
				space = Spacer(110*mm, 6.5)
				story.append(space)
				para = Paragraph(line, style=stylesheet['h2'])
				story.append(para)
		elif '<h3' in line:
			para = Paragraph(line, style=stylesheet['h3'])
			story.append(para)
		elif '<hr' in line:
			story.append(PageBreak())
		elif '<framebreak>' in line:
			story.append(FrameBreak())
		elif '<small' in line:
			if 'introduction' in filename:
				para = Paragraph(line, style=stylesheet['smallwhite'])
			elif 'x-dex' in filename:
				para = Paragraph(line, style=stylesheet['smallxdex'])
			else:
				para = Paragraph(line, style=stylesheet['small'])
			story.append(para)
		elif '<blockquote' in line:
			if 'introduction' in filename:
				para = Paragraph(line, style=stylesheet['blockquotewhite'])
				story.append(para)
			else:
				para = Paragraph(line, style=stylesheet['blockquote'])
				story.append(para)
		elif '<ul' in line: 
			listtype = 'ul'
			continue
		elif '<ol' in line:
			listtype = 'ol'
			count = 0
		elif '<li' in line:
			if listtype:
				if listtype == 'ul':
					if 'x-dex' in filename:
						if any(string.lower() in line.lower() for string in ['<li>handle', '<li>handvat', '<li>Nansa', '<li>Asidero', '<li>Poignée']):
							xdexhandlecounter += 1
							import re
							items = re.split(': |\. ', line)
							header = Paragraph(items[0], stylesheet['xdexcolumntables'])
							text = Paragraph(items[1], stylesheet['xdexcolumntables'])
							description = Paragraph(items[2], stylesheet['xdexcolumntables'])
							story = addSimpleTable(header, text, description, xdexhandlecounter, story)
							if xdexhandlecounter == 4: 
								xdexhandlecounter = 0
						elif any(string.lower() in line.lower() for string in ['<li><em>curves', '<li><em>Kurven', '<li><em>Corbes', '<li><em>curvas', '<li><em>Rondingen', '<li><em>courbes']):
							print('xxx FORM CURVE !!!')
							para = Paragraph(line, stylesheet['color-curves'])
							story.append(para)
						elif any(string.lower() in line.lower() for string in ['<li><em>colors', '<li><em>kleuren', '<li><em>colores', '<li><em>Farben', '<li><em>couleurs']):
							para = Paragraph(line, stylesheet['color-colors'])
							story.append(para)
						elif any(string.lower() in line.lower() for string in ['<li><em>text', '<li><em>texto', '<li><em>tekst']):
							para = Paragraph(line, stylesheet['color-text'])
							story.append(para)
						elif any(string.lower() in line.lower() for string in ['<li><em>absences', '<li><em>Ausencias', '<li><em>absenties', '<li><em>Auslassungen', '<li><em>absències']):
							para = Paragraph(line, stylesheet['color-absences'])
							story.append(para)
						else:
							d = Drawing(45*mm, 0)
							l = Line(0, 0, 45*mm, 0, strokeWidth=0.1*mm)
							d.add(l)
							para = Paragraph(line, style=stylesheet['xdexlistitem'], bulletText='')
							story.append(para)
							story.append(d)
					else:
						para = Paragraph('---&nbsp;&nbsp;{}'.format(line), style=stylesheet['li'])
						story.append(para)
				elif listtype == 'ol':
					count += 1
					if count < 10:
						c = '&nbsp;' + str(count)
					else:
						c = str(count)
					para = Paragraph('{} - {}'.format(c, line), style=stylesheet['footnotes'])
					story.append(para)
		elif '<code' in line:
			if 'x-dexing' in filename:
				para = Paragraph(line, style=stylesheet['xdexscore'])
				story.append(para)
				d = Drawing(75*mm, 22)
				l = PolyLine([
					0*mm,0, 
					10*mm,11, 
					20*mm,0, 
					30*mm,11, 
					40*mm,0, 
					50*mm,11, 
					60*mm,0, 
					70*mm,11,
					80*mm,0,
					90*mm,11,
					], strokeColor=xdexcolor3, strokeWidth=0.1*mm)
				d.add(l)
				story.append(d)
			else:
				para = Paragraph(line, style=stylesheet['comment'])
				story = addSuperSimpleTable(para, story)
		elif '<df' in line:
			para = Paragraph(line, style=stylesheet['df'], bulletText='<>')
			story.append(para)
		elif '╯' in line:
			para = Paragraph(line, style=stylesheet['mono'])
			story.append(para)
		elif any(el in line for el in ['<table>', '<td>', '<th>', '<tr>']):
			print('table')
		else:
			para = Paragraph(line, style=custom_style)
			story.append(para)
				
	img_count = 0

	# ---
	# add backcover
	if addSectionCover == True:
		if 'introduction' in filename:
			story.append(NextPageTemplate('sectioncover'))
			story.append(PageBreak())
			story.append(PageBreak())
		elif 'backtraces' in title:
			story.append(NextPageTemplate('cover'))
		else:
			story.append(NextPageTemplate('sectioncover'))
			story.append(PageBreak())
			story.append(PageBreak())
	# ---
	# define pageframes
	fullpage = Frame(0*mm, 0*mm, 110*mm, 170*mm, showBoundary=0)
	sectioncoverframe = Frame(10*mm, 10*mm, 90*mm, 150*mm, showBoundary=0)
	mainframe = Frame(12.5*mm, 7.5*mm, 80*mm, 160*mm, showBoundary=0)
	column1 = Frame(5*mm, 5*mm, 50*mm, 162.5*mm, showBoundary=0)
	column2 = Frame(56*mm, 5*mm, 50*mm, 162.5*mm, showBoundary=0)
	ricaframe = Frame(5*mm, 5*mm, 90*mm, 162.5*mm, showBoundary=0)
	behukiframe = Frame(15*mm, 12.5*mm, 80*mm, 155*mm, showBoundary=0)
	behukiIMGframe = Frame(5*mm, 5*mm, 100*mm, 160*mm, showBoundary=0)
	scoresIMGframe = Frame(0*mm, -2.9*mm, 110*mm, 175*mm, showBoundary=0)
	colophonframe1 = Frame(2*mm, 5*mm, 36.5*mm, 162.5*mm, showBoundary=0)
	colophonframe2 = Frame(37*mm, 5*mm, 37*mm, 162.5*mm, showBoundary=0)
	colophonframe3 = Frame(72*mm, 5*mm, 36.5*mm, 162.5*mm, showBoundary=0)
	xdexIMGframe1 = Frame(5*mm, 5*mm, 100*mm, 75*mm, showBoundary=1)
	xdexIMGframe2 = Frame(5*mm, 85*mm, 100*mm, 75*mm, showBoundary=1)
	xdexIMGframe3 = Frame(25*mm, 85*mm, 50*mm, 75*mm, showBoundary=1)
	xdexcolumn1 = Frame(5*mm, 5*mm, 50*mm, 162.5*mm, showBoundary=0)
	xdexcolumn2 = Frame(55*mm, 5*mm, 50*mm, 162.5*mm, showBoundary=0)
	xdexSCORESframe = Frame(5*mm, 7.5*mm, 100*mm, 160*mm, showBoundary=0)
	xdexSCOREScolumn1 = Frame(5*mm, 85*mm, 90*mm, 80*mm, showBoundary=0)
	xdexSCOREScolumn2 = Frame(5*mm, 5*mm, 90*mm, 75*mm, showBoundary=0)
	# ---
	# define page templates
	if 'x-dexing' in filename:	
		sectioncover = PageTemplate(id="sectioncover", pagesize=pagesizes, frames=[sectioncoverframe], onPage=sectionCover)
	else:
		sectioncover = PageTemplate(id="sectioncover", pagesize=pagesizes, frames=[sectioncoverframe], onPage=sectionCover)
	cover = PageTemplate(id="cover", pagesize=pagesizes, frames=[fullpage], onPage=prepareTracesPages)
	main = PageTemplate(id="main", pagesize=pagesizes, frames=[mainframe])
	spell = PageTemplate(id="spell", pagesize=pagesizes, frames=[mainframe], onPage=makeSpellPages)
	rica = PageTemplate(id="rica", pagesize=pagesizes, frames=[ricaframe], onPage=makeRicaPages)
	behuki = PageTemplate(id="behuki", pagesize=pagesizes, frames=[behukiframe], onPage=addBehukiBackground, onPageEnd=prepareBehukiSection)	
	behukiIMG = PageTemplate(id="behukiIMG", pagesize=pagesizes, frames=[behukiIMGframe], onPage=prepareBehukiSection)
	scoresIMG = PageTemplate(id="scoresIMG", pagesize=pagesizes, frames=[scoresIMGframe])
	introcolumns = PageTemplate(id="introcolumns", pagesize=pagesizes, frames=[column1, column2], onPage=addBlackBackground)
	colophon = PageTemplate(id="colophon", pagesize=pagesizes, frames=[colophonframe1, colophonframe2, colophonframe3], onPage=addBlackBackground)
	timeline = PageTemplate(id="timeline", pagesize=pagesizes, frames=[fullpage])
	xdexcolumns = PageTemplate(id="xdexcolumns", pagesize=pagesizes, frames=[xdexcolumn1, xdexcolumn2], onPage=addGreyBackground)
	xdexScores = PageTemplate(id="xdexScores", pagesize=pagesizes, frames=[xdexSCORESframe], onPage=addGreyBackground, onPageEnd=addXdexScoresfooter)
	
	# ---
	# add page templates	
	if 'cover' in filename:
		pdf.addPageTemplates([cover])
	elif 'colophon' in filename:
		pdf.addPageTemplates([colophon])
	elif 'timeline' in filename:
		pdf.addPageTemplates([timeline])
	else:
		pdf.addPageTemplates([sectioncover, main, introcolumns, spell, behuki, behukiIMG, rica, scoresIMG, colophon, timeline, xdexcolumns, xdexScores])
	
	# ---
	# make the pdf
	pdf.build(story)

	print('... DONE! :)')
	print('... PDF created: {}'.format(pdffilename))
	print('---')
