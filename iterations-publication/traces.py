# -*- coding: UTF-8 -*-

from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.platypus import Paragraph
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics

import urllib.request
from datetime import date, timedelta
import re
import csv
from math import sin, pi

from variables import *
from stylesheet import *

# -------------

# FUNCTIONS USED IN MULTIPLE TRACES

# -------------

pagesizes = [110*mm, 170*mm]

def translateCoordinate(coordinate):
	newcoordinate = (65 * sin((pi/50) * (coordinate - 5))) + 90
	return newcoordinate

def addXdexScoresfooter(pdf, t1=None, t2=None, loose=None, color=None):
	pdf.setFont('unifont', 7)
	pagenumber = pdf.getPageNumber()

	# add legend
	if color != None:
		c, m, y, k = color[0], color[1], color[2], color[3]
	else:
		c, m, y, k = xdexcolor2[0], xdexcolor2[1], xdexcolor2[2], xdexcolor2[3]
	pdf.setFillColorCMYK(c,m,y,k)

	pdf.drawString(7.5*mm, 23*mm, 'Handles (H)')

	if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
		pdf.setFillColorCMYK(0,0,0,0.25)
		pdf.drawString(7.5*mm, 19.5*mm, '◷ = Time')
	if any(tool for tool in [t1, t2] if 'time' in tool):
		pdf.drawString(7.5*mm, 19.5*mm, '◷ = Time')
	
	if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
		pdf.setFillColorCMYK(0,0,0,0.25)
		pdf.drawString(7.5*mm, 16*mm, '▩ = We')
	if any(tool for tool in [t1, t2] if 'we' in tool):
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(7.5*mm, 16*mm, '▩ = We')

	if any(tool for tool in [t1, t2] if 'how' in tool):
		pdf.drawString(27*mm, 19.5*mm, '⁎ = How')
	if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
		pdf.setFillColorCMYK(0,0,0,0.25)
		pdf.drawString(27*mm, 19.5*mm, '⁎ = How')

	if any(tool for tool in [t1, t2] if 'transition' in tool):
		pdf.drawString(27*mm, 16*mm, '⧒ = Transition')
	if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
		pdf.setFillColorCMYK(0,0,0,0.25)
		pdf.drawString(27*mm, 16*mm, '⧒ = Transition')
	
	if 'handle' in loose:
		pdf.drawString(7.5*mm, 19.5*mm, '◷ = Time')
		pdf.drawString(7.5*mm, 16*mm, '▩ = We')
		pdf.drawString(27*mm, 19.5*mm, '⁎ = How')
		pdf.drawString(27*mm, 16*mm, '⧒ = Transition')

	pdf.setFillColorCMYK(c,m,y,k)
	pdf.drawString(55*mm, 23*mm, 'Forms (F)')

	if any(tool for tool in [t1, t2] if 'curve' in tool):
		c, m, y, k = xcurves[0], xcurves[1], xcurves[2], xcurves[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(55*mm, 19.5*mm, 'Curves')
	if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
		pdf.setFillColorCMYK(0,0,0,0.25)
		pdf.drawString(55*mm, 19.5*mm, 'Curves')
		
	if any(tool for tool in [t1, t2] if 'color' in tool):
		c, m, y, k = xcolors[0], xcolors[1], xcolors[2], xcolors[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(55*mm, 16*mm, 'Colors')
	if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
		pdf.setFillColorCMYK(0,0,0,0.25)
		pdf.drawString(55*mm, 16*mm, 'Colors')

	if any(tool for tool in [t1, t2] if 'text' in tool):
		if color != None:
			c, m, y, k = color[0], color[1], color[2], color[3]
		else:
			c, m, y, k = xtext[0], xtext[1], xtext[2], xtext[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(75*mm, 19.5*mm, 'Text')

	if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
		pdf.setFillColorCMYK(0,0,0,0.25)
		pdf.drawString(75*mm, 16*mm, 'Absences')	
	if any(tool for tool in [t1, t2] if 'absence' in tool):
		c, m, y, k = xabsences[0], xabsences[1], xabsences[2], xabsences[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(75*mm, 16*mm, 'Absences')
	
	
	if 'form' in loose:
		c, m, y, k = xcurves[0], xcurves[1], xcurves[2], xcurves[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(55*mm, 19.5*mm, 'Curves')
		c, m, y, k = xcolors[0], xcolors[1], xcolors[2], xcolors[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(55*mm, 16*mm, 'Colors')
		c, m, y, k = xtext[0], xtext[1], xtext[2], xtext[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(75*mm, 19.5*mm, 'Text')
		c, m, y, k = xabsences[0], xabsences[1], xabsences[2], xabsences[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.drawString(75*mm, 16*mm, 'Absences')

	# ---

	if color != None:
		c, m, y, k = color[0], color[1], color[2], color[3]
	else:
		c, m, y, k = xdexcolor2[0], xdexcolor2[1], xdexcolor2[2], xdexcolor2[3]	
	pdf.setFillColorCMYK(c,m,y,k)

	pdf.drawString(7.5*mm, 10.5*mm, 'Contributions (C)')

	if any(tool for tool in [t1, t2] if 'kym' in tool):
		pdf.drawString(7.5*mm, 7*mm, 'Kym Ward')

	if any(tool for tool in [t1, t2] if 'rica' in tool):
		pdf.drawString(7.5*mm, 3.5*mm, 'Rica Rickson')

	if any(tool for tool in [t1, t2] if 'spideralex' in tool):		
		pdf.drawString(28*mm, 7*mm, 'spideralex')

	if any(tool for tool in [t1, t2] if 'behuki' in tool):
		pdf.drawString(28*mm, 3.5*mm, 'Behuki')		

	if any(tool for tool in [t1, t2] if 'collective' in tool):
		pdf.drawString(45*mm, 7*mm, 'Collective Conditions')

	if any(tool for tool in [t1, t2] if 'common' in tool):
		pdf.drawString(45*mm, 3.5*mm, 'common ground')

	if 'contribution' in loose:
		if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
			pdf.setFillColorCMYK(0,0,0,0.25)
		pdf.drawString(7.5*mm, 7*mm, 'Kym Ward')
		pdf.drawString(7.5*mm, 3.5*mm, 'Rica Rickson')
		pdf.drawString(28*mm, 7*mm, 'spideralex')
		pdf.drawString(28*mm, 3.5*mm, 'Behuki')		
		pdf.drawString(45*mm, 7*mm, 'Collective Conditions')
		pdf.drawString(45*mm, 3.5*mm, 'common ground')
	
	pdf.setFillColorCMYK(c,m,y,k)
	if pagenumber == 1 and t2 == 'text' or pagenumber == 4 and t1 == 'absence':
		pdf.setFont('brazil', 7)
		pdf.drawCentredString(100*mm, 7*mm, 'x-dex')
		pdf.setFont('unifont', 7)
		pdf.drawCentredString(100*mm, 3.5*mm, 'inside!')
	else:
		pdf.setFont('brazil', 7)
		pdf.drawCentredString(95*mm, 7*mm, 'x-Dexing Traces')
		pdf.setFont('unifont', 7)
		pdf.drawCentredString(95*mm, 3.5*mm, '(Beta Version)')

# -------------

# FUNCTION FOR EACH TRACE

# -------------

def trace1(pdf, color=None):
	# Turn a quote into an anecdote

	quote = '“Iterations investigates the future of artistic collaboration in a digital context.”'
	anecdote = 'We all travelled to Barcelona to actively engage with Iterations, both as a project but also as a phenomenon. Jara made a nice comment: an “iteration” is a sort of repetition, but without the loop. It triggered multiple questions. How can we investigate the future together? What is needed to be iterating together? And what forms of artistic collaboration in a digital context do we want to explore?”'

	para_quote = Paragraph(quote, stylesheet['tracequote'])
	para_anecdote = Paragraph(anecdote, stylesheet['tracequote'])

	w, h = para_quote.wrap(90*mm, 85*mm)
	para_quote.drawOn(pdf, 10*mm, 140*mm)

	w, h = para_anecdote.wrap(90*mm, 85*mm)
	para_anecdote.drawOn(pdf, 10*mm, 42.5*mm)

	addXdexScoresfooter(pdf, t1='how', t2='transition', loose='form', color=color)

def trace2(pdf):	
	# What tools can we use to support each others? orange. 
	# What are the barriers to it? light blue.

	pdf.setFont('unifont', 10)

	tools = [
	    'being together',
	    'socio-technical protocols',
	    'ficción',
	    'a body that protects myself from the artworld outside',
		'wording and worlding',
	    'a spatial conception, that might be inconsequent, but therefore open for Interpretation and Improvisation',
	]
	barriers = [
	    'what and how you “take”',
	    'when do we really have time for collective digestion of the events happening?',
	    'power-<br />relations given by institutions',
	    'violencias <br />estructurales',
	    'It takes <br />time to work together!',
	    'Protocols have a tendency to focus energy on discursive processes, rather than building concrete skills and practices within groups and across participants.',
	    'the dominant Image of the authentic Composer, whose individual work is stylistically coherent.',
	]

	def makeWordBlobs(lists, these_colors, pdf):
		h = 17.5
		xx = 5
		yy = 40
		border = 5

		for n, listing in enumerate(lists):

			color = these_colors[n]
			c,m,y,k = color[0],color[1],color[2],color[3]

			for text in listing:

				w = len(text) * 0.7
				if xx + w > 100:
					xx = 5
					yy += h

				if w > 90:
					w = 95
				if w < 25:
					w = 25

				p = Paragraph(text, stylesheet['wordblocks'])
				pw, ph = p.wrap((w-2.5)*mm, h*mm)
				if ph > h:
					w += 5
					pw += 5
					pw, ph = p.wrap((w-2.5)*mm, h*mm)

				pdf.setFillColorCMYK(c, m, y, k)
				pdf.roundRect(xx*mm, yy*mm, w*mm, h*mm, border*mm, stroke=0, fill=1)
				p.drawOn(pdf, xx*mm, (yy+3)*mm)

				xx += w 

	orange = [0,0.5,0.75,0]
	lightblue = [0.5,0,0,0]

	lists = [barriers, tools]
	these_colors = [lightblue, orange]
		
	makeWordBlobs(lists, these_colors, pdf)
	addXdexScoresfooter(pdf, t1='how', t2='color', loose='contribution')

def trace3(pdf):
	# Copied an anecdote from Rica Rickson's text

	anecdote = 'I was cleaning my cradle this morning and I found this box full of memories. On the top of the box there was a bingo board full of english words: <br /><br />questioning – magic – empowerment – (un)selfishness – transformation – authorship – staying with the problem – misun-derstanding – improvisation – singularities– tension – taking care – agency – trust – porosity – open mindset – collective – collaborative – effort – joy – dis/agreement – respons-ability – needs – power-relations – desires'

	p = Paragraph(anecdote, stylesheet['trace'])

	w, h = p.wrap(90*mm, 170*mm)
	p.drawOn(pdf, 10*mm, 40*mm)

	addXdexScoresfooter(pdf, t1='transition', t2='text', loose='contribution')

def trace4_1(pdf, color=None):

	we1 = '''feminist infrastructures
	a group
	itself
	we
	our efforts
	infrastructure
	community
	feminism
	they
	a community
	its ills
	what it needs
	they
	patriarchy
	that criminal alliance of patriarchy and capitalism
	us
	a breathing space together
	the possible
	ecosystems
	narratives
	fictional characters
	calls for transformative collective action'''

	def makeWordBlobs(lists, these_colors, pdf):
		h = 17.5
		xx = 5
		yy = 147.5
		border = 5

		for n, listing in enumerate(lists):
			
			for text in listing:
				red = ['we', 'our efforts', 'what it needs', 'a community', 'ills']
				magenta = ['they', 'possible', 'frictions']
				orange = ['group','community','narratives', 'speculative fictions']
				darkeryellow = ['ecosystems','trasformative collective','infrastructure']
				blue = ['patriarchy', 'criminal', 'libertarian']
				
				if any(word for word in blue if word in text):
					color = these_colors[5]
				elif any(word for word in red if word in text):
					color = these_colors[4]
				elif any(word for word in magenta if word in text):
					color = these_colors[3]
				elif any(word for word in orange if word in text):
					color = these_colors[2]
				elif any(word for word in darkeryellow if word in text):
					color = these_colors[1]
				else:
					color = these_colors[0]

				c,m,y,k = color[0],color[1],color[2],color[3]

				w = len(text) * 0.75

				# if w > 95:
					# w = 95
				if w < 22.5:
					w = 22.5

				p = Paragraph(text, stylesheet['wordblocks'])
				pw, ph = p.wrap((w-2.5)*mm, h*mm)
				if ph > h:
					w += 5
					pw += 5
					pw, ph = p.wrap((w-2.5)*mm, h*mm)

				# max width for a split
				if xx + w > 105:
					xx = 5
					yy -= h

				pdf.setFillColorCMYK(c, m, y, k)
				pdf.roundRect(xx*mm, yy*mm, w*mm, h*mm, border*mm, stroke=0, fill=1)

				p.drawOn(pdf, xx*mm, (yy+3)*mm)

				xx += w 

	w1 = we1.split('\n')

	c0 = [0,0,1,0] # yellow
	c1 = [0,0.15,1,0] # darker yellow
	c2 = xabsences # orange
	c3 = xcolors # magenta
	c4 = [0,1,1,0] # red
	c5 = [0.75,0.5,0,0] # blue

	lists = [w1]
	these_colors = [c0, c1, c2, c3, c4, c5]
		
	makeWordBlobs(lists, these_colors, pdf)
	addXdexScoresfooter(pdf, t1='color', t2='we', loose='contribution', color=color)

def trace4_2(pdf, color=None):
	we2='''we
	frictions
	community/libertarian infrastructure
	feminist infrastructure
	we
	speculative fictions
	feminist infrastructure
	we
	some techniques for doing speculative fiction workshops together'''

	def makeWordBlobs(lists, colors, pdf):
		h = 17.5
		xx = 5
		yy = 147.5
		border = 5

		for n, listing in enumerate(lists):

			for text in listing:
				red = ['we', 'our efforts', 'what it needs', 'a community', 'ills']
				magenta = ['they', 'possible']
				orange = ['group','community','narratives', 'speculative fictions']
				darkeryellow = ['ecosystems','trasformative collective','infrastructure']
				blue = ['patriarchy','criminal','community/libertarian']
				
				if any(word for word in blue if word in text):
					color = colors[5]
				elif any(word for word in red if word in text):
					color = colors[4]
				elif any(word for word in magenta if word in text):
					color = colors[3]
				elif any(word for word in orange if word in text):
					color = colors[2]
				elif any(word for word in darkeryellow if word in text):
					color = colors[1]
				else:
					color = colors[0]

				c,m,y,k = color[0],color[1],color[2],color[3]

				w = len(text) * 0.75

				# if w > 95:
					# w = 95
				if w < 22.5:
					w = 22.5

				p = Paragraph(text, stylesheet['wordblocks'])
				pw, ph = p.wrap((w-2.5)*mm, h*mm)
				if ph > h:
					w += 5
					pw += 5
					pw, ph = p.wrap((w-2.5)*mm, h*mm)

				# max width for a split
				if xx + w > 105:
					xx = 5
					yy -= h

				pdf.setFillColorCMYK(c, m, y, k)
				pdf.roundRect(xx*mm, yy*mm, w*mm, h*mm, border*mm, stroke=0, fill=1)

				p.drawOn(pdf, xx*mm, (yy+3)*mm)

				xx += w 

	w2 = we2.split('\n')

	c0 = [0,0,1,0] # yellow
	c1 = [0,0.15,1,0] # darker yellow
	c2 = xabsences # orange
	c3 = xcolors # magenta
	c4 = [0,1,1,0] # red
	c5 = [0.75,0.5,0,0] # blue

	lists = [w2]
	colors = [c0, c1, c2, c3, c4, c5]
		
	makeWordBlobs(lists, colors, pdf)
	addXdexScoresfooter(pdf, t1='color', t2='we', loose='contribution', color=color)

def trace5(pdf):
	# Trace the agents that are referred to in the materi- als through prepositions (eg. under, with, without, in). Write them in [preposition, agent] pairs.

	prepositions1 = '''as codes, <br />
	of conduct, <br />
	of expertise, <br />
	into contact, <br />
	by work, <br />
	of collectives, <br />
	on ally-ship, <br />
	by ways, <br />
	within Culture, <br />
	of commitment, <br />
	that collectives, <br />
	in cultures, <br />
	of harassment, <br />
	in imagination, <br />
	of collectivities, <br />
	within collectives, <br />
	on intersection, <br />
	of practice, <br />
	against limits, <br />
	of
	'openness,<br />
	on
	processes,<br />
	than
	skills,<br />
	within
	groups,<br />
	across
	participants,<br />
	of
	collide,<br />
	with
	tango,<br />
	of
	concern,<br />
	with
	possibility,<br />
	of
	collectivity,<br />
	in
	way,<br />
	as
	form,<br />
	of
	togetherness,<br />
	for
	example,<br />
	for
	constellations,<br />
	with
	needs,<br />
	of
	forces,<br />
	as
	laws,<br />
	of
	victimization,<br />'''

	prepositions2 = '''with people,<br />
	through history,<br />
	of today,<br />
	of affiliation,<br />
	through cloaks,<br />
	of forms,<br />
	in rituals,<br />
	of spellcraft,<br />
	as glimpses,<br />
	of practices,<br />
	of share,<br />
	By converging,<br />
	of group,<br />
	by turns,<br />
	of amount,<br />
	of sharing,<br />
	of practices,<br />
	on communication,<br />
	on visibility,<br />
	of issues,<br />
	on correspondent,<br />
	within group,<br />
	on Air,<br />
	In
	style,<br />
	of
	storm,<br />
	in
	peace,<br />
	as
	practice,<br />
	in
	place,<br />
	at
	moments,<br />
	in
	response,<br />
	of
	festival,<br />
	of
	moments,<br />
	after
	ends,<br />
	as
	practitioners,<br />
	in
	practices,<br />
	at
	formation,<br />
	of
	universe,<br />
	from
	others,'''

	prepositions3 = '''by institutions,<br />
	whereas institution,<br />
	from market,<br />
	that matrix,<br />
	although presence,<br />
	for survival,<br />
	at time,<br />
	in environment,<br />
	against desires,<br />
	that [,<br />
	[ [,<br />
	if fragmenting,<br />
	in host,<br />
	by organisms,<br />
	of cells,<br />
	with ],<br />
	in state,<br />
	In box,<br />
	with words,<br />
	of email,<br />
	Within days,<br />
	upon ways,<br />
	of collaboration,
	,<br />
	from
	concepts,<br />
	for
	example,<br />
	between
	languages,<br />
	that
	name,<br />
	as
	author,<br />
	Of
	course,<br />
	In
	morning,<br />
	about
	identity,<br />
	in
	art,<br />
	into
	artworks,<br />
	about
	death,<br />
	of
	author,<br />
	by
	Roland,<br />
	that
	yesterday,<br />
	in
	content,<br />'''

	p = Paragraph(prepositions1, stylesheet['tracesmall'])
	w, h = p.wrap(33*mm, 150*mm)
	p.drawOn(pdf, 10*mm, 32.5*mm)

	p = Paragraph(prepositions2, stylesheet['tracesmall'])
	w, h = p.wrap(33*mm, 150*mm)
	p.drawOn(pdf, 42.5*mm, 32.5*mm)

	p = Paragraph(prepositions3, stylesheet['tracesmall'])
	w, h = p.wrap(33*mm, 150*mm)
	p.drawOn(pdf, 75*mm, 32.5*mm)

	addXdexScoresfooter(pdf, t1='we', t2='text', loose='contribution')

def trace6(pdf):
	# Look for question marks in multiple contributions.

	questions = '''
	doing no harm?
	tense?
	Shy?Melancholic?
	In a continuous hurry?
	events happening?
	if we can not stop?
	Who am I?
	what is its meaning?
	What is your name? How much is it?
	who will come?
	butwho are they?
	are you hearing me?
	all on?
	What was the question again?
	production?
	tomorrow?
	simply got carry by others?
	like the motor of the whole body?
	with anyone else?
	what if I don’t need thecalm of the Unity?
	me?
	am I?
	before the sky?
	---
	enslave?
	documentar?
	tocar?
	esto?
	adpatable?
	feminista?
	propio?
	inter-especies?
	---
	trust?
	memory?
	---
	together?
	ecological conditions?
	commit to care?
	most subversivelines of thought?
	But how to ap-proach this? 
	stopping to make and share pictures?
	with a similarattitude?
	leave space for others?
	where does the “house” begin and end? 
	add ageism?
	Why not consent forms? 
	needs a “non-commercial” license?
	Is this possible? Fun?'''

	questions = questions.replace('\n', '\n<br />')
	questions = questions.replace('?', ' ?')
	questions = questions.replace('---', '  ')

	p = Paragraph(questions, stylesheet['tracequestions'])
	w, h = p.wrap(100*mm, 170*mm)
	p.drawOn(pdf, 5*mm, 30*mm)

	addXdexScoresfooter(pdf, t1='how', t2='text', loose='contribution')


def trace7(pdf):		
	# Copied an anecdote from Rica Rickson's text
	
	anecdote = 'when do we really have time for collective digestion of the events happening'
	a = [c for c in anecdote]
	a.sort()
	anecdote = ''.join(a)

	p = Paragraph(anecdote, stylesheet['trace'])
	w, h = p.wrap(90*mm, 170*mm)
	p.drawOn(pdf, 10*mm, 150*mm)

	addXdexScoresfooter(pdf, t1='rica', t2='time', loose='form')

def trace8(pdf):
	img = 'images/brown-architectural-elements.png'
	pdf.drawImage(img, 0*mm, 30*mm, width=110*mm, height=150*mm, mask=[0,0,0,0.25,0,0], preserveAspectRatio=True)

	addXdexScoresfooter(pdf, t1='common', t2='absence', loose='handle')

def trace9(pdf):
	img = 'images/curves-infrastructure.png'
	pdf.drawImage(img, 0*mm, 0*mm, width=110*mm, height=170*mm, preserveAspectRatio=True)

	addXdexScoresfooter(pdf, t1='spideralex', t2='time', loose='form')

def trace10(pdf):	
	explicit_transitions = 'first, 2018, between, the next, that followed, emerged, contamination, will be, resulting, immediately, followed, a goal, after'

	transitions = explicit_transitions.split(',')

	pdf.setFont('unifont', 16)
	color = [1,1,1,1]
	c, m, y, k = color[0], color[1], color[2], color[3]
	pdf.setFillColorCMYK(c,m,y,k)
	color = xcurves
	c, m, y, k = color[0], color[1], color[2], color[3]
	pdf.setStrokeColorCMYK(c,m,y,k)

	xx = 10
	yy = 160

	x1 = 100
	y1 = 30
	x2 = 30
	y2 = 195
	x3 = 80
	y3 = 195
	x4 = 5
	y4 = 120

	n = 0
	for transition in transitions:
		transition = transition.strip()
		# yy = translateCoordinate(xx)
		pdf.drawString(xx*mm, yy*mm, transition)
		pdf.bezier(x1*mm, y1*mm, x2*mm, y2*mm, x3*mm, y3*mm, x4*mm, y4*mm)
		yy -= 10
		xx += 5
		n += 1

		if n % 2 == 0:
			x2 += 10
		else:
			x3 += 10

		y2 -= 10
		y3 -= 10

	addXdexScoresfooter(pdf, t1='curve', t2='transition', loose='contribution')

def trace11(pdf):	
	# Trace glossary items in all the contributions that engage with notions of temporality

	pdf.setFont('unifont', 6)

	temporalities = {
		'time': 75,
		'temporalities': 7,
		'when': 41,
		'during': 17,
		'while': 4,
		'future': 19,
		'past-ing': 1,
		'present-ing': 1,
		'past': 3,
		'repeated': 1,
		'remember': 5,
		'memory': 6,
		'projection': 4,
	}
	formula = '(writing time)=(futuring)+(past-ing)+(present-ing)'
	xx = 10
	yy = 155

	pdf.drawCentredString(10*mm, 149*mm, '(writing time)')
	pdf.drawCentredString(10*mm, 137*mm, '=')
	pdf.drawCentredString(10*mm, 125*mm, '(futuring)')
	pdf.drawCentredString(10*mm, 113*mm, '+')
	pdf.drawCentredString(10*mm, 101*mm, '(past-ing)')
	pdf.drawCentredString(10*mm, 89*mm, '+')
	pdf.drawCentredString(10*mm, 77*mm, '(present-ing)')

	for key, value in temporalities.items():
		for n in range(value):

			x1 = xx
			x4 = x1+3.5 # width of the curve
			y1 = yy+3 # position of 1rst bezier handles
			y2 = yy+10+(n/2) # position of 2nd bezier handles
			y4 = y1
			x2 = x1+5
			x3 = x4-5
			y3 = y2

			pdf.drawString(xx*mm, yy*mm, key)
			# pdf.bezier(x1*mm, y1*mm, x2*mm, y2*mm, x3*mm, y3*mm, x4*mm, y4*mm)

			xx += 5
			if xx % 100 == 0:
				xx = 10
				yy -= 12

	addXdexScoresfooter(pdf, t1='behuki', t2='time', loose='form')

def trace12(pdf, color=None):	
	# Trace glossary items in all the contributions that engage with notions of temporality

	glossary = {
		'Systematics': '<em>The study of the diversification of living forms, both past and present, and the relationships among living things through time.</em>',
		'Handle': '<em>A part of an object designed for holding, moving, or carrying the object easily.</em>',
		'Together': '<em>Into companionship or close association.</em>',
		'Temporality': '<em>Traditionally the linear progression of past, present, and future. However, some modern-century philosophers have interpreted temporality in ways other than this linear manner. In social sciences, temporality is also studied with respect to human’s perception of time and the social organization of time. The perception of time undergoes significant change in the three hundred years between the Middle Ages and Modernity.</em>',
		'Recursion': '<em>Recursion is the process a procedure goes through when one of the steps of the procedure involves invoking the procedure itself. A procedure that goes through recursion is said to be “recursive”. To understand recursion, one must recognize the distinction between a procedure and the running of a procedure. A procedure is a set of steps based on a set of rules, while the running of a procedure involves actually following the rules and performing the steps.</em>',
		'Sensibility': '<em>Sensibility refers to an acute perception of or responsiveness toward something, such as the emotions of another. This concept emerged closely associated with studies of sense perception as the means through which knowledge is gathered.</em>',
		'Pronoun': '<em>A word that substitutes for a noun or noun phrase. It is a particular case of a pro-form. An example of a pronoun is "their", which is both plural and singular. Subtypes include personal and possessive pronouns, reflexive and reciprocal pronouns, demonstrative pronouns, relative and interrogative pronouns, and indefinite pronouns.</em>',
		'Problematization': '<em>To consider the concrete or existential elements of those involved as challenges (problems) that invite the people involved to transform those situations. It is a method of defamiliarization of common sense. Problematization is a critical thinking and pedagogical dialogue or process and may be considered demythicisation. Rather than taking the common knowledge (myth) of a situation for granted, problematization poses that knowledge as a problem, allowing new viewpoints, consciousness, reflection, hope, and action to emerge.</em>',
		'Potential': '<em>Potential generally refers to a currently unrealized ability. The term is used in a wide variety of fields, from physics to the social sciences to indicate things that are in a state where they are able to change in ways ranging from the simple release of energy by objects to the realization of abilities in people.</em>',
		# 'Absence': '<em>Argument from ignorance:  is a fallacy in informal logic. It asserts that a proposition is true because it has not yet been proven false or a proposition is false because it has not yet been proven true. This represents a type of false dichotomy in that it excludes the possibility that there may have been an insufficient investigation to prove that the proposition is either true or false.</em>'
		}

	xx = 5
	yy = 145
	border = 5
	pdf.setFont('unifont', 6)
	n = 0

	for key, description in glossary.items():

		color = xtextback
		# color = [0.5,0.5,0.5,0.9]
		# color = [0.5,0.5,0.5,0.9]
		c,m,y,k = color[0],color[1],color[2],color[3]

		w = len(description) / 5.9
		print('w:', w)
		# if w > 95:
			# w = 95
		if w < 33.3:
			w = 33.3

		p = Paragraph(description, stylesheet['glossary'])
		pw, ph = p.wrap((w-2.5)*mm, 20*mm)
		
		ph = (ph/2) -2.5 # adjust ph
		if ph < 20:
			ph = 20

		# max width for a split
		if xx + w > 105:
			xx = 5
			yy -= ph

		print('xx/yy pw/ph:', xx, yy, round(pw), ph)

		pdf.setFillColorCMYK(c, m, y, k)
		pdf.roundRect(xx*mm, yy*mm, w*mm, ph*mm, border*mm, stroke=0, fill=1)

		if 'Handle' in key:
			pdf.drawCentredString(55*mm, 166.5*mm, key)
		elif 'Together' in key:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -107.5*mm, key)
			pdf.rotate(-90)
		elif 'Temporality' in key:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -81*mm, key)
			pdf.rotate(-90)
		elif 'Recursion' in key:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -3.5*mm, key)
			pdf.rotate(-90)
		elif 'Potential' in key:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -3.5*mm, key)
			pdf.rotate(-90)
		elif 'Pronoun' in key:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -106.5*mm, key)
			pdf.rotate(-90)
		elif 'Problematization' in key:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -99*mm, key)
			pdf.rotate(-90)
		elif n == 0:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -3.5*mm, key)
			pdf.rotate(-90)
		elif n % 2 == 0:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -107.5*mm, key)
			pdf.rotate(-90)
		else:
			pdf.rotate(90)
			pdf.drawCentredString((yy+(ph/2))*mm, -3.5*mm, key)
			pdf.rotate(-90)

		p.drawOn(pdf, xx*mm, (yy+3)*mm)

		xx += w 
		n += 1

	addXdexScoresfooter(pdf, t1='text', t2='', loose='', color=color)

def trace13(pdf):	
	# Trace material conditions of possibility. Mark the words that point at a notion of material conditions in the text, on one sheet of transparent paper.

	p = '''at bibliostrike
	welcome
	a place to make acts of solidarity
	from behind pay-walls
	walled
	isolated
	your own
	greatest
	hottest
	subversive
	resistance
	industries
	benefit
	liberated
	illegalized
	costs
	risks
	maintain
	infrastructures
	re-orientation
	distribution
	current
	optimise
	resource
	business model
	pivoting towards
	products
	capture
	sharing
	bubbly
	narrow
	fall under
	orphan
	boundaries
	technical justice
	using'''

	possibilities = p.split('\n')
	pp = ', '.join(possibilities)

	p = Paragraph(pp, stylesheet['trace'])

	w, h = p.wrap(90*mm, 170*mm)
	p.drawOn(pdf, 10*mm, 65*mm)

	addXdexScoresfooter(pdf, t1='how', t2='collective', loose='form')

def trace14(pdf):
	# Trace material conditions of possibility. Mark the words that point at a notion of material conditions in the text, on one sheet of transparent paper.

	tmp = '''210
	have time
	16
	digestion
	71
	cannot stop
	204
	transformation
	449
	would like
	4
	further continue
	49
	further reflect upon
	102
	to integrate with
	90
	continuously developing
	817
	I could
	2015
	transforming issues
	780
	switch on
	575
	getting axiety
	300
	would react
	515
	inhabit
	798
	transform monstrosity
	3007
	destabilizes memory
	1074
	crosswordpuzzle
	79
	misunderstood myself
	743
	to provisonally define/understand
	427
	maybe after that
	44
	sometimes
	85
	kind of fluctuation
	42
	sometimes I have
	36
	sometimes my legs want
	259
	back again
	1068
	shall reveal the forces
	70
	'''

	pdf.setFont('unifont', 8)
	color = [0,1,1,0]
	c, m, y, k = color[0], color[1], color[2], color[3]
	pdf.setFillColorCMYK(c,m,y,k)

	out = ''
	tmp = tmp.split('\n')

	for t in tmp:
		t = t.strip()
		
		num = None
		line = None

		if t.isdigit() == True:
			num = int(t)/2.75
		else:
			line = t

		if num:
			out += ('-' * int(num))
		if line:
			out += line

	p = Paragraph(out, stylesheet['tmporalties'])

	w, h = p.wrap(100*mm, 170*mm)
	p.drawOn(pdf, 5.75*mm, 30*mm)

	addXdexScoresfooter(pdf, t1='color', t2='time', loose='')

def trace15_1(pdf):	
	# Trace material conditions of possibility. Mark the words that point at a notion of material conditions in the text, on one sheet of transparent paper.

	money = '''- elemental Earth
	- fermentation
	- poetry
	- The convergence of two or more new worlds
	- in the words we use
	-  at the intersection of wording and worlding
	- making magic palpable
	- communities of affiliation
	- momentary occultism
	- a group
	- elemental Air
	'''

	pdf.setFont('unifont', 8)
	color = [1,0,1,0]
	c, m, y, k = color[0], color[1], color[2], color[3]
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.setStrokeColorCMYK(c,m,y,k)
	pdf.setLineWidth(3*mm)

	greendots = money.split('\n')
	xx = 15

	import random

	for dot in greendots:
		
		dot = dot.replace('- ', '').strip()

		xx += 25
		if xx > 100:
			xx = 15

		reach = [r for r in range(40, 165, 2)]
		yy = random.choice(reach)
		# yy = translateCoordinateCos(xx)

		pdf.circle(xx*mm, (yy+5)*mm, 2*mm)
		p = Paragraph(dot, stylesheet['tracesmall'])

		pw, ph = p.wrap(25*mm, 170*mm)
		p.drawOn(pdf, (xx-10)*mm, (yy-(ph/2))*mm)

	addXdexScoresfooter(pdf, t1='color', t2='how', loose='contributions')

def trace15_2(pdf):
	# Trace material conditions of possibility. Mark the words that point at a notion of material conditions in the text, on one sheet of transparent paper.
		
	money2= '''-  similar moments which will flow after we have stopped
	- mutuality
	- elemental water
	- transformation
	- residency at Hangar
	- a mechanism
	some uninvited organism
	different ways of collaboration
	open
	enormous creative power
	something about starting
	nispero fruits
	writing another story'''

	pdf.setFont('unifont', 8)
	color = [1,0,1,0]
	c, m, y, k = color[0], color[1], color[2], color[3]
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.setStrokeColorCMYK(c,m,y,k)
	pdf.setLineWidth(3*mm)

	greendots = money2.split('\n')
	xx = 15

	import random

	for dot in greendots:
		
		dot = dot.replace('- ', '').strip()

		xx += 25
		if xx > 100:
			xx = 15

		reach = [r for r in range(40, 165, 2)]
		yy = random.choice(reach)
		# yy = translateCoordinateCos(xx)

		pdf.circle(xx*mm, (yy+5)*mm, 2*mm)
		p = Paragraph(dot, stylesheet['tracesmall'])

		pw, ph = p.wrap(25*mm, 170*mm)
		p.drawOn(pdf, (xx-10)*mm, (yy-(ph/2))*mm)

	addXdexScoresfooter(pdf, t1='color', t2='how', loose='contributions')

def trace16(pdf):
	# Trace material conditions of possibility. Mark the words that point at a notion of material conditions in the text, on one sheet of transparent paper.

	absences1 = '''
	<em>1. What is absent</em>
	- Being together was the most important thing, but we do not remember the name was it “Minga”?
	- When do we really have time for collective digestion of the events happening?
	- What was the question again?
	- I just erased something that I found irrelevant for the reader, is it self censorship?
	- If there are Guidelines, they need to address responsibility and consequences in some way?
	- Or is there no way out of the dominant regime of exploited visibility?
	- List of hateful -isms: add ageism?
	- How do we create in this and the other planets an economy that puts in its center an ecology based on geological and inter-species solidarity?'''
	absences2 = '''
	<em>2. What might go missing, <br />&nbsp;&nbsp;&nbsp;&nbsp;or should not be absent</em>
	- Shall we keep all the coasters with drawings?
	- What should be kept, as evidence or reminder of someone or a collective process?
	- As it might lead to the end of the group itself, who cares to take a picture then?
	- What are the systematics of working together that need to be established before a process is even set in motion (like the boot system in Linux)?
	- What if the we can mean a bigger ‘we’ than the people we actually know?
	- What is it that makes certain works -and not others- materially possible?
	- Is this [to change perspectives and bring new orientations to trusted concepts] close to what the project envisioned that artists could do?
	- What was the question again?
	- Where is the reader, who is taking care of what and whom?
	- What different needs do ‘complex collectives’ have when they are the result of structural forces such as laws, racism, technology, wars, austerity, queerphobia and ecological conditions?'''
	absences3 = '''
	<em>3. What if something was absent</em>
	- Do I need a house?
	- Do I need a body?
	- But what happens then when we take that individuality out?
	- Does collective production exist without these difficulties?
	- What was the question again?'''

	absences1 = absences1.replace('- ', '').replace('\n', '\n<br />')
	absences2 = absences2.replace('- ', '').replace('\n', '\n<br />')
	absences3 = absences3.replace('- ', '').replace('\n', '\n<br />')

	p1 = Paragraph(absences1.replace('\n','<br />'), stylesheet['absences'])
	p2 = Paragraph(absences2.replace('\n','<br />'), stylesheet['absences'])
	p3 = Paragraph(absences3.replace('\n','<br />'), stylesheet['absences'])

	w, h = p1.wrap(50*mm, 170*mm)
	p1.drawOn(pdf, 5*mm, 77*mm)

	w, h = p2.wrap(50*mm, 170*mm)
	p2.drawOn(pdf, 55*mm, 41*mm)

	w, h = p3.wrap(50*mm, 170*mm)
	p3.drawOn(pdf, 5*mm, 28.8*mm)

	addXdexScoresfooter(pdf, t1='absence', t2='text', loose='contributions')

def trace17(pdf):
	img = 'images/empty-spaces-intro.png'
	pdf.drawImage(img, 0*mm, 0*mm, width=110*mm, height=170*mm, preserveAspectRatio=True)

	addXdexScoresfooter(pdf, t1='we', t2='curves', loose='contributions')

def trace18(pdf):	
	xx = 10
	yy = 155
	h = 4.5
	w = 45

	these_colors = '''green
	red
	red
	green
	green
	red
	red
	green
	green
	red
	red
	green
	green
	yellow
	yellow
	green
	green
	yellow
	yellow
	magenta
	magenta
	green
	green
	yellow
	yellow
	red
	red
	yellow
	yellow
	purple
	purple
	red
	red
	yellow
	yellow
	red
	red
	green
	green
	red
	red
	cyan
	cyan
	magenta
	magenta
	cyan
	cyan
	magenta
	magenta
	cyan
	cyan
	red
	red
	purple'''

	for color in these_colors.split('\n'):
		if 'red' in color:
			color = [0,0.95,1,0]
		elif 'cyan' in color:
			color = [0.75,0,0,0]
		elif 'magenta' in color:
			color = [0,0.45,0,0]
		elif 'purple' in color:
			color = [0.5,1,0,0]
		elif 'yellow' in color:
			color = [0,0,1,0]
		elif 'green' in color:
			color = [0.75,0,1,0]

		if xx + w > 110:
			xx = 10
			yy -=h

		c, m, y, k = color[0], color[1], color[2], color[3]
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.rect(xx*mm, yy*mm, w*mm, h*mm, fill=1, stroke=0)

		xx += w

	addXdexScoresfooter(pdf, t1='rica', t2='transition', loose='form')

def trace19(pdf):	
	color1 = 39
	color2 = 37
	color3 = 29
	color4 = 22
	color5 = 16
	color6 = 32
	color7 = 19

	xx = 5
	yy = 160
	h = 5
	w = 7

	c1 = [0.0,0.45,0.0,0.0]
	c2 = [0.0,0.0,1.0,0.0]
	c3 = [0.0,0.7,0.69,0.0]
	c4 = [0.15,0.2,0.0,0.02]
	c5 = [0.9,0.0,0.9,0.0]
	c6 = [0.75,0.0,0.0,0.0]
	c7 = [0.0,0.35,1.0,0.0]

	these_colors = [color1, color2, color3, color4, color5, color6, color7]
	cs = [c1, c2, c3, c4, c5, c6, c7]

	for n, c in enumerate(these_colors):
		for i in range(c):
			color = cs[n]

			w += (i^2)/9

			if xx + w > 105:
				xx = 5+(n*5)
				w = 6
				yy -=h

			c, m, y, k = color[0], color[1], color[2], color[3]
			pdf.setFillColorCMYK(c,m,y,k)
			pdf.ellipse(xx*mm, yy*mm, (xx+w)*mm, (yy+h)*mm, fill=1, stroke=0)


			xx += w

	addXdexScoresfooter(pdf, t1='rica', t2='color', loose='')