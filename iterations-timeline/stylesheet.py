from reportlab.lib.styles import ParagraphStyle, StyleSheet1
from reportlab.lib.units import mm

from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFont

registerFont(TTFont('unifont', './assets/unifont-11.0.03.ttf'))

stylesheet = StyleSheet1()

stylesheet.add(ParagraphStyle(name='timeline',
						fontName='unifont',
						fontSize=6,
						leading=7.6,
						textColor=[0,0,0,0],
						hyphenationMinWordLength=5)
					)
