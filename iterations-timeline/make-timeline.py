# -*- coding: UTF-8 -*-

import os

from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics

from functions import *
from variables import *

pdfmetrics.registerFont(TTFont('concrete-reg', 'assets/cmunorm.ttf'))
pdfmetrics.registerFont(TTFont('concrete-i', 'assets/cmunoti.ttf'))
pdfmetrics.registerFont(TTFont('unifont', 'assets/unifont-11.0.03.ttf'))
pdfmetrics.registerFont(TTFont('brazil', 'assets/Brazil-Regular-extraglyphs.ttf'))

# ---

pagesizes = [440*mm, 170*mm]
title = 'iterations-timeline'
pdffilename = title+'.pdf'
authors = 'many'

pdf = canvas.Canvas(pdffilename)
pdf.setPageSize(pagesizes) 
pdf.setTitle(title)
pdf.setAuthor(authors)
pdf.setFont('concrete-reg', 8)

# ---

# background
pdf.setFillColorCMYK(0.75,0.75,0.75,1) # black
pdf.rect(0*mm, 0*mm, pagesizes[0]*mm, pagesizes[1]*mm, fill=1, stroke=0)

addActivities_triggers(pdf)
addOnlineMeetings_triggers(pdf)

addActivities(pdf)
addOnlineMeetings(pdf)

# addImages(pdf)
addTitle(pdf)

addLegenda(pdf)
addTimeIndicator(pdf)

# ---

pdf.showPage()
pdf.save()