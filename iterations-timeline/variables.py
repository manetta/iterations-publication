from reportlab.lib.units import mm

# blue/purple
xcurves = (1, 0.15, 0, 0)
xcurvesback = (0.5, 0, 0, 0)
# ccurves = (0.5, 1, 0, 0)
# ccurvesback = (0.25, 0.5, 0, 0)

# pink
xcolors = (0, 0.7, 0, 0)
xcolorsback = (0, 0.4, 0, 0)

# green
xtext = (0.75, 0, 1, 0)
xtextback = (0.5, 0, 0.5, 0)

# orange
xabsences = (0, 0.45, 1, 0)
xabsencesback = (0, 0.25, 0.5, 0)


# xdexcolor1 = (1, 0, 1, 0) # inside cover background
xdexcolor1 = xtext # inside cover background
xdexcolor2 = (0, 0, 0, 1) # textcolor
# xdexcolor3 = (0, 0.5, 0, 0) # scores text
xdexcolor3 = xcolors  # ?
xdexcolor4 = (0, 0, 0, 0.1) # ? backcover title
