# -*- coding: UTF-8 -*-

from reportlab.lib.units import mm

import urllib.request
from datetime import date, timedelta
import re
import csv
from reportlab.platypus import Paragraph

from variables import *
from stylesheet import *

# ---------------
# TIMELINE STARTS
# ---------------

startdate = date(int(2017), int(10), int(23))
enddate = date(int(2020), int(5), int(19))
totaldays = (enddate - startdate).days
daywidth = 400 / totaldays # width of the canvas in mm / total nr of days in the project
print('... total nr of days:', totaldays)
print('... daywidth:', daywidth)

from math import sin, pi
import random 

def translateCoordinate(coordinate):
	newcoordinate = (20 * sin((pi/100) * (coordinate - 5))) + 95
	return newcoordinate

def transformIntoDateObject(moment):
	values = moment.split('-')
	day = values[0]
	month = values[1]
	year = values[2]
	dateobject = date(int(year), int(month), int(day))
	return dateobject

# def addImage(date, filename, pdf, move='plus'):
# 	between_moment_and_start = (date - startdate).days
# 	position = between_moment_and_start * daywidth

# 	if 'iterations-1' in filename:
# 		xx = position + 12.5
# 	else:
# 		xx = position - 10
# 	yy = translateCoordinate(xx)
# 	w = 25
# 	h = 25

# 	if move == 'plus':
# 		yy += 15
# 	elif move == 'mediumplus':
# 		yy += 25
# 	elif move == 'extraplus':
# 		yy += 35
# 	elif move == 'extraextraplus':
# 		yy += 40
# 	elif move == 'extramin':
# 		yy -= 35
# 	elif move == 'extramediummin':
# 		yy -= 55
# 	elif move == 'extraextramin':
# 		yy -= 75
# 	elif move == 'min':
# 		yy -= 40

# 	pdf.drawImage(filename, xx*mm, (yy-(w/2))*mm, width=w*mm, height=h*mm, mask=[0,0,100,255,0,0], preserveAspectRatio=True)

# def addImages(pdf):

# 	csvfile = open('timeline-activities-formatted-years-with-presences.csv', newline='') # activities
# 	activities = csv.reader(csvfile, delimiter=',', quotechar='"')

# 	# ---

# 	for linenumber, activity in enumerate(activities): 
# 		momentstartdate = activity[2]
# 		momentenddate = activity[3]

# 		if momentstartdate != '' and not 'xx' in momentstartdate and not 'start date' in momentstartdate:
# 			momentstartdate = transformIntoDateObject(momentstartdate)
# 			filename = ''
# 			move = ''
# 			if linenumber == 4:
# 				filename = 'assets/img/iterations-1.png'
# 				# move = 'min'
# 			if linenumber == 6:
# 				filename = 'assets/img/iterations-2.png'
# 				move = 'extramediummin'
# 			if linenumber == 18:
# 				filename = 'assets/img/iterations-3.png'
# 				move = 'extramin'
# 			if linenumber == 34:
# 				filename = 'assets/img/iterations-5-bozar-june.png' # red dots
# 				move = 'extraextramin'
# 			if linenumber == 38:
# 				filename = 'assets/img/iterations-5-bozar-nov.png' # etherpad paper
# 				move = 'extramin'
# 			if linenumber == 25:
# 				filename = 'assets/img/esc-iterationen-iv.png' # lines
# 				move = 'extramin'
# 			if linenumber == 31:
# 				filename = 'assets/img/esc-iterationen-4-2.png' # table
# 				move = 'extramediummin'
# 			if linenumber == 29:
# 				filename = 'assets/img/esc-iterationen-4.png' # plate
# 				move = 'extramin'
# 			if linenumber == 39:
# 				filename = 'assets/img/iterations-5-operation-exploitation.png' # stairs
# 				move = 'extramin'
# 			if linenumber == 43:
# 				filename = 'assets/img/iterations-5-collective-conditions-2.png' # sponge
# 				move = 'extramediummin'
# 				# addImage(momentstartdate, filename, pdf, move=move)
# 				# filename = 'assets/img/iterations-5-collective-conditions.png' # bucket
# 				# move = 'extramin'
# 			if linenumber == 47:
# 				filename = 'assets/img/Constant_v_may-2020.png'
# 				move = 'extramin'

# 			if filename:
# 				if move == '':
# 					move = 'extramin'
# 				addImage(momentstartdate, filename, pdf, move=move)

def addTriggerPattern(startxx, startyy, steps, color, pdf):
	# rectangles
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	w = 2
	h = 3
	xx = startxx
	yy = startyy

	# put the yy on a grid
	yy = round(yy)
	while yy % 3 != 0:
		yy += 1

	for n in range(1, steps, 1):
		yy_down = yy
		yy_up = yy

		# adjust the width of the shapes slowly
		w -= 0.2
		if w < 0:
			w = 0.1

		# adjust the colors slowly
		c -= 0.1
		m -= 0.1
		y -= 0.1
		k += 0.1
		if c < 0:
			c = 0
		if m < 0:
			m = 0
		if y < 0:
			y = 0
		if k < 0:
			k = 0
		# c,m,y,k = 0,0,0,0 # white
		# c,m,y,k = 1,1,1,1 # black
		pdf.setFillColorCMYK(c,m,y,k)
		
		for _ in range(1, n, 1):
			xx += 1
			pdf.rect(xx*mm, yy_down*mm, w*mm, h*mm, fill=1, stroke=0)
			pdf.rect(xx*mm, yy_up*mm, w*mm, h*mm, fill=1, stroke=0)
			# yy_down -= 3
			# yy_up += 3

			# adjust yy position slightly:		
			reach = [reach for reach in range(0, 9, 3)]
			yy_down -= random.choice(reach)
			yy_up += random.choice(reach)

def addTravelPath(xx, yy, rx, ry, color, pdf):


	w = 1 # width of shape
	h = 3 # height of shape
	xx += 0 # repositioning of departure point

	yy = 20 # repositioning of departure point

	if color == xtext:
		yy -= 2.5 # repositioning for the online meetings markers
	elif color == xcurves:
		yy += 2.5 # repositioning for the activities markers

	if xx > 380 and color == xcurves: # only add travelpaths before COVID-19 time
		print('... no travels due to COVID-19')
		color = xtext
	# else:
		# print('... xx/rx yy/ry:', xx, rx, yy, ry)

	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setStrokeColorCMYK(c,m,y,k)
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.setLineWidth(0.3*mm)
	pdf.setLineCap(1)

	# # maze paths
	# p = pdf.beginPath()
	# p.moveTo(xx*mm, yy*mm)
	# for _ in range(10):
	# # while xx != rx:
	# 	options = [-1, 1, -2, 2, -3, 3]
	# 	xx += random.choice(options)
	# 	p.lineTo(xx*mm, yy*mm)
	# 	options = [-3, 3, -6, 6, -9, 0]
	# 	yy += random.choice(options)
	# 	p.lineTo(xx*mm, yy*mm)
	# pdf.drawPath(p)

	# # maze paths with direction
	# p = pdf.beginPath()
	# p.moveTo(xx*mm, yy*mm)
	# mx = rx - xx
	# my = round((ry - yy) / 3)
	# # print('... mx/my:      ', mx, my)

	# x_reroutes_pos = [1, -3, 1, 3]

	# # xx
	# if mx < 0:
	# 	mx = mx * -1
	# 	negative = True
	# else:
	# 	negative = False
	# for step in range(0, mx, 1):
	# 	# print('xxx step:', step)
	# 	if negative == True:
	# 		xx -= 1
	# 	else:
	# 		xx += 1
	# 	p.lineTo(xx*mm, yy*mm)

	# # yy
	# if my < 0:
	# 	my = my * -1
	# 	negative = True
	# 	yy += 3
	# else:
	# 	negative = False
	# for step in range(0, my, 1):
	# 	# print('yyy step:', step)
	# 	if negative == True:
	# 		yy -= 3
	# 	else:
	# 		yy += 3
	# 	p.lineTo(xx*mm, yy*mm)
	# pdf.drawPath(p)

	# bezier paths
	xx2 = xx + 40
	yy2 = translateCoordinate(xx2)
	xx3 = xx - 10
	yy3 = translateCoordinate(xx3)
	xx4 = rx 
	yy4 = ry 
	pdf.bezier((xx+17.5)*mm, (yy+5)*mm, xx2*mm, yy2*mm, xx3*mm, yy3*mm, xx4*mm, yy4*mm)

def addPresences(date, presences, color, listing, pdf):
	between_moment_and_start = (date - startdate).days
	position = between_moment_and_start * daywidth
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.setStrokeColorCMYK(c,m,y,k)

	for p in range(presences):	
		x_reach = [r for r in range(-40, 40, 5)]
		y_reach = [r for r in range(-21, 21, 3)]
		rx = random.choice(x_reach)
		ry = random.choice(y_reach)
		xx = position-10
		yy = translateCoordinate(xx)

		xx = round(xx)
		yy = round(yy)
		# put the yy on a grid
		while yy % 3 != 0:
			yy += 1

		if xx < 25:
			xx += 12.5
			rx += 12.5

		# trigger patterns + travel paths
		if listing == 'activities':
			tmp_color = xcurves
			# color = [0,0,1,0]
			addTravelPath(xx, yy, (xx+rx), (yy+ry), tmp_color, pdf)
		elif listing == 'onlinemeetings':
			tmp_color = xtext
			# color = [0,0,1,0]
			addTravelPath(xx, yy, (xx+rx), (yy+ry), tmp_color, pdf)

		# add triggers of affection of people/presences
		addTriggerPattern((xx+rx), (yy+ry), 15, color, pdf)

		# presence-symbols for:
		# online-meetings
		# activities
		c,m,y,k = color[0], color[1], color[2], color[3] 
		pdf.setFillColorCMYK(c,m,y,k)
		pdf.setFont('unifont', 10)
		pdf.drawCentredString((xx+rx)*mm, (yy+ry+0.5)*mm, '▩')

def addText(date, text, color, linenumber, pdf):
	between_moment_and_start = (date - startdate).days
	position = between_moment_and_start * daywidth

	if linenumber == 4: # iterations #1
		xx = 22.5+position
	else:
		xx = 5+position

	if linenumber % 2 == 0:
		if linenumber == 4: # Iterations #1
			yy = 130
		elif linenumber == 6: # Iterations #2: Trasformatorio
			yy = 160
		elif linenumber == 18: # Iterations #3: Residency
			yy = 155
		elif linenumber == 36: # Constant V: Collaborative Guidelines
			yy = 150
		elif linenumber == 34: # Iterations #5
			yy = 155
		elif linenumber == 38: # Iterations #5
			yy = 155
		elif linenumber == 40: # Iterations #5, talks and performance
			yy = 145
		elif linenumber == 48: # Next iterations
			yy = 135
		else:
			yy = 135
	else:
		if linenumber == 11: # esc lab 2 distant skies
			yy = 155
		elif linenumber == 13: # esc lab
			yy = 150
		elif linenumber == 17: # Handover meeting
			yy = 160
		elif linenumber == 23: # Handover meeting
			yy = 165
		elif linenumber == 25: # esclab I: ITERATIONEN IV
			yy = 155
		elif linenumber == 29: # iterations #4
			yy = 150
		elif linenumber == 31: # Iterations #4: Phase II (Net of Iterations)
			yy = 160
		elif linenumber == 39: # Iterations #5: Operating / Exploitation
			yy = 160
		elif linenumber == 41: # Update meeting
			yy = 155
		elif linenumber == 47: # Constant V
			yy = 130
		elif linenumber == 49: # wrap up meeting
			yy = 125
		else:
			yy = 165

	pdf.setFont('unifont', 8)
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.drawCentredString(xx*mm, yy*mm, text)

def addSingleDayActivity(date, listing, color, pdf):
	between_moment_and_start = (date - startdate).days
	position = between_moment_and_start * daywidth
	print('>>> adding {} gathering mark!'.format(listing))
	
	xx = 5+position
	if xx < 20:
		xx = 17.5 # reposition the first activity

	yy = 20

	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)

	symbol = '⁎'

	if listing == 'onlinemeetings':		
		yy -= 2.5 # vertical displacement
	elif listing == 'activities':
		yy += 2.5 # vertical displacement
	elif listing == 'handover':
		symbol = '⧒'
		yy += 2

	pdf.setFont('unifont', 25)
	pdf.drawString(xx*mm, yy*mm, symbol)

def addSingleDayActivity_triggers(date, listing, color, pdf):
	between_moment_and_start = (date - startdate).days
	position = between_moment_and_start * daywidth

	xx = 5+position
	if xx < 20:
		xx = 17.5 # reposition the first activity

	yy = 20

	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)

	# extra trigger pattern at the bottom
	# visualising infrastructure affections
	if listing == 'onlinemeetings':		
		yy -= 2.5 # vertical displacement
	elif listing == 'activities':
		yy += 2.5 # vertical displacement

	addTriggerPattern((xx+5), yy, 10, color, pdf)

def addOnlineMeetings_triggers(pdf):
	url = 'https://pad.constantvzw.org/p/iterations-publication-timeline/export/txt' # online-meetings
	data = urllib.request.urlopen(url)
	onlinemeetings = data.read().decode('utf-8').split('\n')
	onlinemeetings.sort()
	listing = 'onlinemeetings'

	# ---
	# calculate the average presence of online meetings
	presencesall = 0
	nrofonlinemeetings = 0
	# ---

	for moment in onlinemeetings: 
		if moment and not '§' in moment:
			data = moment.split(',')
			moment = data[0]
			presences = int(data[1])
			thisdate = transformIntoDateObject(moment)
			
			color = [0,0,1,0] # yellow

			if presences == 0:
				presences = 3 # this is a pre-calculated average
			addPresences(thisdate, presences, color, listing, pdf)

			# add gathering triggers (caused by server used) at the bottom
			tmp_color = xtext # green for server triggers
			addSingleDayActivity_triggers(thisdate, listing, tmp_color, pdf)
			
			presencesall += presences
			nrofonlinemeetings += 1
	
	average_presences_during_online_meetings = presencesall / nrofonlinemeetings
	# print('... average presences during online meetings:', average_presences_during_online_meetings)
	# 3.3125

def addOnlineMeetings(pdf):
	url = 'https://pad.constantvzw.org/p/iterations-publication-timeline/export/txt' # online-meetings
	data = urllib.request.urlopen(url)
	onlinemeetings = data.read().decode('utf-8').split('\n')
	onlinemeetings.sort()
	listing = 'onlinemeetings'

	# seperate loop to put to markers on top
	for moment in onlinemeetings: 
		if moment and not '§' in moment:
			data = moment.split(',')
			moment = data[0]
			thisdate = transformIntoDateObject(moment)
			
			# add gathering marker on the bottom
			color = [0,0,1,0] # yellow
			addSingleDayActivity(thisdate, listing, color, pdf)

def addActivities_triggers(pdf):
	csvfile = open('timeline-activities-formatted-years-with-presences.csv', newline='') # activities
	activities = csv.reader(csvfile, delimiter=',', quotechar='"')

	# ---

	listing = 'activities'

	for linenumber, activity in enumerate(activities): 
		momentstartdate = activity[2]
		momentenddate = activity[3]
		if momentstartdate != '' and not 'xx' in momentstartdate and not 'start date' in momentstartdate:
			momentstartdate = transformIntoDateObject(momentstartdate)
			description = activity[5]
			print('... activity: {} ({})'.format(description, activity[2]))

			if activity[6]:
				presences = int(activity[6])
			else:
				presences = 0
			
			color = xcolors

			# add presence markers (people that attended the activity)
			addPresences(momentstartdate, presences, color, listing, pdf)
			
			# add activity triggers (caused by travels) at the bottom 
			tmp_color = xcurves # blue for the travel triggers
			addSingleDayActivity_triggers(momentstartdate, listing, tmp_color, pdf)

def addActivities(pdf):
	# seperate loop to put to markers on top
	csvfile = open('timeline-activities-formatted-years-with-presences.csv', newline='') # activities
	activities = csv.reader(csvfile, delimiter=',', quotechar='"')
	for linenumber, activity in enumerate(activities): 
		momentstartdate = activity[2]
		momentenddate = activity[3]
		if momentstartdate != '' and not 'xx' in momentstartdate and not 'start date' in momentstartdate:
			momentstartdate = transformIntoDateObject(momentstartdate)
			description = activity[5]

			# mark handover meetings with transition symbol
			if 'hand' in description.lower():
				listing = 'handover'
				color = [0,1,1,0]
			else:
				listing = 'activities'
				color = xcolors # pink

			# add gathering marker on the bottom
			addSingleDayActivity(momentstartdate, listing, color, pdf)
				
			# add activity description
			addText(momentstartdate, description, [0,0,0,0], linenumber, pdf)

def addLegenda(pdf):
	pdf.setFont('unifont', 6)

	left = 388
	bottom = 148

	color = [0,0,0,0.5] # grey
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setStrokeColorCMYK(c,m,y,k)
	pdf.setLineWidth(0.05*mm)
	# pdf.rect((left-3)*mm, (bottom-7)*mm, 53*mm, 27*mm)

	color = [0,0,0,0] # white
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)

	pdf.drawString(left*mm, (bottom+16)*mm, '   = Physical gatherings')
	pdf.drawString(left*mm, (bottom+12)*mm, '   = Online gatherings')
	pdf.drawString(left*mm, (bottom+8)*mm, '   = Presences')
	pdf.drawString(left*mm, (bottom+4)*mm, '   = Server connection')
	pdf.drawString(left*mm, (bottom+0)*mm, '   = Travel displacements')
	pdf.drawString(left*mm, (bottom-4)*mm, '   = Handover meeting')
	
	color = xcolors
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.rect((left+30.5)*mm, (bottom+15)*mm, 1*mm, 3*mm, fill=1, stroke=0)
	
	color = [0,0,1,0]
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.rect((left+29)*mm, (bottom+15)*mm, 1*mm, 3*mm, fill=1, stroke=0)
	
	color = xcurves
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.rect((left+30.5)*mm, (bottom+11.5)*mm, 1*mm, 3*mm, fill=1, stroke=0)
	
	color = xtext
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.rect((left+29)*mm, (bottom+11.5)*mm, 1*mm, 3*mm, fill=1, stroke=0)
	
	color = [0,0,0,0] # white
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	echo = 'implications, consequences, aftermath, potential, resonances, echo, affects and effects'
	p = Paragraph(echo, stylesheet['timeline'])
	p.wrap(16*mm, 20*mm)
	p.drawOn(pdf, (left+35)*mm, (bottom-0.5)*mm)
	pdf.drawString((left+33)*mm, (bottom+16)*mm, '=')

	pdf.setFont('unifont', 8)
	pdf.drawString((left+29.5)*mm, (bottom-4)*mm, '◷')
	pdf.setFont('unifont', 6)
	pdf.drawString((left+30.5)*mm, (bottom-4)*mm, '  = Time')
	
	pdf.setFont('unifont', 10)
	color = xcolors
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.drawString((left)*mm, (bottom+16)*mm, '⁎')
	
	color = [0,0,1,0]
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.drawString((left)*mm, (bottom+12)*mm, '⁎')
	
	pdf.setFont('unifont', 8)
	color = xcolors
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.drawString((left-2)*mm, (bottom+8)*mm, '▩')
	color = [0,0,1,0]
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.drawString(left*mm, (bottom+8)*mm, '▩')
	
	# Displacements
	color = xtext
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setStrokeColorCMYK(c,m,y,k)
	pdf.bezier((left-0.5)*mm, (bottom+4.5)*mm, left*mm, (bottom+7.5)*mm, (left+1.5)*mm, (bottom+6.5)*mm, (left+1.5)*mm, (bottom+4)*mm)

	color = xcurves
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setStrokeColorCMYK(c,m,y,k)
	pdf.bezier((left-0.5)*mm, (bottom-1)*mm, left*mm, (bottom+4.5)*mm, (left+1.5)*mm, (bottom+2)*mm, (left+1.5)*mm, (bottom+1)*mm)

	pdf.setFont('unifont', 10)
	color = [0,1,1,0]
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)
	pdf.drawString((left)*mm, (bottom-4.5)*mm, '⧒')


def addTitle(pdf):
	timeline =  'Línia del temps-Zeitleiste-Timeline-Línea temporal-Chronologie-Tijdslijn'
	timelines = timeline.split('-')
	iteration = 'Iteracions-Iterationen-Iterations-Iteraciones-Itérations-Iteraties'
	iterations = iteration.split('-')

	color = [0,0,0,0] # white
	c,m,y,k = color[0], color[1], color[2], color[3] 
	pdf.setFillColorCMYK(c,m,y,k)

	left = 1
	bottom = 164

	textobject = pdf.beginText()
	textobject.setTextOrigin(left*mm, bottom*mm)
	textobject.setFont('brazil', 12)
	for n, line in enumerate(timelines):
		textobject.textLine('{} {}'.format((n*' '), line))
	pdf.drawText(textobject)
	
	textobject = pdf.beginText()
	textobject.setTextOrigin((left+32.5)*mm, bottom*mm)
	textobject.setFont('brazil', 12)
	for n, line in enumerate(iterations):
		textobject.textLine('{} {}'.format((n*' '), line))
	pdf.drawText(textobject)

def addTimeIndicator(pdf):
	pdf.setFont('unifont', 8)
	pdf.setLineCap(1)
	linewidth = 0.5
	lineheight = 5
	pdf.setLineWidth(linewidth*mm)

	color = [0,0,0,0] # white
	c,m,y,k = color[0], color[1], color[2], color[3] 

	pdf.setStrokeColorCMYK(c,m,y,k)
	pdf.setFillColorCMYK(c,m,y,k)

	left = 0
	bottom = 67.5 # curve position, this bottom is substracted from the 0 line at 85
	indication_y = 7 # time indicator position, this is the height of the time indicators
	
	# start and end
	start = 5
	end = 410		
	duration = end - start
	pdf.drawCentredString(24*mm, (indication_y-(lineheight/2))*mm, 'Oct 2017')
	pdf.line((start+left)*mm, (indication_y-lineheight)*mm, (start+left)*mm, indication_y*mm)
	pdf.drawCentredString(425*mm, (indication_y-(lineheight/2))*mm, 'May 2020')
	pdf.line((end+left)*mm, (indication_y-lineheight)*mm, 410*mm, indication_y*mm)

	print('... nr of days:', duration / daywidth)
	
	# first piece of line
	start = 46
	end = 88
	duration = end - start
	days = round(duration / daywidth)
	pdf.line((start+left)*mm, (indication_y-lineheight)*mm, (start+left)*mm, indication_y*mm)
	pdf.drawCentredString((start + (duration/2))*mm, (indication_y-(lineheight/2))*mm, '◷ {} days'.format(days))
	pdf.line((end+left)*mm, (indication_y-lineheight)*mm, (end+left)*mm, indication_y*mm)
	
	# 1.5 piece of line
	start = 88
	end = 150
	duration = end - start
	days = round(duration / daywidth)
	hours = round(days * 24)
	pdf.line((start+left)*mm, (indication_y-lineheight)*mm, (start+left)*mm, indication_y*mm)
	pdf.drawCentredString((start + (duration/2))*mm, (indication_y-(lineheight/2))*mm, '◷ {} hours'.format(hours))
	pdf.line((end+left)*mm, (indication_y-lineheight)*mm, (end+left)*mm, indication_y*mm)
	
	# second piece of line
	start = 150
	end = 303
	duration = end - start
	days = round(duration / daywidth)
	months = round(days / 30)
	pdf.line((start+left)*mm, (indication_y-lineheight)*mm, (start+left)*mm, indication_y*mm)
	pdf.drawCentredString((start + (duration/2))*mm, (indication_y-(lineheight/2))*mm, '◷ {} months'.format(months))
	pdf.line((end+left)*mm, (indication_y-lineheight)*mm, (end+left)*mm, indication_y*mm)
	
	# third piece of line
	start = 303
	end = 410
	duration = end - start
	days = round(duration / daywidth)
	weeks = round(days / 7)
	pdf.line((start+left)*mm, (indication_y-lineheight)*mm, (start+left)*mm, indication_y*mm)
	pdf.drawCentredString((start + (duration/2))*mm, (indication_y-(lineheight/2))*mm, '◷ {} weeks'.format(weeks))
	pdf.line((end+left)*mm, (indication_y-lineheight)*mm, (end+left)*mm, indication_y*mm)
	