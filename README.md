# Iterations

This publication aims to create openings and inspire new concepts for work yet to be developed. As a vessel it is filled with hybrid content that can be unpacked and remixed. Inside this book, contributions of some participants from the past and Iterators from the future coincide in a joint effort to transport ideas to persons and contexts that are yet unknown to us. As such this  publication could be read as an Iteration in itself.
The contributions are related to approaches, attitudes, positions that emerged from the Iterations project. Manetta Berends & Jara Rocha created a set of descriptions of common threads that were called ‘handles’, and invited each contributor to respond to one of them. During the ‘editorial sprint’ to process the material, scores were produced that allowed for a transversal reading of the contributions. This practice was named ‘x-dexing’.

https://iterations.space/publication/

![](https://iterations.space/files/iterations-publication.gif)

![](https://iterations.space/files/iterations-x-dex.gif)

Download the publication

Iterations \[PDF, 3.2MB\]: https://iterations.space/files/iterations-publication.pdf

x-dex \[PDF, 600 KB\]: https://iterations.space/files/iterations-x-dex.pdf
